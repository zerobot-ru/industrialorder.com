<?php

$errorNum = isset($_REQUEST['errorNum']) ? $_REQUEST['errorNum'] : false;

$pageTitle = 'Внутренняя ошибка (error id: ' . $_REQUEST['errorNum'] . ')';
$h1Title = 'Ошибка!';
$pageDesc = '';

//echo '<div class="text-muted"> '"</div><br>";
echo '<header id="head" class="secondary"></header>
<!-- container -->
<div class="container">

    <ol class="breadcrumb">
        <li><a href="<?php echo BEZ_HOST; ?>?mode=index">Главная</a></li>
        <li class="active">Ошибка</li>
    </ol>

    <div class="row">
        <!-- Article main content -->
        <article class="col-md-9 maincontent">
            <header class="page-header">
                <h1 class="page-title">Ошибка!</h1>
            </header>
<div class="panel panel-warning">
  <div class="panel-heading">
    <h3 class="panel-title">Код ошибки:</h3>
  </div>
  <div class="panel-body">' . $_REQUEST['errorNum'] . '</div>
</div>';
switch ($errorNum) {
    case 1:
        echo '<div class="alert alert-info" role="alert">!</div>';
        break;
    case 2:
        echo '<div class="alert alert-info" role="alert">!</div>';
        break;
    case 3:
        echo '<div class="alert alert-info" role="alert">Неправильный SSID и IP!</div>';
        echo '<div class="alert alert-info" role="alert">Вы уже выполнили вход с ip адреса: ' . $_REQUEST['ip'] . '</div>';
        break;
    case 4:
        echo '<div class="alert alert-info" role="alert">!</div>';
        break;
    case 5:
        echo '<div class="alert alert-info" role="alert">Вы уже добавили компанию!</div>';
        break;
    case 6:
        echo '<div class="alert alert-info" role="alert">!</div>';
        break;
    case 7:
        echo '<div class="alert alert-info" role="alert">Вы должны <a href="' . BEZ_HOST . '?mode=company_add">добавить компанию</a>!</div>';
        break;
    case 8:
        echo '<div class="alert alert-info" role="alert">Вы должны <a href="' . BEZ_HOST . '?mode=auth">авторизоваться</a> или <a href="' . BEZ_HOST . '?mode=reg">зарегистрироваться</a>!</div>';
        break;
    case 9:
        echo '<div class="alert alert-info" role="alert">!</div>';
        break;
    case 10:
        echo '<div class="alert alert-info" role="alert">Доступ закрыт!</div>';
        break;
    default:
        echo '<div class="alert alert-info" role="alert">Неизвестная ошибка.</div>';
        break;
}
echo '</article><!-- /Article --></div>
</div><!-- /container -->';
?>
