<?php

/**
 * Обработчик редактирования анкеты пользователя
 */

if ($_SESSION['currentLng'] == "ru-ru") {
    $pageTitle = 'Редактирование профиля пользователя' . $label_PTitle;
    $pageDesc = 'Личный кабинет пользователя';
} else {
    $pageTitle = 'Edit user profile' . $label_PTitle;
    $pageDesc = 'User account';
}

if ($user === false) {
    echo '<div class="alert alert-info" role="alert">' . $label_Guest . '</div>' . "\n";
    $reg_login = '';
    $reg_user_name = '';
    $reg_user_tel = '';
    $reg_avatar = '';
}

if ($user === true) {
    $sql = 'SELECT * FROM `' . BEZ_DBPREFIX . 'reg` WHERE `login`=:email';
    $stmt = $db->prepare($sql);
    $stmt->bindValue(':email', $_SESSION['login'], PDO::PARAM_STR);
    $stmt->execute();
    $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
    $reg_login = htmlspecialchars($rows[0]['login']);
    $reg_user_name = htmlspecialchars($rows[0]['name']);
    $department=htmlspecialchars($rows[0]['department']);
    $position=htmlspecialchars($rows[0]['position']);
    $reg_user_tel = htmlspecialchars($rows[0]['tel']);
    if ($rows[0]['avatar'] == NULL) {
        $_SESSION['reg_avatar'] = BEZ_HOST . 'uploads/avatars/user.jpg';
    } else {
        $_SESSION['reg_avatar'] = BEZ_HOST . 'uploads/avatars/' . htmlspecialchars($rows[0]['avatar']);
    }

    echo "<!-- HOST: " . BEZ_HOST . "<br>";
    $folder_tmp = ROOT_DIR . "/uploads/avatars";
    echo "FULL: " . $folder_tmp . "<br>";
    echo "ROOT_DIR: " . ROOT_DIR . "<br> -->";

    if ($rows[0]['login'] == $_SESSION['login']) {

        if (isset($_POST['submit'])) {

            //echo "1 Нажали кнопку.<br>";

            /*if (empty($_POST['tel']))
                $err[] = '<div class="alert alert-danger" role="alert">Не указан телефон</div>';*/

            if (count($err) > 0)
                echo showErrorMessage($err);
            else {

                //echo "2 Подгатавливаем запрос.<br>";

                /* Создаем запрос на запись в базу
                  новой информации от пользователя */
                $sql2 = 'UPDATE `' . BEZ_DBPREFIX . 'reg` SET tel=:tel, name=:user_name, department=:department, position=:position WHERE login=:login';
                //Подготавливаем PDO выражение для SQL запроса
                $stmt = $db->prepare($sql2);

                $stmt->bindValue(':tel', $_POST['tel'], PDO::PARAM_STR);
                $stmt->bindValue(':user_name', $_POST['user_name'], PDO::PARAM_STR);
                $stmt->bindValue(':department', $_POST['department'], PDO::PARAM_STR);
                $stmt->bindValue(':position', $_POST['position'], PDO::PARAM_STR);
                $stmt->bindValue(':login', $_SESSION['login'], PDO::PARAM_STR);

                //echo "3 Выполняем запрос.<br>";

                $stmt->execute();

                //echo "4 Запрос выполнен.<br>";

                // Начало обработчика загрузки файлов

                $folder = ROOT_DIR . "/uploads/avatars/"; //BEZ_HOST . 'uploads/avatars/'
                $ext = array(".gif", ".jpg", ".jpeg", ".png", ".tif", ".pdf");

                //echo "5 Определили переменные для загрузки файлов.<br>";

                // Получаем расширение файла
                $file_extends = strtolower(strrchr($_FILES['userfile']['name'], '.'));
                // Генерируем случайное число
                $file_name = uniqid(rand(10000, 99999));
                // Формируем путь на сервере
                $uploadedFile = $folder . $file_name . $file_extends;
                $fileName = $file_name . $file_extends;
                echo '<!-- <br>file_name: ' . $file_name;
                echo '<br>file_extends: ' . $file_extends;
                echo '<br>folder: ' . $folder;
                echo '<br>fileName: ' . $fileName . '<br> --';

                if (!in_array($file_extends, $ext)) {
                    echo "<div class=\"alert alert-danger\" role=\"alert\">Такое нельзя загружать.</div>";
                } else {

                    //echo "6 Подгатавливаем запрос для аватарки.<br>";

                    $sql3 = 'UPDATE `bez_reg` SET avatar =:avatar WHERE login=:login';
                    $stm = $db->prepare($sql3);
                    $stm->bindValue(':avatar', $fileName, PDO::PARAM_STR);
                    $stm->bindValue(':login', $_SESSION['login'], PDO::PARAM_STR);

                    //echo "7 Готовимся его выполнить.<br>";

                    $stm->execute();

                    //echo "8 Запрос выполнен.<br>";

                    if (is_uploaded_file($_FILES['userfile']['tmp_name'])) {

                        //echo "9 Из аплоадед файл<br>";

                        if (move_uploaded_file($_FILES['userfile']['tmp_name'], $uploadedFile)) {
                            echo "<div class=\"alert alert-success\" role=\"alert\">Файл загружен.</div>";
                        } else {
                            echo "<div class=\"alert alert-danger\" role=\"alert\">Во время загрузки файла произошла ошибка.</div>";
                            echo($_FILES["userfile"]["error"]);
                        }
                    } else {
                        echo "<div class=\"alert alert-danger\" role=\"alert\">Файл не загружен.</div>";
                    }
                }
            }

            $_SESSION['user_name'] = $_POST['user_name'];

            header('Location:' . BEZ_HOST . '?mode=profile');
            exit;
        }
    }
   
if(isset($_REQUEST['submit1'])){
$new_pass = htmlspecialchars(stripslashes($_REQUEST['new_pass']));
$new_pass1 = htmlspecialchars(stripslashes($_REQUEST['new_pass1']));  
if($new_pass == $new_pass1 && $new_pass !== "" && $new_pass1 !== ""){
    $sql4 = 'UPDATE `bez_reg` SET pass =:pass WHERE id_reg=:id_reg';
    $pass = md5($new_pass);
    $stm4 = $db->prepare($sql4);
    $stm4->bindValue(':pass', $pass, PDO::PARAM_STR);
    $stm4->bindValue(':id_reg', $_SESSION['id_reg'], PDO::PARAM_STR);
    $stm4->execute();
}


}
} else {
    header('Location:' . BEZ_HOST . '?mode=error&errorNum=2');
    exit;
}


?>