<?php

if ($_SESSION['currentLng'] == "ru-ru") {
    $pageTitle = 'Личный кабинет пользователя' . $label_PTitle;
    $pageDesc = 'Личный кабинет пользователя';
} else {
    $pageTitle = 'User account' . $label_PTitle;
    $pageDesc = 'User account';
}

if ($user === false) {
    header('Location:' . BEZ_HOST . '?mode=auth');
    exit;
}
if ($user === true) {

    if ($_SESSION['allready'] == 0) {
        $button_company = '<a href="' . BEZ_HOST . '?mode=company_add" class="btn btn-primary btn-xs">' . $label_AddCompany . '</a>';
        $company_files = '<div class="alert alert-info" role="alert">' . $label_AddFilesAlert . '</div>';
    } else {
        $button_company = '<a href="' . BEZ_HOST . '?mode=company_edit" class="btn btn-danger btn-xs">' . $label_EditCompany . '</a>';
        $button_company .= ' ';
        $button_company .= '<a href="' . BEZ_HOST . '?mode=company_admin" class="btn btn-default btn-xs">' . $label_CManage . '</a>';
        $company_files = '<a href="' . BEZ_HOST . '?mode=company_files" class="btn btn-primary btn-xs">' . $label_AddFiles . '</a>';
        // TODO: Добавить проверку на админа!
    }

    $sql = 'SELECT * FROM `' . BEZ_DBPREFIX . 'reg` WHERE `login`=:email';

    $stmt = $db->prepare($sql);
    $stmt->bindValue(':email', $_SESSION['login'], PDO::PARAM_STR);
    $stmt->execute();

    $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

    $reg_login = htmlspecialchars($rows[0]['login']);
    //$reg_role = htmlspecialchars($rows[0]['role']);
    $reg_user_name = htmlspecialchars($rows[0]['name']);
    $_SESSION['uName'] = htmlspecialchars($rows[0]['name']);
    //$reg_company = htmlspecialchars($rows[0]['r_company']);
    $reg_user_tel = htmlspecialchars($rows[0]['tel']);
    $department = htmlspecialchars($rows[0]['department']);
    $position = htmlspecialchars($rows[0]['position']);
    if ($rows[0]['avatar'] == NULL) {
        $_SESSION['reg_avatar'] = BEZ_HOST . 'uploads/avatars/user.jpg';
    } else {
        $_SESSION['reg_avatar'] = BEZ_HOST . 'uploads/avatars/' . htmlspecialchars($rows[0]['avatar']);
    }
    $add_company = htmlspecialchars($rows[0]['allready']);
    $company_id = htmlspecialchars($rows[0]['company_id']);

    $sql = 'SELECT * FROM `' . BEZ_DBPREFIX . 'companys` WHERE `id_company`=:id_company';
    $stmt = $db->prepare($sql);
    $stmt->bindValue(':id_company', $company_id, PDO::PARAM_STR);
    $stmt->execute();

    $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

    if (count($rows) > 0) {

        $s_select = explode(", ", $rows[0]['c_section']);

        /* foreach ($s_select as $val) {
            $sql1 = 'SELECT * FROM `' . BEZ_DBPREFIX . 'cat` WHERE cat_id=' . $val; // . ' ORDER BY cat_id ASC'
            $stmt1 = $db->prepare($sql1);
            //Выводим контент
            if ($stmt1->execute()) {
                //$summary = '';
                $rows1 = $stmt1->fetchAll(PDO::FETCH_ASSOC);
                foreach ($rows1 as $val1) {
                    $section .= '<span class="label label-default">' . $val1['cat_name'] . '</span> ';
                }
            }
        } */

        foreach ($s_select as $val) {
            if ($_SESSION['currentLng'] == "ru-ru") {
                    $sql1 = 'SELECT * FROM `class_new` WHERE id=' . $val;
                } else {
                    $sql1 = 'SELECT * FROM `class_new_eng` WHERE id=' . $val;
                }
            
            //    $sql1 = 'SELECT * FROM `class_new` WHERE id=' . $val;
            $stmt1 = $db->prepare($sql1);
            if ($stmt1->execute()) {
                $rows1 = $stmt1->fetchAll(PDO::FETCH_ASSOC);
                foreach ($rows1 as $val1) {
                    $section .= '<strong>' . $val1['name'] . '</strong>, ';
                }
            }
        }

        $id_company = $rows[0]['id_company'];
        //$section = ''; // $rows[0]['c_section']
        $c_name = htmlspecialchars($rows[0]['c_name']);
        $short_desc = nl2br($rows[0]['short_desc']);
        $full_desc = nl2br($rows[0]['full_desc']);
        //$country = $rows[0]['country'];
        //$city = $rows[0]['city'];
        //$address = $rows[0]['address'];
        //$c_tel = $rows[0]['c_tel'];
        //$email = $rows[0]['email'];
        //$site = $rows[0]['site'];
        //$contact = $rows[0]['contact'];
        $service = $rows[0]['service'];
        $inn = $rows[0]['c_inn'];

        $cmp = "<table class='table table-striped'><tbody>";
        $cmp .= "<tr><th scope='row'>id:</th><td><a href='" . BEZ_HOST . "?mode=company_view&vid=" . $id_company . "'>" . $id_company . "</a></td></tr>";
       // $cmp .= "<tr><th scope='row'>" . $label_SDescription . ":</th><td>" . nl2br($short_desc) . "</td></tr>";
        $cmp .= "<tr><th scope='row'>" . $label_FDescription . ":</th><td>" . nl2br($full_desc) . "</td></tr>";
        $cmp .= "<tr><th scope='row'>" . $label_Activity . ":</th><td>" . $section . "</td></tr>";
        $cmp .= "<tr><th scope='row'>" . $label_Title . ":</th><td>" . $c_name . "</td></tr>";
      //  $cmp .= "<tr><th scope='row'>" . $label_Services . ":</th><td>" . $service . "</td></tr>";
        $cmp .= "<tr><th scope='row'>" . $label_INN . ":</th><td>" . $inn . "</td></tr>";
        $cmp .= "</tbody></table>";

        // <tr> <th scope="row">1</th> <td>Mark</td> </tr>
        // <tr> <th scope="row">2</th> <td>Jacob</td> </tr>

    } else {
        $cmp = "";
    }
}

//Добавить фотографии
            
$sql_p = 'SELECT * FROM `' . BEZ_DBPREFIX . 'company_files` WHERE `company_id`=' . $id_company;
$stmt = $db->prepare($sql_p);
$stmt->execute();
$rows_p = $stmt->fetchAll(PDO::FETCH_ASSOC);
$folder = 'uploads/companys/goods/';
//print_r($id_company);
foreach ($rows_p as $val_p) {

   $photo .= '<div class= "col-sm-4" style="text-align:center"><a href ="' . $folder . $val_p['file'] . '"class="fancybox" rel="gallery">' . '<img class="o-img img-rounded" src="' . $folder . $val_p['file'] . '">' . '</a><br>';
    $photo .= '<noindex><a href = "?mode=profile&delete=' . $val_p['file'] . '"class = "href" rel=""nofolow>' . $btn_Delete . '</a></noindex></div>';

}

//Удаляем фото 

if(isset($_REQUEST['delete']))
{
$delete =  htmlspecialchars(stripslashes($_REQUEST['delete']));
$filename = $folder . $delete;
unlink($filename);
 $sql_d = 'DELETE FROM `' . BEZ_DBPREFIX . 'company_files` WHERE `file`=:file';
    $stmt_d = $db->prepare($sql_d);
    $stmt_d->bindValue(':file', $delete, PDO::PARAM_STR);
    $stmt_d->execute();
    header('location:?mode=profile');
}


$friend_result .= "<h4><b>" . $label_Friends . ": </b></h4><br>";
$friends_sql2 = "SELECT c_name, friends, id_company FROM `bez_companys` WHERE id_company=:id_company";
$friends_pdo2 = $db -> prepare($friends_sql2);
$friends_pdo2 -> bindValue(':id_company', $_SESSION['r_company'], PDO::PARAM_STR);
$friends_pdo2 -> execute();
$friends1 = $friends_pdo2 -> fetchall(PDO::FETCH_ASSOC);
foreach($friends1 as $friend1) {
    $id_friends = $friend1['friends'];
    $id_friend = explode(",", $id_friends);
    foreach($id_friend as $id_f) {
        $friends_sql3 = "SELECT c_name FROM `bez_companys` WHERE id_company=:id_company";
        $friends_pdo3 = $db -> prepare($friends_sql3);
        $friends_pdo3 -> bindValue(':id_company', $id_f, PDO::PARAM_STR);
        $friends_pdo3 -> execute();
        $friends2 = $friends_pdo3 -> fetchall(PDO::FETCH_ASSOC); 
        foreach($friends2 as $friend2) {
        $friend_result .= "<a href='?mode=company_view&vid=" . $friend1['id_company'] . "'>" . $friend2[c_name] . "</a><br>";
        }
    }
}
$notes_result .= "<h4><b>" . $label_Notes . ": </b></h4><br>";

$notes_sql = "SELECT * FROM `bez_notes` WHERE my_company=:my_company";
$notes_pdo = $db -> prepare($notes_sql);
$notes_pdo -> bindValue(':my_company', $_SESSION['r_company'], PDO::PARAM_STR);
$notes_pdo -> execute();
$notes = $notes_pdo -> fetchall(PDO::FETCH_ASSOC);
foreach($notes as $note) {
    $notes_result .= "Заметка: " . $note['note'] . " "; 
    $id_company = $note['id_company'];
        $notes_sql1 = "SELECT c_name FROM `bez_companys` WHERE id_company=:id_company";
        $notes_pdo1 = $db -> prepare($notes_sql1);
        $notes_pdo1 -> bindValue(':id_company', $id_company, PDO::PARAM_STR);
        $notes_pdo1 -> execute();
        $notes1 = $notes_pdo1 -> fetchall(PDO::FETCH_ASSOC); 
        foreach($notes1 as $note1) {
        $date = strtotime($note['datetime']);
        $datetime = date("d-m-y: H i", $date);
        $notes_result .= "О компании: " . $note1['c_name'] . " дата: " . $datetime . "<br>";
        }
  
}



$label_Delete = "Удалить";
$path = "uploads/companys/goods/";
if (isset($_REQUEST['submit'])) {
    $length = count($_FILES['filename']['name']);
    $array = array(".jpeg", ".gif", ".png", ".jpg");
    $ids .= " ";
    for ($i = 0; $i < $length; $i++) {
        if ($_FILES['filename']['size'][$i] > 1024 * 8 * 1000) {
         $res.= '<br><p class="col-md-12">Разрешается загружать файлы размером не более 1 mb и формата jpeg, gif, png, jpg</p>'; 
         break;
        }
        $ext = strtolower(strrchr($_FILES['filename']['name'][$i], "."));
        if (in_array($ext, $array)) {
            //echo $ext . "<br><br><br><br>";
            //echo  $_FILES['filename']['type'][$i];       
           $file_name = uniqid(rand(10000, 99999)) . $ext;
            resize($_FILES['filename'], $type = 1, $quality = 75, $i, $path);
            move_uploaded_file($_FILES['filename']['tmp_name'][$i], $path . $file_name);
            
            $insert = "INSERT INTO `" . BEZ_DBPREFIX . "company_files` SET file=:file, company_id=:company_id, file_type=:file_type";
            $pdo = $db -> prepare($insert);
            $pdo -> bindValue(':file', $file_name, PDO::PARAM_STR);
            $pdo -> bindValue(':company_id', $_SESSION['r_company'], PDO::PARAM_STR);
            $pdo -> bindValue(':file_type', 0, PDO::PARAM_STR);
            $pdo -> execute();
            $j = $i+1;
            $select = "SELECT id_file FROM `" . BEZ_DBPREFIX . "company_files` WHERE file=:file"; 
            $pdo1 = $db -> prepare($select);
            $pdo1 -> bindValue(':file', $file_name, PDO::PARAM_STR);
            $pdo1 -> execute();
            $select_result = $pdo1 -> fetch(PDO::FETCH_ASSOC);
            $res .= "<div class='panel panel-default'>";
            $res .= "<div class='panel-body'>";
            $res .= '<div class="col-md-6" id = photo>';
            $res .= "<img src = '" . $path . $file_name . "'></div>";
            $res .= "<div class='col-md-8'><span class='pull-right'>";
            $res .= "<span class = 'outer'><a class = 'kill'  href = '?mode=test1&delete=" . $select_result['id_file'] . "' onClick = 'return del()
'><span class = 'del' onClick = 'change()'>" . $j . " Удалить фото</span></a><span></span>";
            $res .= "</div></div></div>";
            //$res .= "<img src = '" . $path . $_FILES['filename']['name'][$i] . "'>";
            
            $ids .= $select_result['id_file'] . " ";
            header("location:" . $_SERVER['SCRIPT_NAME'] . "?" . $_SERVER['QUERY_STRING']);
       
?>

<!-- <div class='someClass' onclick='alert(this.innerHTML)'>123</div> -->

  

  <script>
      function change(){
           $(document).on('click','.del',function two(e){
             var name1 = $(this).html();
             name1 = parseInt(name1) - 1;
          var elem = document.querySelectorAll('span.outer');
           
                elem[name1].innerHTML = "Удалено";
                $('.outer').css({"color" : "#f00"});
            
      });
  }
  
    function del() {
       //var j = show();
       //alert(j);
         $(document).on('click','.del',function one(e){
             var name = $(this).html();
             name = parseInt(name)
        var del = [];
        var del = "<?php echo $ids; ?>";
        var result = del.split(" ");
        //alert(document.getElementsByClassName('outer').innerHTML);
        //alert(del);
        //alert(result[name]);
      // alert(res[name]);
           // alert(res[name]);
           var res = result[name];
        $.ajax({
            type: 'post',
            url: "../scripts/profile/ajax.php",
            data: {del:res},
            response: 'text',
            success: function(data) {  
                //var detete = document.getElementById('del').innerHTML;
                //document.querySelectorAll('.kill').outerHTML = "";
                 //document.getElementsByClassName('del').outerHTML.outerHTML = "";
                //document.getElementsByClassName('outer').innerHTML = "Удалено";
                //alert(document.getElementById('kill').outerHTML);
                //document.getElementById('del').outerHTML.outerHTML = "<span id = 'del'>Удалено</span></a><span>";
                        //document.getElementById('outer').innerHTML = "Удалено";
            },
            error: function() {
                alert("Ошибка ajax");
               
            }
        });
        
        });
    
    return false;
 }
 </script>
<?php
        }
         else{
         $res.= '<br><p class="col-md-12">Разрешается загружать файлы размером не более 100 mb и формата jpeg, gif, png, jpg</p>'; 
         break;
         }
    }
}     

function resize($file, $type = 1, $quality = 85, $i, $path) {
    
    $max_type1_size = 1024;
    $max_type2_size = 800;
    if($file['type'][$i] == 'image/png'){
        $source = imagecreatefrompng($file['tmp_name'][$i]);
    }
    elseif($file['type'][$i] == 'image/jpeg'){
        $source = imagecreatefromjpeg($file['tmp_name'][$i]);
    }
    elseif($file['type'][$i] == 'image/gif'){
        $source = imagecreatefromgif($file['tmp_name'][$i]);
    }   
    else { return false; }
    $w_source = imageSX($source);
    $h_source = imageSY($source);
        if($type == 1) {
            $weight = $max_type1_size;
        }
        elseif($type == 2) {
            $weight = $max_type2_size;
        }
            if($w_source > $weight) {
                $ratio = $w_source/$weight;
                $w_dest = round($w_source/$ratio);
                $h_dest = round($h_source/$ratio);
                $dest = imagecreatetruecolor($w_dest, $h_dest);
                imagecopyresampled($dest, $source, 0, 0, 0, 0, $w_dest, $h_dest, $w_source, $h_source);
                imagejpeg($dest, $file['tmp_name'][$i], $quality);
                imagedestroy($dest);
                imagedestroy($source);
                return $file['name'][$i];
            }
            else {
                imagejpeg($source, $file['tmp_name'][$i] . $file['name'][$i], $quality);
                imagedestroy($source);
                return $file['name'][$i];
                }
}

if(isset($_REQUEST['submit_email'])){
    $email = htmlspecialchars(stripslashes($_REQUEST['email']));
    $sql_ch0 = "SELECT login FROM bez_reg WHERE login=:login";
   $sql_ch0 = $db -> prepare($sql_ch0);
   $sql_ch0 -> bindValue(':login', $email, PDO::PARAM_STR);
   $sql_ch0 -> execute();
   $sql_ch0_rows = $sql_ch0 -> fetchAll(PDO::FETCH_ASSOC);
   
   
   if(count($sql_ch0_rows) == 0){
   $sql_ch = "UPDATE bez_reg SET login=:login WHERE id_reg=:id_reg";
   $sql_ch = $db -> prepare($sql_ch);
   $sql_ch -> bindValue(':login', $email, PDO::PARAM_STR);
   $sql_ch -> bindValue(':id_reg', $_SESSION['id_reg'], PDO::PARAM_STR);
   $sql_ch -> execute();
   
   $sql_ch1 = "UPDATE bez_companys SET email=:email WHERE id_company=:id_company";
   $sql_ch1 = $db -> prepare($sql_ch1);
   $sql_ch1 -> bindValue(':email', $email, PDO::PARAM_STR);
   $sql_ch1 -> bindValue(':id_company', $_SESSION['r_company'], PDO::PARAM_STR);
   $sql_ch1 -> execute();
   
   $_SESSION['login'] = $email;
   header('location:?mode=profile'); 
   }
 
}
if($_SESSION['id_reg'] == 188 || $_SESSION['id_reg'] == 399 || $_SESSION['id_reg'] == 4757) {
    $Mars .= '<form action="" method="post">';
    $Mars .= '<input class="form-control datepicker" type="text" name="date"><br>';
    $Mars .= '<input type="submit" name = "submit2" value = "Выбрать"><br><br>';
    $Mars .= '</form>';
    $date1 = strtotime(htmlspecialchars(stripslashes($_REQUEST['date'])));
    
    $date1 = date("Y-m-d", $date1);
    $_SESSION['date'] = $date1;
    //$date1 = str_replace(".", "-", $date1);
    //$date1 = date("Y-m-d");
    $sql = "SELECT COUNT(*), agent_id FROM `" . BEZ_DBPREFIX . "companys` WHERE reg_date LIKE '%$date1%' GROUP BY agent_id";
    
    $stmt = $db->prepare($sql);
    $stmt->bindValue(':id_company', $company_id, PDO::PARAM_STR);
    $stmt->bindValue(':reg_date', $reg_date, PDO::PARAM_STR);
    $stmt->execute();

    $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
    $i=0;
    
    $number = htmlspecialchars(stripslashes($_REQUEST['number']));

    foreach($rows as $row){
        
        
    $sql1 = 'SELECT * FROM agent_names WHERE agent_id=:agent_id';
    $stmt1 = $db->prepare($sql1);
    $stmt1->bindValue(':agent_id', $row['agent_id'], PDO::PARAM_STR);
    $stmt1->execute(); 
    $rows1 = $stmt1->fetch(PDO::FETCH_ASSOC);
    //$date =date('Y-m-d');
    $Mars .= "<a href='#' onclick='return false' class='" . $row['agent_id'] . " agent'>" .$rows1['agent_name'] . " " . $row['COUNT(*)'] . "</a><br>";
    $i++;
    $sum += $row['COUNT(*)'];
//$row['agent_id']
    }
    
    
   
            
    $Mars .= "Всего зарегистрировано компаний: " . $sum;
    $sql3 = "SELECT * FROM bez_companys WHERE reg_date LIKE '%$date1%' AND agent_id=:agent_id";
    
    $stmt3 = $db->prepare($sql3);
    
    $stmt3->bindValue(':agent_id', 0, PDO::PARAM_STR);
    //$stmt3->bindValue(':reg_date', $reg_date, PDO::PARAM_STR);
    $stmt3->execute();
    $rows3 = $stmt3 -> fetchAll(PDO::FETCH_ASSOC);
    
    
    $Mars .= "<h2 class ='label-control'>Самостоятельно зарегистрировались: <br></h2>";
    foreach($rows3 as $row3){
        
       $Mars .= "<a href = '?mode=company_view&vid=" . $row3['id_company'] . "'>" . $row3['c_name']. "</a><br>";
    }
     
   
}
    

?>
 <html>
    <head>
        <style>
            #photo{
                width: 200px;
            }
            
        </style>
    </head>
 </html>

