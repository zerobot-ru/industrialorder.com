<?php
/**
 * Created by PhpStorm.
 * User: Aleksej Kazin
 * Date: 07.11.2016
 * Time: 13:57
 */

$pageTitle = 'О сайте industrialorder.com';
$h1Title = 'Подробнее о нашем сайте';
$pageDesc = '';

// SELECT COUNT(*) FROM table

//Запрос на выборку контента согласно роли
$sql1 = 'SELECT id_order FROM `' . BEZ_DBPREFIX . 'orders`';
$stmt = $db->prepare($sql1);

//Выводим контент
if ($stmt->execute()) {
    $rows1 = $stmt->fetchAll(PDO::FETCH_ASSOC);
    $rows11 = count($rows1);
} else {
    $rows1 = "n/a";
}

//Запрос на выборку контента согласно роли
$sql2 = 'SELECT id_file FROM `' . BEZ_DBPREFIX . 'files`';
$stmt = $db->prepare($sql2);

//Выводим контент
if ($stmt->execute()) {
    $rows2 = $stmt->fetchAll(PDO::FETCH_ASSOC);
    $rows22 = count($rows2);
} else {
    $rows2 = "n/a";
}

//Запрос на выборку контента согласно роли
$sql3 = 'SELECT id_company FROM `' . BEZ_DBPREFIX . 'companys`';
$stmt = $db->prepare($sql3);

//Выводим контент
if ($stmt->execute()) {
    $rows3 = $stmt->fetchAll(PDO::FETCH_ASSOC);
    $rows33 = count($rows3);
} else {
    $rows3 = "n/a";
}

//Запрос на выборку контента согласно роли
$sql4 = 'SELECT id_reg FROM `' . BEZ_DBPREFIX . 'reg`';
$stmt = $db->prepare($sql4);

//Выводим контент
if ($stmt->execute()) {
    $rows4 = $stmt->fetchAll(PDO::FETCH_ASSOC);
    $rows44 = count($rows4);
} else {
    $rows4 = "n/a";
}

if ($_SESSION['login'] == 'kazin@brasque.ru') {

    $_SESSION['test']='Abrakadabra!';
    $a = session_id();

    if ($_SESSION['currentLng'] == "ru-ru") {

        $sql1 = 'SELECT * FROM `type_new` ORDER BY id ASC';

    } else {

        $sql1 = 'SELECT * FROM `type_new` ORDER BY id ASC';

    }

    $stmt1 = $db->prepare($sql1);

    if ($stmt1->execute()) {

        $summary = '<ol>';
        $rows1 = $stmt1->fetchAll(PDO::FETCH_ASSOC);

        foreach ($rows1 as $val1) {

            if ($_SESSION['currentLng'] == "ru-ru") {

                $sql22 = 'SELECT * FROM `class_new` WHERE parent=' . $val1['id'];

            } else {

                $sql22 = 'SELECT * FROM `class_new` WHERE parent=' . $val1['id'];

            }

            $stmt22 = $db->prepare($sql22);
            $summary .= '<li>' . $val1['name'] . '</li>';
            $summary .= "<ol>";

            if ($stmt22->execute()) {

                $rows2222 = $stmt22->fetchAll(PDO::FETCH_ASSOC);

                foreach ($rows2222 as $val22) {

                    $sql_c = 'SELECT * FROM `bez_companys` WHERE `c_section` = :c_section';
                    $stmt_c = $db->prepare($sql_c);
                    $stmt_c->bindValue(':c_section', $val22['id'], PDO::PARAM_STR);

                    if ($stmt_c->execute()) {

                        $rows_c = $stmt_c->fetchAll(PDO::FETCH_ASSOC);

                        if (count($rows_c) > 0) {

                            $summary .= '<li><strong>' . $val22['name'] . '</strong> - <span class="label label-default pull-right">' . count($rows_c) . '</span></li>';

                        }

                    }

                }

            }

            $summary .= "</ol>";

        }

        $summary .= "</ol>";

    }

} else {
    $admin = 'Вы не админ.';
}

?>