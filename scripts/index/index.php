<?php
/**
 * Created by PhpStorm.
 * User: Aleksej Kazin
 * Date: 26.11.2016
 * Time: 20:12
 */

if ($_SESSION['currentLng'] == "ru-ru") {
    $pageTitle = 'Главная страница' . $label_PTitle;
    $pageDesc = 'Сайт industrialorder.com - эффективный инструмент для привлечения и поиска заказов для вашего предприятия';
} else {
    $pageTitle = 'Main page' . $label_PTitle;
    $pageDesc = 'Site industrialorder.com - an effective tool for attracting and searching for orders for your company';
}

//Запрос на выборку контента согласно роли
$sql1 = 'SELECT id_order FROM `' . BEZ_DBPREFIX . 'orders`';
$stmt = $db->prepare($sql1);

//Выводим контент
if ($stmt->execute()) {
    $rows1 = $stmt->fetchAll(PDO::FETCH_ASSOC);
    $rows11 = 10 + count($rows1);
} else {
    $rows1 = "n/a";
}

//Запрос на выборку контента согласно роли
$sql2 = 'SELECT id_file FROM `' . BEZ_DBPREFIX . 'files`';
$stmt = $db->prepare($sql2);

//Выводим контент
if ($stmt->execute()) {
    $rows2 = $stmt->fetchAll(PDO::FETCH_ASSOC);
    $rows22 = 10 + count($rows2);
} else {
    $rows2 = "n/a";
}

//Запрос на выборку контента согласно роли
$sql3 = 'SELECT id_company FROM `' . BEZ_DBPREFIX . 'companys`';
$stmt = $db->prepare($sql3);

//Выводим контент
if ($stmt->execute()) {
    $rows3 = $stmt->fetchAll(PDO::FETCH_ASSOC);
    $rows33 = 10 + count($rows3);
} else {
    $rows3 = "n/a";
}

//Запрос на выборку контента согласно роли
$sql4 = 'SELECT id_reg FROM `' . BEZ_DBPREFIX . 'reg`';
$stmt = $db->prepare($sql4);

//Выводим контент
if ($stmt->execute()) {
    $rows4 = $stmt->fetchAll(PDO::FETCH_ASSOC);
    $rows44 = 10 + count($rows4);
} else {
    $rows4 = "n/a";
}
$sql_people = 'SELECT id_company FROM `' . BEZ_DBPREFIX . 'companys`';
$arr_people = $db->prepare($sql_people);
$arr_people -> execute();
$result = $arr_people -> fetchAll(PDO::FETCH_ASSOC);
$peoples = count($result);

if ($_SESSION['currentLng'] == "ru-ru") {
    $mainText = '
    <!-- Header -->
<header id="head">
    <div class="container">
        <div class="row">
            <div id = "js_hidden"><h3>Для корректной работы сайта включите javascript в настройках браузера</h3></div>
            <h1 class="lead" style = "font-size: 70px">Единый бизнес портал</h1>
            <p class="tagline">
                <h4>Профессиональный международный автоматизированный портал в сфере B2B.<br>Найдите своих клиентов, сотрудников, поставщиков, партнеров в России и по всему Миру.</h4>
            </p>
            <div id = "registr">
            <p><a class="btn btn-action btn" style="width: 176px" role="button" href="' . BEZ_HOST . '?mode=auth">Войти</a>
                <a class="btn btn-action btn" style="width: 176px" role="button"
                   href="' . BEZ_HOST . '?mode=reg">Зарегистрироваться</a>
                 <a class="btn btn-success btn" role="button" style="width: 176px"
                   href="' . BEZ_HOST . '?mode=reg_and_order_add&order=true">Разместить заказ</a></p>
            </div>
         <p class = "highlight"><h3>Нас уже: ' . $peoples . '</h3></p>   
        </div>
    </div>
</header>
<!-- /Header -->

<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1 highlight">
            <h4>INDUSTRIAL ORDER — инновационный, комплексный, уникальный в своем роде продукт для предприятий любого уровня и любой сферы деятельности. Десятки инструментов позволяют решать практически любые задачи в сфере B2B: от поиска клиентов, поставщиков и сотрудников до  автоматизации бизнес-процессов. Объединяя в себя тысячи компаний со всего мира мы позволяем раздвинуть границы партнерских связей, сократить затраты денежных средств и времени, увеличить прибыль исключая посредников. </h4>
            

                <div id = "How_this_work"
                    <p><h2 style = "text-align: center"><strong>Как это работает:</strong></h2></p>
                    <p><h3><strong>Для тех, кому нужно разместить заказ:</strong></h3></p>
                </div>     
        </div>
    </div>
    

    <div id = "helper">
        <div class="row">
            <div class="col-md-10 col-md-offset-1 highlight">
                <div class="col-md-2 col-xs-4">
                    <div><button data-toggle-id = "create_company" id = "create_company1" class ="btn btn-primary">Открыть подсказку</button></div>   
                </div>
        
                <div class="col-md-2 col-xs-4 col-md-offset-1 col-xs-offset-2">
                    <div><button data-toggle-id = "create_order" id = "create_order1" class ="btn btn-primary">Открыть подсказку</button></div>
                </div>
        
                <div class="col-md-2 col-xs-4 col-md-offset-1 col-xs-offset-2 ">
                    <div><button data-toggle-id = "show_order" id = "show_order1" class = "btn btn-primary">Открыть подсказку</button></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-10 col-md-offset-1 highlight">
                <div class="col-md-2 col-xs-4 ">
                    <div class="h-caption"><h4><img width = "50%" src = "images/create_company.png"><br><br><a class ="register" href = "?mode=profile">Зарегистрируйте компанию</a></h4></div>
                </div>
                <div class="col-md-1 col-xs-2 "><br><br>
                    <img width = "50%" src = "images/arrow.png">
                </div>
        
                <div class="col-md-2 col-xs-4 ">
                    <div class="h-caption"><h4><img width = "50%" src = "images/create_order.png"><br><br><a class= "register" href = "?mode=order_add">Разместите заказ</a></h4></div>
                </div>
                <div class="col-md-1 col-xs-2 "><br><br>
                    <img width = "50%" src = "images/arrow.png">
                </div>
        
                <div class="col-md-2 col-xs-4">
                    <div class="h-caption"><h4><img width = "50%" src = "images/discussion.png"><br><br><a class = "register" href = "?mode=my_pm">Получайте и выбирайте лучшие коммерческие предложения для Вас</a></h4></div>
                </div>
                <div class="col-md-1 col-xs-2"><br><br>
                    <img width = "50%" src = "images/arrow.png">
                </div>
                <div class="col-md-2 col-xs-4">
                    <div class="h-caption"><h4><img width = "50%" src = "images/according.png"> Заключите сделку</h4></div>
                </div>
            </div>
        </div>
      

    <div class = "row">
        <div class="col-md-10 col-md-offset-1">
            <span id ="create_company" style = "position: relative; margin-left: 100px" hidden>
                <div class = "reg col-md-3"><a href = "images/reg1.PNG" class="fancybox" rel="gallery"><img src = "images/reg1.PNG"></a></div>
                <div class = "reg col-md-3"><a href = "images/reg2.PNG" class="fancybox" rel="gallery"><img src = "images/reg2.PNG"></a></div>
                <div class = "reg col-md-3"><a href = "images/reg3.PNG" class="fancybox" rel="gallery"><img src = "images/reg3.PNG"></a></div>
                <div class = "reg col-md-3"><a href = "images/reg4.PNG" class="fancybox" rel="gallery"><img src = "images/reg4.PNG"></a></div>
            </span>
        </div>

        <div class = "row">
            <span id = "create_order" hidden>
                <div class = "reg col-md-4 col-xs-offset-3" style = "width:20%"><a href = "images/order1.PNG" class="fancybox" rel="gallery1"><img src = "images/order1.PNG"></a></div>
                <div class = "reg col-md-4" style = "width:20%"><a href = "images/order2.PNG" class="fancybox" rel="gallery1"><img src = "images/order2.PNG"></a></div>
            </span>
            <span id = "show_order" hidden>
                <div class = "reg col-md-9 col-xs-offset-3"><a href = "images/get_orders.png" class="fancybox" rel="gallery2"><img src = "images/get_orders.png"></a></div>
            </span>
        </div>
    </div>
    <hr>


    <div class="row">
        <div class="col-md-10 col-md-offset-1 ">
            <p><h3><strong>Для тех, кто ищет заказы:</strong></h3></p>
            
            <div class="row">
                <div class="col-md-12  highlight">
                    <div class="col-md-2 col-xs-10">
                        <div><button data-toggle-id = "create_company_second1" id = "create_company_second11" class ="btn btn-primary">Открыть подсказку</button></div>
                    </div>
        
                    <div class="col-md-2 col-xs-10 col-md-offset-1 col-xs-offset-2">
                        <div><button data-toggle-id = "search_order1" id = "search_order11" class ="btn btn-primary">Открыть подсказку</button></div> 
                    </div>

                    <div class="col-md-2 col-xs-10 col-md-offset-1 col-xs-offset-2 ">
                        <div><button data-toggle-id = "send_order1" id = "send_order11" class = "btn btn-primary">Открыть подсказку</button></div>
                    </div>
                </div>
            </div>
            
            <div class="row">
                <div class="col-md-2 col-xs-10 highlight">
                    <div class="h-caption"><h4><img width = "50%" src = "images/create_company.png"><br><br><a class = "register" href = "?mode=profile">Зарегистрируйте компанию</a></h4></div>
                </div>
        
                <div class="col-md-1 col-xs-0 highlight"><br><br>
                    <img width = "50%" src = "images/arrow.png">
                </div>
        
                <div class="col-md-2 col-xs-10 highlight">
                    <div class="h-caption"><h4><img width = "50%" src = "images/user.png"><br><br>Найдите заказ</h4></div>
                </div>
                <div class="col-md-1 col-xs-0 highlight"><br><br>
                    <img width = "50%" src = "images/arrow.png">
                </div>
                <div class="col-md-2 col-xs-10 highlight">
                    <div class="h-caption"><h4><img width = "50%" src = "images/view.png"><br><br>Отправьте коммерческое предложение</h4></div>
                </div>
                <div class="col-md-1 col-xs-0 highlight"><br><br>
                    <img width = "50%" src = "images/arrow.png">
                </div>
                <div class="col-md-2 col-xs-10 highlight">
                    <div class="h-caption"><h4><img width = "50%" src = "images/according.png"><br><br> Заключите сделку</h4></div>
                </div>
                
                <div class="col-md-12 highlight top-space">
        
                    <div class = "row">
                        <span id ="create_company_second1" style = "position: relative; margin-left: 100px" hidden>
                            <div class = "reg col-md-3"><a href = "images/reg1.PNG" class="fancybox" rel="gallery3"><img src = "images/reg1.PNG"></a></div>
                            <div class = "reg col-md-3"><a href = "images/reg2.PNG" class="fancybox" rel="gallery3"><img src = "images/reg2.PNG"></a></div>
                            <div class = "reg col-md-3"><a href = "images/reg3.PNG" class="fancybox" rel="gallery3"><img src = "images/reg3.PNG"></a></div>
                            <div class = "reg col-md-3"><a href = "images/reg4.PNG" class="fancybox" rel="gallery3"><img src = "images/reg4.PNG"></a></div>
                        </span>
                    </div>

                    <div class = "row">
                        <span id = "search_order1" hidden>
                            <div class = "reg col-md-3"><a href = "images/search_order1.png" class="fancybox" rel="gallery4"><img src = "images/search_order1.png"></a></div>
                            <div class = "reg col-md-3"><a href = "images/search_order2.png" class="fancybox" rel="gallery4"><img src = "images/search_order2.png"></a></div>
                            <div class = "reg col-md-3"><a href = "images/search_order3.png" class="fancybox" rel="gallery4"><img src = "images/search_order3.png"></a></div>
                            <div class = "reg col-md-3"><a href = "images/search_order4.png" class="fancybox" rel="gallery4"><img src = "images/search_order4.png"></a></div>
                        </span>
                    </div>

                    <div class = "row">
                        <span id = "send_order1" hidden>
                            <div class = "reg col-md-4"><a href = "images/send_order1.png" class="fancybox" rel="gallery5"><img src = "images/send_order1.png"></a></div>
                            <div class = "reg col-md-4"><a href = "images/send_order2.png" class="fancybox" rel="gallery5"><img src = "images/send_order2.png"></a></div>
                            <div class = "reg col-md-4"><a href = "images/send_order3.png" class="fancybox" rel="gallery5"><img src = "images/send_order3.png"></a></div>
                        </span>
                    </div>
                    
                    <h4><strong>* Если не нашли нужный Вам заказ, при его поступлении в систему, вам на электронную почту придет уведомление. <strong></h4></div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="col-md-10 col-md-offset-1 highlight top-space">
        <div class="row">
        <h4>
            <p>Зарегистрировавшись в профессиональной социальной сети INDUSTRIAL ORDER вам открывается доступ к огромному количеству инструментов для коммуникаций с потенциальными клиентами, поставщиками, партнерами. Автоматизированная система проведения сделок значительно ускоряет процесс выбора поставщика и снижает коррупцию при проведении торгов.</p>
            <p>INDUSTRIAL ORDER охватывает абсолютно все сферы бизнеса, включая крупнейшие мировые концерны, предприятия малого и среднего бизнеса различных государств, ремонтные компании, предприятия в сфере услуг, консалтинга и др. Взаимодействие и развитие бизнеса может осуществляться внутри простого интуитивно понятного интерфейса из любой точки планеты.</p>
        </h4>
        </div>
    </div>

</div>    <!-- /container --> 


<!-- Intro
<div id="bg1" class="container text-center">
    <h2 class="thin"></h2>
    <h3>ПРОФЕССИОНАЛЬНАЯ МЕЖДУНАРОДНАЯ СОЦИАЛЬНАЯ СЕТЬ.</h3>
    <div class="row">
        <div class="col-md-3 col-sm-6 highlight">
            <div class="h-caption"><h4><i class="fa fa-search fa-4x"></i> НАЙТИ ПАРТНЕРА</h4></div>
            <div class="h-body text-center">
                <p>Поиск клиентов и поставщиков. В базе: производства оборудования и комплектующих, компании из сферы
                    услуг, торговли, консалтинга и др.</p>
            </div>
        </div>
        <div class="col-md-3 col-sm-6 highlight">
            <div class="h-caption"><h4><i class="fa fa-rub fa-4x"></i> $</h4></div>
            <div class="h-body text-center">
                <p>Снижение издержек и увеличение прибыли за счет автоматизации бизнес-процессов посредством встроенной
                    CRM.</p>
            </div>
        </div>
        <div class="col-md-3 col-sm-6 highlight">
            <div class="h-caption"><h4><i class="fa fa-user-plus fa-4x"></i> ИНВЕСТИЦИИ И ПАРТНЕРЫ</h4></div>
            <div class="h-body text-center">
                <p>Возможность привлечения инвесторов и партнеров для совместных проектов из любой страны мира.</p>
            </div>
        </div>
        <div class="col-md-3 col-sm-6 highlight">
            <div class="h-caption"><h4><i class="fa fa-users fa-5x" aria-hidden="true"></i> НАЙТИ СОТРУДНИКА</h4></div>
            <div class="h-body text-center">
                <p>Встроенная база резюме и вакансий, доступная всем зарегистрированным пользователям. Сотрудники в
                    России и за рубежом.</p>
            </div>
        </div>
    </div>
</div>
/Intro-->';
    
$sql5 = "SELECT * from " . BEZ_DBPREFIX . "orders, " . BEZ_DBPREFIX . "companys WHERE bez_companys.id_company = bez_orders.o_company AND (end_date > CURRENT_DATE)";
$stmt5= $db ->prepare($sql5);
$stmt5 -> execute();
$rows5 = $stmt5 -> fetchall(PDO::FETCH_ASSOC);
$length = count($rows5);
$random = mt_rand(0, ($length-1));
$result1 = $rows5[$random]['o_section'];
//$table .= $rows5[$random]['id_company'];

$sql6 = "SELECT name FROM class_new WHERE id = :id";
$stmt6= $db ->prepare($sql6);
$stmt6 -> bindValue(':id', $result1, PDO::PARAM_STR);
$stmt6 -> execute();
$rows6 = $stmt6 -> fetch(PDO::FETCH_ASSOC);

        
    $mainText.='
    </div>
</div>
<!-- /Intro-->

<hr>

<div class="jumbotron text-center">
    <div class="container">
        <h2>Актуальный заказ</h2>
        <div class="row">
            <article class="post">
                <header class="entry-header" style = "background-color: #eee">';
if($user == true) {    
$address = $rows5[$random]['address'];
}
$table .= "<div class='panel panel-default' style = 'background-color: #eee'>";
$table .= "<div class='panel-heading' style = 'background-color: #eee; text-align:left''>";
$table .= "<span class='pull-right'>" . $address . "</span>";
$table .= "<h3 class='panel-title'><a style = 'color: blue' target='_blank' href='" . BEZ_HOST . "?mode=order_view&vid=" . $rows5[$random]['id_order'] . "'>" . $rows5[$random]['o_title'] . "</a></h3>";
$table .= "</div><div class='panel-body' style = 'text-align:left'>";
$table .= '<div class="col-md-8">';
$table .= $rows5[$random]['content'] . "</div>";
$table .= "<div class='col-md-4'><span class='pull-right'><ul>";
$table .= "<li>Дата добавления: " . date('d-m-Y', strtotime($rows5[$random]['add_date'])) . "</a></li>";
$table .= "</ul></span>";
$table .= "</div></div><div class='panel-footer' style = 'background-color: #eee; text-align:left''>" . $rows6['name'] . "</div>";
$table .= "</div>";
$mainText.= $table;

                $mainText.= '</header>
            </article>
        </div>
    </div>
</div>
            <!-- Intro -->
<div style = "background-color: #135999; margin-bottom: -60px;padding-left: 3%; max-height: 400px" class="jumbotron text-center">';
    //#0061ff   //#428bca
/*
    <h2 class="thin">БЕСПЛАТНАЯ регистрация в 1 клик!</h2>
    <h3>Уже через 5 минут получить доступ к базе компаний по всему миру и эффективным инструментам
        управления от INDUSTRIAL ORDER.</h3>
 */
 
    $sql2 = 'SELECT id_company FROM `bez_companys` WHERE c_logo IS NOT NULL';
    $stmt1 = $db->prepare($sql2);
    $stmt1->execute();
    $rows1 =  $stmt1 -> fetchAll(PDO::FETCH_ASSOC);
    $max = 64; //count($rows1)
    
      $mainText.= '<div class="row">';
      $mainText.= '<h2 style = "color: #fff">Наши клиенты:</h2>';
      $mainText.= '<div class = "cont">';
      
      $mainText.= '<table class = "slider">';
      $mainText.= '<tr>';
      $mainText.= '<ul>';
      $mainText.='<img id = "left_arrow" style= "width: 2%" src = "images/left.jpg">';
      $mainText.='<img id = "rigth_arrow" style= "width: 2%" src = "images/rigth.jpg">';
      $random = [];
      for($i = 0; $i < $max; $i++) {
    $random[] = rand(1, $max)-1;
    $sql = "SELECT c_name, c_logo, id_company FROM `bez_companys` WHERE c_logo IS NOT NULL LIMIT 1 OFFSET $random[$i]";  //WHERE c_logo NOT NULL
      $stmt = $db->prepare($sql);
      //$stmt->bindValue(':email', $_SESSION['login'], PDO::PARAM_STR);
      $stmt->execute();
      $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
      //$name = $rows[0]['c_name'];
      //$logo = $rows[0]["c_logo"];
          
      //?mode=company_view&vid=111
          $logo = $rows[0]["c_logo"];
          $id = $rows[0]["id_company"];
      $mainText.= '<td><a href = "?mode=company_view&vid=' . $id . '"><li><img class="img img-rounded carusel" style = "width: 50%" src = "uploads/logo/' . $logo . '"></li></a>';
      }
      $mainText.= '</ul>';
      $mainText.= '</tr>';
      $mainText.= '<tr>';
      $mainText.= '<ul>';
       for($i = 0; $i < $max; $i++) {
    $sql = "SELECT c_name, c_logo, id_company FROM `bez_companys` WHERE c_logo IS NOT NULL LIMIT 1 OFFSET $random[$i]";  //WHERE c_logo NOT NULL
      $stmt = $db->prepare($sql);
      //$stmt->bindValue(':email', $_SESSION['login'], PDO::PARAM_STR);
      $stmt->execute();
      $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
      //$name = $rows[0]['c_name'];
      //$logo = $rows[0]["c_logo"];
          
      
          $id = $rows[0]["id_company"];
      $mainText.= '<td  style = "color: #fff"><a style = "color: #fff" href = "?mode=company_view&vid=' . $id . '"><li>' . $rows[0]['c_name'] . '</li></a></td>';
      
       }
        $mainText.= '</ul>';
        $mainText.= '</tr>';
      $mainText.= '</table>';
      $mainText.= '</div>';
    
      $mainText.= '</div>';
    $mainText.= '</div>';
/*
$mainText.= '<!-- Social links. -->
<section id="social">
    <div class="container">
        <div class="wrapper clearfix">
            <!-- AddThis Button BEGIN -->
            <div>
            </div>
            <!-- AddThis Button END -->
        </div>
    </div>
</section>
<!-- /social links -->
';
 */
} else {
   $mainText = '
    <!-- Header -->
<header id="head">
    <div class="container">
        <div class="row">
            
            <h1 class="lead" style = "font-size: 70px">United bisness portal</h1>
            <p class="tagline">
                Professional international automated portal in B2B sphere. Find your customers, suppliers, partners in Russia and all over the world.
            </p>
            <div id = "registr">
            <p><a class="btn btn-action btn" style="width: 117px" role="button" href="' . BEZ_HOST . '?mode=auth">' . $btn_SignIn . '</a>
                <a class="btn btn-action btn" style="width: 117px" role="button"
                   href="' . BEZ_HOST . '?mode=reg">' . $btn_SignUp . '</a>
               <a class="btn btn-success btn" style="width: 117px" role="button"
                   href="' . BEZ_HOST . '?mode=reg_and_order_add&order=true">Create order</a></p>
            </div>
         <p class = "highlight"><h3>We are already: ' . $peoples . '</h3></p>   
        </div>
    </div>
</header>
<!-- /Header -->

<div class="container">
    <div class="row">
        <div class="col-md-10 highlight col-md-offset-1">
            <h4>INDUSTRIAL ORDER — innovative, comprehensive, one-of-a-kind product for enterprises of any level
or field of activity. Dozens of tools allow to meet any challenges in B2B sphere: from searching for
customers and suppliers to automating of business processes. Connecting thousands of companies
we allow to expand the boundaries of partnerships, reduce the costs of money and time, increase
profits excluding intermediate parties.
<div id = "How_this_work">
            <p><h2 style = "text-align: center"><strong>how it works:</strong></h2></p>
            <p><h3><strong>For those who need to place an order:</strong></h3></p>
     </div>     
    </div>
    </div>
    

<div id = "helper">
            <div class="row ">
            <div class="col-md-10 highlight col-md-offset-1">
            
            <div class="col-md-2 col-xs-4 ">
            <div class="h-caption"><h4><img width = "50%" src = "images/create_company.png"><br><br><a class ="register" href = "?mode=profile">Register your company</a></h4>
            </div>
        </div>
        
        <div class="col-md-1 col-xs-2 "><br><br>
        <img width = "50%" src = "images/arrow.png">
        </div>
        
        <div class="col-md-2 col-xs-4 ">
            <div class="h-caption"><h4><img width = "50%" src = "images/create_order.png"><br><br><a class= "register" href = "?mode=order_add">Place an order</a></h4>
            </div>
        </div>
        <div class="col-md-1 col-xs-2 "><br><br>
        <img width = "50%" src = "images/arrow.png">
        </div>
        
        <div class="col-md-2 col-xs-4">
            <div class="h-caption"><h4><img width = "50%" src = "images/discussion.png"><br><br><a class = "register" href = "?mode=my_pm">Get and choose the best commercial offers for you</a></h4></div>
        </div>
        <div class="col-md-1 col-xs-2"><br><br>
        <img width = "50%" src = "images/arrow.png">
        </div>
        <div class="col-md-2 col-xs-4">
            <div class="h-caption"><h4><img width = "50%" src = "images/according.png"><br><br> Close the deal</h4></div>
        </div>
                </div>
            </div>

<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <p><h3><strong>For those who are looking for orders:</strong></h3></p>
            
            
            <div class="row">
            <div class="col-md-2 col-sm-10 highlight" style = "margin-left: -3%">
            <div class="h-caption"><h4><img width = "50%" src = "images/create_company.png"><br><br><a class = "register" href = "?mode=profile">Register your company</a></h4></div>
            </div>
        
        <div class="col-md-1 col-xs-0 highlight"><br><br>
        <img width = "50%" src = "images/arrow.png">
        </div>
        
        <div class="col-md-2 col-xs-10 highlight">
            <div class="h-caption"><h4><img width = "50%" src = "images/user.png"><br><br>Find an order</h4></div>
        </div>
        <div class="col-md-1 col-xs-0 highlight"><br><br>
        <img width = "50%" src = "images/arrow.png">
        </div>
        <div class="col-md-2 col-xs-10 highlight">
            <div class="h-caption"><h4><img width = "50%" src = "images/view.png"><br><br>Submit your quotation</h4></div>
        </div>
        <div class="col-md-1 col-xs-0 highlight"><br><br>
        <img width = "50%" src = "images/arrow.png">
        </div>
        <div class="col-md-2 col-xs-10 highlight">
            <div class="h-caption"><h4><img width = "50%" src = "images/according.png"><br><br> Close the deal</h4></div>
        </div>
        <div class="container">
        <div class="col-md-12 highlight top-space col-md-offset-1" style = "margin-left: -2%">
       
        <h4><strong>* If you did not find the order you need, when it arrives at the system, you will be notified by e-mail. <strong></h4></div>
        </div>
            </div>
        </div>
    </div>
</div>



</div>

<!-- container -->
<div class="container">


<div class="col-md-10 col-md-offset-1 highlight top-space">
        <h4>
           <p>Having registered in professional social net INDUSTRIAL ORDER you getting the access to a huge
number of possibilities for communication with potential customers, suppliers, partners. Automated
transaction system significantly speeds up the process of selecting a supplier and reduces
corruption in tenders.</p>
            <p>INDUSTRIAL ORDER covers absolutely all business spheres, including the largest concerns, small
and medium-sized companies in different countries, repair companies, enterprises in the field of
services, consulting, etc. Cooperation and business development can be carry out within a simple
intelligible interface from anywhere in the world.</p>
        </h4>
    </div>

</div>    <!-- /container -->
    
<hr>



<!-- Intro
<div id="bg1" class="container text-center">
    <h2 class="thin"></h2>
    <h3>ПРОФЕССИОНАЛЬНАЯ МЕЖДУНАРОДНАЯ СОЦИАЛЬНАЯ СЕТЬ.</h3>
    <div class="row">
        <div class="col-md-3 col-sm-6 highlight">
            <div class="h-caption"><h4><i class="fa fa-search fa-4x"></i> НАЙТИ ПАРТНЕРА</h4></div>
            <div class="h-body text-center">
                <p>Поиск клиентов и поставщиков. В базе: производства оборудования и комплектующих, компании из сферы
                    услуг, торговли, консалтинга и др.</p>
            </div>
        </div>
        <div class="col-md-3 col-sm-6 highlight">
            <div class="h-caption"><h4><i class="fa fa-rub fa-4x"></i> $</h4></div>
            <div class="h-body text-center">
                <p>Снижение издержек и увеличение прибыли за счет автоматизации бизнес-процессов посредством встроенной
                    CRM.</p>
            </div>
        </div>
        <div class="col-md-3 col-sm-6 highlight">
            <div class="h-caption"><h4><i class="fa fa-user-plus fa-4x"></i> ИНВЕСТИЦИИ И ПАРТНЕРЫ</h4></div>
            <div class="h-body text-center">
                <p>Возможность привлечения инвесторов и партнеров для совместных проектов из любой страны мира.</p>
            </div>
        </div>
        <div class="col-md-3 col-sm-6 highlight">
            <div class="h-caption"><h4><i class="fa fa-users fa-5x" aria-hidden="true"></i> НАЙТИ СОТРУДНИКА</h4></div>
            <div class="h-body text-center">
                <p>Встроенная база резюме и вакансий, доступная всем зарегистрированным пользователям. Сотрудники в
                    России и за рубежом.</p>
            </div>
        </div>
    </div>
</div>
/Intro-->';
    
$sql5 = "SELECT * from " . BEZ_DBPREFIX . "orders, " . BEZ_DBPREFIX . "companys WHERE bez_companys.id_company = bez_orders.o_company AND (end_date > CURRENT_DATE)";
$stmt5= $db ->prepare($sql5);
$stmt5 -> execute();
$rows5 = $stmt5 -> fetchall(PDO::FETCH_ASSOC);
$length = count($rows5);
$random = mt_rand(0, ($length-1));
$result1 = $rows5[$random]['o_section'];
//$table .= $rows5[$random]['id_company'];


$sql6 = "SELECT name FROM class_new_eng WHERE id = :id";

$stmt6= $db ->prepare($sql6);
$stmt6 -> bindValue(':id', $result1, PDO::PARAM_STR);
$stmt6 -> execute();
$rows6 = $stmt6 -> fetch(PDO::FETCH_ASSOC);

        
    $mainText.='
    </div>
</div>
<!-- /Intro-->

<hr>

<div class="jumbotron text-center">
    <div class="container">
        <h2>Current order</h2>
        <div class="row">
            <article class="post">
                <header class="entry-header" style = "background-color: #eee">';
if($user == true) {    
$address = $rows5[$random]['address'];
}
$table .= "<div class='panel panel-default' style = 'background-color: #eee'>";
$table .= "<div class='panel-heading' style = 'background-color: #eee; text-align:left''>";
$table .= "<span class='pull-right'>" . $address . "</span>";
$table .= "<h3 class='panel-title'><a style = 'color: blue' target='_blank' href='" . BEZ_HOST . "?mode=order_view&vid=" . $rows5[$random]['id_order'] . "'>" . $rows5[$random]['o_title'] . "</a></h3>";
$table .= "</div><div class='panel-body' style = 'text-align:left'>";
$table .= '<div class="col-md-8">';
$table .= $rows5[$random]['content'] . "</div>";
$table .= "<div class='col-md-4'><span class='pull-right'><ul>";
$table .= "<li>Date added: " . date('d-m-Y', strtotime($rows5[$random]['add_date'])) . "</a></li>";
$table .= "</ul></span>";
$table .= "</div></div><div class='panel-footer' style = 'background-color: #eee; text-align:left''>" . $rows6['name'] . "</div>";
$table .= "</div>";
$mainText.= $table;
                $mainText.= '</header>
            </article>
        </div>
    </div>
</div>
            <!-- Intro -->
<div style = "background-color: #135999; margin-bottom: -60px;padding-left: 7%; max-height: 450px" class="jumbotron text-center">';
    //#0061ff   //#428bca
/*
    <h2 class="thin">БЕСПЛАТНАЯ регистрация в 1 клик!</h2>
    <h3>Уже через 5 минут получить доступ к базе компаний по всему миру и эффективным инструментам
        управления от INDUSTRIAL ORDER.</h3>
 */
 
    $sql2 = 'SELECT id_company FROM `bez_companys` WHERE c_logo IS NOT NULL';
    $stmt1 = $db->prepare($sql2);
    $stmt1->execute();
    $rows1 =  $stmt1 -> fetchAll(PDO::FETCH_ASSOC);
    $max = 64; //count($rows1)
    
      $mainText.= '<div class="row">';
      $mainText.= '<h2 style = "color: #fff">Our clients:</h2>';
      $mainText.= '<div class = "cont">';
      
      $mainText.= '<table class = "slider">';
      $mainText.= '<tr>';
      $mainText.= '<ul>';
      $mainText.='<img id = "left_arrow" style= "width: 2%" src = "images/left.jpg">';
      $mainText.='<img id = "rigth_arrow" style= "width: 2%" src = "images/rigth.jpg">';
      $random = [];
      for($i = 0; $i < $max; $i++) {
    $random[] = rand(1, $max)-1;
    $sql = "SELECT c_name, c_logo, id_company FROM `bez_companys` WHERE c_logo IS NOT NULL LIMIT 1 OFFSET $random[$i]";  //WHERE c_logo NOT NULL
      $stmt = $db->prepare($sql);
      //$stmt->bindValue(':email', $_SESSION['login'], PDO::PARAM_STR);
      $stmt->execute();
      $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
      //$name = $rows[0]['c_name'];
      //$logo = $rows[0]["c_logo"];
          
      //?mode=company_view&vid=111
          $logo = $rows[0]["c_logo"];
          $id = $rows[0]["id_company"];
      $mainText.= '<td><a href = "?mode=company_view&vid=' . $id . '"><li><img class="img img-rounded carusel" style = "width: 50%" src = "uploads/logo/' . $logo . '"></li></a>';
      }
      $mainText.= '</ul>';
      $mainText.= '</tr>';
      $mainText.= '<tr>';
      $mainText.= '<ul>';
       for($i = 0; $i < $max; $i++) {
    $sql = "SELECT c_name, c_logo, id_company FROM `bez_companys` WHERE c_logo IS NOT NULL LIMIT 1 OFFSET $random[$i]";  //WHERE c_logo NOT NULL
      $stmt = $db->prepare($sql);
      //$stmt->bindValue(':email', $_SESSION['login'], PDO::PARAM_STR);
      $stmt->execute();
      $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
      //$name = $rows[0]['c_name'];
      //$logo = $rows[0]["c_logo"];
          
      
          $id = $rows[0]["id_company"];
      $mainText.= '<td  style = "color: #fff"><a style = "color: #fff" href = "?mode=company_view&vid=' . $id . '"><li>' . $rows[0]['c_name'] . '</li></a></td>';
      
       }
        $mainText.= '</ul>';
        $mainText.= '</tr>';
      $mainText.= '</table>';
      $mainText.= '</div>';
    
      $mainText.= '</div>';
    $mainText.= '</div>';
/*
$mainText.= '<!-- Social links. -->
<section id="social">
    <div class="container">
        <div class="wrapper clearfix">
            <!-- AddThis Button BEGIN -->
            <div>
            </div>
            <!-- AddThis Button END -->
        </div>
    </div>
</section>
<!-- /social links -->
';
 */
}

/*
 * Зарегистрировать компанию самостоятельно
<div class ="col-sm-2" style="position:absolute; right:0px;top: 20%; display:none" id = "help">
    <div id ="close" style = "float: right;" onclick="closet()"><i class="fa fa-times-circle fa-2x" aria-hidden="true"></i>&nbsp;</div>
    <div class="panel panel-default">
        <div class='panel-heading' style = "background-color: #6dc96d">
            <small><?php echo $label_Helper; ?></small>
        </div>
        <div class='panel-body'>
        <form class="form-horizontal" action="" method="post">
            <div class="form-group">
                <div class="col-xs-10 col-xs-offset-1">
                <label for ="email">e-mail:</label>
                <input type="text" name ="email" placeholder="e-mail" class = "form-control" style="font-size:10px" required>
                </div>
            </div>
            <div class="form-group">
                <div class="col-xs-10 col-xs-offset-1">
                <label for = "site">web-сайт:</label>
                <input type="text" name ="site" placeholder="<?php echo $label_Website; ?>" id="inlineFormInput" class = "form-control" style="font-size:10px" required>
                </div>
            </div>
            <div class="form-group">
                <div class="col-xs-10 col-xs-offset-1">
                    <input type="submit" name ="reg_us" value="<?php echo $btn_Send ?>" class = "btn btn-primary">
                </div>
            </div>
        </form>
        </div>
    </div>
</div>
*/
?>
<script>
        $(document).ready(function() {
            $('.fancybox').fancybox();
        });
</script>