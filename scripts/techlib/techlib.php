<?php
$lib_query_section = "SELECT * FROM type_lib";
$lib_option_section = $db -> prepare($lib_query_section);
$lib_option_section -> execute();
$lib_select_section = $lib_option_section -> fetchAll(PDO::FETCH_ASSOC);

$summary = '';

foreach($lib_select_section as $lib_section) {
    $parent_id = $lib_section["id"];
    $lib_query_subsection = "SELECT * FROM class_lib WHERE parent_id = '$parent_id'";
    $lib_option_subsection = $db -> prepare($lib_query_subsection);
    $lib_option_subsection -> execute();
    $lib_select_subsection = $lib_option_subsection -> fetchAll(PDO::FETCH_ASSOC);
  
    $summary .= '<optgroup label ="' .  $lib_section["section"] . '">';      
        foreach($lib_select_subsection as $lib_subsection) {
        $summary .= '<option value ="' . $lib_subsection["id"] . '">' . $lib_subsection["subsection"] . '</option>';
        }
}
    $summary .= '</optgroup>';
    if ($user === false) {
    header('Location:' . BEZ_HOST . '?mode=auth');
    exit;
}
    
if ($user === true) {
    if(isset($_REQUEST["submit"])) {
            $folder = ROOT_DIR . '/uploads/techlib/';
            $ext = array(".gif", ".jpg", ".jpeg", ".png", ".tif", ".tiff", ".pdf", ".doc", ".docx", ".exl");
            $file_ext = strtolower(strrchr($_FILES['uploadLibFile']['name'], '.'));
                // Генерируем случайное число
                $file_name = uniqid(rand(10000, 99999));
                // Формируем путь на сервере
                $uploadedLibFile = $folder . $file_name . $file_ext;
                $fileName = $file_name . $file_ext;
            if (!in_array($file_ext, $ext) || $_FILES['uploadLibFile']['size'] > 2*1024*1024) {
                    echo "<div class=\"alert alert-danger\" role=\"alert\">Такое нельзя загружать.</div>";
                } else {
                    print_r($_FILES['uploadLibFile']);
                    $lib = $_REQUEST["lib"];
                    $sql3 = 'INSERT INTO `bez_techlib` SET file=:file, id_user=:userID, subsection=:subsection,
                            filename=:filename, description=:description'; // :r_comapny,
                    $stmt = $db->prepare($sql3);
                    $stmt->bindValue(':file', $fileName, PDO::PARAM_STR);
                    $stmt->bindValue(':userID', $id_reg, PDO::PARAM_INT);
                    $stmt->bindValue(':subsection', $lib, PDO::PARAM_INT);
                    $stmt->bindValue(':filename', $_REQUEST['filename'], PDO::PARAM_INT);
                    $stmt->bindValue(':description', $_REQUEST['short_desc_id'], PDO::PARAM_INT);
                    $stmt->execute();
                if (is_uploaded_file($_FILES['uploadLibFile']['tmp_name'])) {
                        if (move_uploaded_file($_FILES['uploadLibFile']['tmp_name'], $uploadedLibFile)) {
                            echo "<div class=\"alert alert-success\" role=\"alert\">Файл загружен.</div>";
                        } else {
                            echo "<div class=\"alert alert-danger\" role=\"alert\">Во время загрузки файла произошла ошибка.</div>";
                        }
                    } else {
                        echo "<div class=\"alert alert-danger\" role=\"alert\">Файл не загружен.</div>";
                    }
        }
}
}
?>

