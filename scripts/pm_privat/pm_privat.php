<?php
/**
 * Created by PhpStorm.
 * User: Алексей
 * Date: 31.10.2016
 * Time: 15:24
 */
$pageTitle = 'Просмотр сообщения';
$h1Title = 'Чтение входящего сообщения';
$pageDesc = '';

if ($user === false) {
    header('Location:' . BEZ_HOST . '?mode=auth');
    exit;
}
if ($user === true) {
    $pm_hash = htmlspecialchars(stripslashes($_REQUEST['pm_hash']));
    $from = htmlspecialchars(stripslashes($_REQUEST['from']));
    $sql5 = 'SELECT * FROM `' . BEZ_DBPREFIX . 'pm` WHERE (`u_to` = :u_to AND  u_from =:u_from) OR (u_to = :u_from AND u_from=:u_to) AND `pm_hash`=:pm_hash';  //AND `pm_hash`=:pm_hash
    $stmt5 = $db->prepare($sql5);
    $stmt5->bindValue(':pm_hash', $_GET['pm_hash']);
    //$stmt->bindValue(':pm_title', $_REQUEST['pm_title']);
    $stmt5->bindValue(':u_to', $_SESSION['id_reg']);
    $stmt5->bindValue(':u_from', $from);
    $stmt5->execute();
    $rows5 = $stmt5->fetchAll(PDO::FETCH_ASSOC);
    $order_num = urlencode($rows5['pm_title']);
    $tr = htmlspecialchars(stripslashes($_REQUEST['is_company']));
    
    if($tr == 1){
    foreach($rows5 as $val) {

  
       if(preg_match('/_/ism', $val['pm_hash']) == 0){
            continue;
        }
        
        $sql3 = 'SELECT id_reg, name, avatar, company_id FROM `' . BEZ_DBPREFIX . 'reg` WHERE `id_reg`=:admin';
        $stmt = $db->prepare($sql3);
        $stmt->bindValue(':admin', $val['u_from']); //, PDO::PARAM_INT
        $stmt->execute();
        $rows2 = $stmt->fetch(PDO::FETCH_ASSOC);
        $pm_from = $rows2['name'];
        $fromWho = $pm_from;
        $id_from = $val['u_from'];
        $id_to = $pm_to = $val['u_to'];
        $pm_subject = $val['pm_title'];
        $pm_id = $val['pm_id'];

        $sql_c = 'SELECT c_name FROM `' . BEZ_DBPREFIX . 'companys` WHERE `id_company` = :id';
        $stmt = $db->prepare($sql_c);
        $stmt->bindValue(':id', $rows2['company_id']);
        $stmt->execute();
        $rows_c = $stmt->fetchAll(PDO::FETCH_ASSOC);

        if ($rows2['avatar'] == NULL) {
            $reg_avatar = BEZ_HOST . 'uploads/avatars/user.jpg';
        } else {
            $reg_avatar = BEZ_HOST . 'uploads/avatars/' . htmlspecialchars($rows2['avatar']);
        }

        /* Вытаскиваем файлы */
        $sql_f = 'SELECT * from `bez_answer` WHERE `pmid` = :pmid';
        $stmt = $db->prepare($sql_f);
        $stmt->bindValue(':pmid', $pm_id, PDO::PARAM_STR);
        $stmt->execute();
        $rows_f = $stmt->fetchAll(PDO::FETCH_ASSOC);

        /* $sql9 = 'SELECT * FROM `' . BEZ_DBPREFIX . 'reg` WHERE `id_reg`=:id';
          $stmt = $db->prepare($sql9);
          $stmt->bindValue(':id', $rows2[0]['avatar'], PDO::PARAM_STR); //$_SESSION['login']
          $stmt->execute();
          $rows9 = $stmt->fetchAll(PDO::FETCH_ASSOC);
          if ($rows9[0]['avatar'] == NULL) {
          $reg_avatar = BEZ_HOST . 'uploads/avatars/user.jpg';
          } else {
          $reg_avatar = BEZ_HOST . 'uploads/avatars/' . htmlspecialchars($rows9[0]['avatar']);
          } */

        if ($val['pm_status'] == '0') {
            $m_align = '<div class="col-md-10">';
            $panel_info = 'panel-default';
        } else {
            $m_align = '<div class="col-md-10">'; //col-md-offset-2
            $panel_info = 'panel-info';
        }

        if ($val['pm_flag'] == '1' AND $val['u_from'] != $_SESSION['id_reg']) {
            $panel_info = 'panel-danger';
        }
        $messages .= $m_align;
        $messages .= '<div class="panel' . $panel_info . '"><div class="panel-heading">';
        $messages .= '<span class="pull-right">' . $label_DateAdding . ': ' . date('d-m-Y H:i', strtotime($val['pm_date'])) . '</span>';
        $messages .= '<h3 class="panel-title">' . $val['pm_title'] . '</h3>' . $label_From . ' <a href="?mode=company_view&vid=' . $rows2['company_id'] . '">' . $fromWho . ' (' . $rows_c[0]['c_name'] . ')</a></div>';
        $messages .= '<div class="panel-body"><span class="pull-left"><img src="' . $reg_avatar . '" width="50px"></span>' . nl2br($val['pm_message']) . '<div class="clearfix"></div><hr>';

        foreach ($rows_f as $row_f) {
            $messages .= '<a href="' . BEZ_HOST . 'uploads/answers/' . $row_f['file'] . '" target = _blank>' . $row_f['file'] . '</a><hr>';
        }
        //$messages .= '<span class="pull-right"><button class = "btn btn-primary dialog">Ответить</button></span></div>';
        $messages .= '</div>';
        $messages .= '</div>';
        $messages .= '</div>';
    
    
    }
    } else {   
         foreach($rows5 as $val) {
             
         
        
        
    $sql4 = 'SELECT * FROM `' . BEZ_DBPREFIX . 'pm` WHERE (`u_to` = :u_to AND  u_from =:u_from) OR (u_to = :u_from AND u_from=:u_to) AND `pm_title`=:pm_title ORDER BY pm_id DESC';
    $stmt4 = $db->prepare($sql4);
    //$stmt4->bindValue(':pm_hash', $_GET['pm_hash']);
    $stmt4->bindValue(':pm_title', $val['pm_title']);
    $stmt4->bindValue(':u_to', $_SESSION['id_reg']);
    $stmt4->bindValue(':u_from', $from);
    $stmt4->execute();
    $rows4 = $stmt4->fetchall(PDO::FETCH_ASSOC);
    //$messages .=  $rows4[0]['pm_title'];

    
    /*
      $sql2 = 'SELECT * FROM `' . BEZ_DBPREFIX . 'pm` WHERE `pm_title`=:pm_title AND  (u_to=:u_from OR u_from=:u_from) ORDER BY pm_id DESC';
      $stmt = $db->prepare($sql2);
      //$stmt->bindValue(':pm_hash', $_GET['pm_hash']);
      $stmt->bindValue(':pm_title', $rows4['pm_title']);
      $stmt->bindValue(':u_to', $_SESSION['id_reg']);
      $stmt->bindValue(':u_from', $_REQUEST['from']);
      $stmt->execute();
      $rows2 = $stmt->fetchAll(PDO::FETCH_ASSOC);
     */
    /* TODO: Выборка сообщений по pm_hash, вывод всех откликов по данному заказу. Проверка на разрешение чтения сообщения. */

    foreach ($rows4 as $val) {
         if(preg_match('/_/ism', $val['pm_hash'])){
            continue;
        }
        $sql3 = 'SELECT id_reg, name, avatar, company_id FROM `' . BEZ_DBPREFIX . 'reg` WHERE `id_reg`=:admin';
        $stmt = $db->prepare($sql3);
        $stmt->bindValue(':admin', $val['u_from']); //, PDO::PARAM_INT
        $stmt->execute();
        $rows2 = $stmt->fetch(PDO::FETCH_ASSOC);
        $pm_from = $rows2['name'];
        $fromWho = $pm_from;
        $id_from = $val['u_from'];
        $id_to = $pm_to = $val['u_to'];
        $pm_subject = $val['pm_title'];
        $pm_id = $val['pm_id'];

        $sql_c = 'SELECT c_name FROM `' . BEZ_DBPREFIX . 'companys` WHERE `id_company` = :id';
        $stmt = $db->prepare($sql_c);
        $stmt->bindValue(':id', $rows2['company_id']);
        $stmt->execute();
        $rows_c = $stmt->fetchAll(PDO::FETCH_ASSOC);

        if ($rows2['avatar'] == NULL) {
            $reg_avatar = BEZ_HOST . 'uploads/avatars/user.jpg';
        } else {
            $reg_avatar = BEZ_HOST . 'uploads/avatars/' . htmlspecialchars($rows2['avatar']);
        }

        /* Вытаскиваем файлы */
        $sql_f = 'SELECT * from `bez_answer` WHERE `pmid` = :pmid';
        $stmt = $db->prepare($sql_f);
        $stmt->bindValue(':pmid', $pm_id, PDO::PARAM_STR);
        $stmt->execute();
        $rows_f = $stmt->fetchAll(PDO::FETCH_ASSOC);

        /* $sql9 = 'SELECT * FROM `' . BEZ_DBPREFIX . 'reg` WHERE `id_reg`=:id';
          $stmt = $db->prepare($sql9);
          $stmt->bindValue(':id', $rows2[0]['avatar'], PDO::PARAM_STR); //$_SESSION['login']
          $stmt->execute();
          $rows9 = $stmt->fetchAll(PDO::FETCH_ASSOC);
          if ($rows9[0]['avatar'] == NULL) {
          $reg_avatar = BEZ_HOST . 'uploads/avatars/user.jpg';
          } else {
          $reg_avatar = BEZ_HOST . 'uploads/avatars/' . htmlspecialchars($rows9[0]['avatar']);
          } */

        if ($val['pm_status'] == '0') {
            $m_align = '<div class="col-md-10">';
            $panel_info = 'panel-default';
        } else {
            $m_align = '<div class="col-md-10">'; //col-md-offset-2
            $panel_info = 'panel-info';
        }

        if ($val['pm_flag'] == '1' AND $val['u_from'] != $_SESSION['id_reg']) {
            $panel_info = 'panel-danger';
        }

        $messages .= $m_align;
        $messages .= '<div class="panel' . $panel_info . '"><div class="panel-heading">';
        $messages .= '<span class="pull-right">' . $label_DateAdding . ': ' . date('d-m-Y H:i', strtotime($val['pm_date'])) . '</span>';
        $messages .= '<h3 class="panel-title">' . $val['pm_title'] . '</h3>' . $label_From . ' <a href="?mode=company_view&vid=' . $rows2['company_id'] . '">' . $fromWho . ' (' . $rows_c[0]['c_name'] . ')</a></div>';
        $messages .= '<div class="panel-body"><span class="pull-left"><img src="' . $reg_avatar . '" width="50px"></span>' . nl2br($val['pm_message']) . '<div class="clearfix"></div><hr>';

        foreach ($rows_f as $row_f) {
            $messages .= '<a href="' . BEZ_HOST . 'uploads/answers/' . $row_f['file'] . '" target = _blank>' . $row_f['file'] . '</a><hr>';
        }
        //$messages .= '<span class="pull-right"><button class = "btn btn-primary dialog">Ответить</button></span></div>';
        $messages .= '</div>';
        $messages .= '</div>';
        $messages .= '</div>';
        }
    }
    }
    
    
    $sql = 'UPDATE `' . BEZ_DBPREFIX . 'pm` SET `pm_flag`=:pm_flag WHERE `pm_hash`=:pm_hash AND u_to=:u_to';
    //Подготавливаем PDO выражение для SQL запроса
    $stmt = $db->prepare($sql);
    $stmt->bindValue(':u_to', $_SESSION['id_reg'], PDO::PARAM_STR);
    $stmt->bindValue(':pm_flag', '1', PDO::PARAM_STR);
    $stmt->bindValue(':pm_hash', $_REQUEST['pm_hash'], PDO::PARAM_STR);
    $stmt->execute();
   
    // TODO: добавить запись файла и id сообщения для этого файла
    /*
      if (isset($_POST['submit'])) {
      $sql_w = 'INSERT INTO `bez_pm` SET u_from = :u_from, u_to = :u_to, pm_title = :pm_title, pm_message = :pm_message, pm_flag = :pm_flag, pm_status = :pm_status, pm_hash = :pm_hash';
      $stmt_w = $db->prepare($sql_w);
      $stmt_w->bindValue(':u_from', $id_to);
      $stmt_w->bindValue(':u_to', $id_from);
      $stmt_w->bindValue(':pm_title', $pm_subject);
      $stmt_w->bindValue(':pm_message', $_POST['pm_answer']); //, PDO::PARAM_STR
      $stmt_w->bindValue(':pm_flag', '0');
      $stmt_w->bindValue(':pm_status', '1');
      $stmt_w->bindValue(':pm_hash', $pm_id);
      $stmt_w->execute();
     */


    
    
    if (isset($_REQUEST['submit'])) {
        
    $sql_title = 'SELECT * FROM `' . BEZ_DBPREFIX . 'pm` WHERE pm_hash=:pm_hash';
    $stmt_title = $db->prepare($sql_title);
    $stmt_title->bindValue(':pm_hash', $_GET['pm_hash']);
    $stmt_title->execute();
    $rows_title = $stmt_title->fetch(PDO::FETCH_ASSOC);
    
        $mess = htmlspecialchars(stripslashes($_REQUEST['mess']));
        $sql_w = 'INSERT INTO `bez_pm` SET u_from = :u_from, u_to = :u_to, pm_title = :pm_title, pm_message = :pm_message, pm_flag = :pm_flag, pm_status = :pm_status, pm_hash = :pm_hash';
        $stmt_w = $db->prepare($sql_w);
        $stmt_w->bindValue(':u_from', $_SESSION['id_reg']);
        $stmt_w->bindValue(':u_to', $from);  // 399 $id_from
        $stmt_w->bindValue(':pm_title', $rows_title['pm_title']);  //$pm_subject
        $stmt_w->bindValue(':pm_message', $mess); //, PDO::PARAM_STR   $_POST['pm_answer']
        $stmt_w->bindValue(':pm_flag', '0');
        $stmt_w->bindValue(':pm_status', '0');
        $stmt_w->bindValue(':pm_hash', $_GET['pm_hash']); 
        $stmt_w->execute();
        
    $sql6 = 'SELECT login FROM `' . BEZ_DBPREFIX . 'reg` WHERE `id_reg`=:id_reg';
    $stmt6 = $db->prepare($sql6);
    $stmt6->bindValue(':id_reg', $from, PDO::PARAM_STR);
    $stmt6->execute();
    $stmt_res6 = $stmt6 -> fetch(PDO::FETCH_ASSOC);
    
    
    $to= $stmt_res6['login'];
    $subject = 'Новое сообщение';
    $message = 'Здравствуйте.' . PHP_EOL . 'Вам написали новое сообщение. Вы можете прочитать его в своем профиле на сайте'.  PHP_EOL . 'https://industrialorder.com';
    $headers = 'From: robot@industrialorder.com' . PHP_EOL .
    'Reply-To: robot@industrialorder.com' . PHP_EOL;
    mail($to, $subject, $message, $headers);
        
        $newID = $db->lastInsertId();
        $ext = array(".gif", ".jpg", ".jpeg", ".png", ".tif", ".pdf", ".xls", ".xlsx", ".doc", ".docx", ".odt", ".zip", ".rar", ".pdf");
        $folder = ROOT_DIR . '/uploads/answers/';
        $result = count($_FILES);

        if ($result > 20) {
            echo '<div class="alert alert-danger" role="alert">Слишком много файлов!</div>';
        } else {
            for ($i = 0; $i < count($_FILES['message']['name']); $i++) {
                // Получаем расширение файла
                $file_ext[$i] = strtolower(strrchr($_FILES['message']['name'][$i], '.'));
                // Генерируем случайное число
                $file_name = uniqid(rand(10000, 99999));
                // Формируем путь на сервере
                $uploadedFile[$i] = $folder . $file_name . $file_ext[$i];
                $fileName = $file_name . $file_ext[$i];
                if (!in_array($file_ext[$i], $ext)) {
                    echo "<div class=\"alert alert-danger\" role=\"alert\">Такое нельзя загружать.</div>";
                } else {

                    $sql4 = 'INSERT INTO `bez_answer` SET file=:file, order_num=:order_num, pmid=:pmid'; // :r_comapny,
                    $stmt1 = $db->prepare($sql4);
                    $stmt1->bindValue(':file', $fileName, PDO::PARAM_STR);
                    $stmt1->bindValue(':order_num', $val['pm_hash'], PDO::PARAM_INT);
                    $stmt1->bindValue(':pmid', $newID, PDO::PARAM_INT);
                    $stmt1->execute();
                    if (is_uploaded_file($_FILES['message']['tmp_name'][$i])) {
                        if (move_uploaded_file($_FILES['message']['tmp_name'][$i], $uploadedFile[$i])) {
                            echo "<div class=\"alert alert-success\" role=\"alert\">Файл загружен.</div>";
                        } else {
                            echo "<div class=\"alert alert-danger\" role=\"alert\">Во время загрузки файла произошла ошибка.</div>";
                        }
                    } else {
                        echo "<div class=\"alert alert-danger\" role=\"alert\">Файл не загружен.</div>";
                    }
                }
            }
        }
        header('Location:' . BEZ_HOST . '?mode=pm_privat&pm_hash=' . $pm_hash . '&from=' . $from);
    }
}
?>

<script>

    /*
     $(function() {    
     $("#dialog").dialog({
     zIndex: 4,
     minWidth: 800,
     minHeight: 450,
     autoOpen: false,
     
     });
     
     $(".dialog").click(function() {
     $("#dialog").dialog( "open" );
     });
     });   */
</script>