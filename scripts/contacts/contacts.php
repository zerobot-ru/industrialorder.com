<?php
/**
 * Обработчик формы регистрации
 */

if ($_SESSION['currentLng'] == "ru-ru") {
    $pageTitle = 'Форма обратной связи' . $label_PTitle;
    $pageDesc = 'Связь с администрацией сайта industrialorder.com';
} else {
    $pageTitle = 'Feedback form' . $label_PTitle;
    $pageDesc = 'Contact with the site administration';
}

/*
$skype='
<script type="text/javascript" src="https://secure.skypeassets.com/i/scom/js/skype-uri.js"></script>
<div id="SkypeButton_Call_info_655692_1">
 <script type="text/javascript">
 Skype.ui({
 "name": "chat",
 "element": "SkypeButton_Call_info_655692_1",
 "participants": ["info_655692"],
 "imageSize": 32
 });
 </script>
</div>
';
*/

// Выводим сообщение об удачной регистрации
if (isset($_GET['status']) and $_GET['status'] == 'ok')
    echo '<div class="alert alert-success" role="alert"><b>Сообщение отправлено.</b></div>';

include 'scripts/contacts/contacts.html';

/* Если нажата кнопка на регистрацию,
  начинаем проверку */
if (isset($_POST['submit'])) {
    // Утюжим пришедшие данные
    if (empty($_POST['email']))
        $err[] = '<div class="alert alert-danger" role="alert">Поле Email не может быть пустым!</div>';
    else {
        if (emailValid($_POST['email']) === false)
            $err[] = '<div class="alert alert-danger" role="alert"><span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span><span class="sr-only">Error:</span>Не правильно введен E-mail</div>' . "\n";
    }

    if (empty($_POST['name']))
        $err[] = '<div class="alert alert-danger" role="alert">Поле Имя не может быть пустым!</div>';

    if (empty($_POST['message']))
        $err[] = '<div class="alert alert-danger" role="alert">Поле Сообщение не может быть пустым!</div>';

    // Проверяем наличие ошибок и выводим пользователю
    if (count($err) > 0)
        echo showErrorMessage($err);
    else {

        // Проверяем наличие ошибок и выводим пользователю
        if (count($err) > 0)
            echo showErrorMessage($err);
        else {

            // Отправляем письмо для активации
            $title = 'Сообщение с сайта industrialorder.com';
            $message = trim($_POST['name']) . ' <' . trim($_POST['email']) . '><br><hr><br>' . nl2br($_POST['message']);

            sendMessageMail('info@industrialorder.com', BEZ_MAIL_AUTOR, $title, $message);

            // Сбрасываем параметры
            header('Location:' . BEZ_HOST . '?mode=contacts&status=ok');
            exit;
        }
    }

}
?>