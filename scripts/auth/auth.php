<?php

/**
 * Обработчик формы авторизации
 */

if ($_SESSION['currentLng'] == "ru-ru") {
    $pageTitle = 'Вход на сайт' . $label_PTitle;
    $pageDesc = 'Вход на сайт. Найдите своих клиентов и поставщиков, производителей оборудования и комплектующих, партнеров и инвесторов, сотрудников. В России и по всему Миру.';
} else {
    $pageTitle = 'Sign in page' . $label_PTitle;
    $pageDesc = 'Sign in page. Find your customers and suppliers, manufacturers of equipment and components, partners and investors, employees. In Russia and around the world.';
}

// Выход из авторизации
if (isset($_GET['exit']) == true) {
setcookie('id_reg', $_SESSION['id_reg'], time()-3600, '/', 'industrialorder.com');
setcookie('user', $_SESSION['user'], time()-3600, '/', 'industrialorder.com');
setcookie('userlogin', $_SESSION['userlogin'], time()-3600, '/', 'industrialorder.com');
    // Уничтожаем сессию
    session_unset();
    if (session_destroy()) {

        // Делаем редирект
        header('Location:' . BEZ_HOST . '?mode=index');
        exit;
    } else {
        echo "exit error.";
    }
}

if ($user === FALSE) {
    include 'scripts/auth/auth_form.html';
} else {
    /* $err[] = 'Вы уже залогированы.';
    if (count($err) > 0) {
        echo showErrorMessage($err);
    } */
    header('Location:' . BEZ_HOST . '?mode=profile');
}

// Если нажата кнопка то обрабатываем данные
if (isset($_POST['submit'])) {
    // Проверяем на пустоту
    if (empty($_POST['email'])) {
        $err[] = 'Не введен Логин.';
    }

    if (empty($_POST['pass'])) {
        $err[] = 'Не введен Пароль.';
    }

    // Проверяем email
    if (emailValid($_POST['email']) === false) {
        $err[] = 'Не корректный E-mail.';
    }

    // Проверяем наличие ошибок и выводим пользователю
    if (count($err) > 0)
        echo showErrorMessage($err);
    else {
        /* Создаем запрос на выборку из базы
          данных для проверки подлинности пользователя */
        $sql = 'SELECT * FROM `' . BEZ_DBPREFIX . 'reg` WHERE `login` = :email';
        // Подготавливаем PDO выражение для SQL запроса
        $stmt = $db->prepare($sql);
        $stmt->bindValue(':email', $_POST['email'], PDO::PARAM_STR);
        $stmt->execute();

        // Получаем данные SQL запроса
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

        // Если логин совпадает, проверяем пароль
        if (count($rows) > 0) {

            // Получаем данные из таблицы
            if (md5($_POST['pass']) == $rows[0]['pass']) { // изменение
                $_SESSION['user'] = true;
                $_SESSION['allready'] = $rows[0]['allready'];
                $_SESSION['login'] = $rows[0]['login']; /* $_POST['email'] */
                $_SESSION['r_company'] = $rows[0]['company_id'];
                $_SESSION['id_reg'] = $rows[0]['id_reg'];
                $_SESSION['user_name'] = $rows[0]['name'];
                if ($rows[0]['avatar'] == NULL) {
                    $_SESSION['reg_avatar'] = BEZ_HOST . 'uploads/avatars/user.jpg';
                } else {
                    $_SESSION['reg_avatar'] = BEZ_HOST . 'uploads/avatars/' . htmlspecialchars($rows[0]['avatar']);
                }
                if ($rows[0]['defaultLng'] == NULL) {
                    $_SESSION['currentLng'] = chkLng();
                    if (chkLng() == "ru-ru") {
                        $_SESSION['rusLngSel'] = "selected";
                    } else {
                        $_SESSION['engLngSel'] = "selected";
                    }
                } else {
                    if ($rows[0]['defaultLng'] == "ru-ru") {
                        $_SESSION['currentLng'] = "ru-ru";
                        $_SESSION['rusLngSel'] = "selected";
                    } else {
                        $_SESSION['currentLng'] = "en-en";
                        $_SESSION['engLngSel'] = "selected";
                    }
                }

                $sql3 = 'UPDATE `bez_reg` SET `ssid` = :ssid, `last_login` = :last_login WHERE `id_reg` = :id_reg'; // :r_comapny,
                $stmt = $db->prepare($sql3);
                $stmt->bindValue(':ssid', session_id(), PDO::PARAM_STR);
                $stmt->bindValue(':id_reg', $rows[0]['id_reg'], PDO::PARAM_INT);
                $stmt->bindValue(':last_login', date("Y-m-d H:i:s"), PDO::PARAM_INT);
                $stmt->execute();

                $sql4 = 'UPDATE `bez_reg` SET `ip` = :ip WHERE `id_reg` = :id_reg'; // :r_comapny,
                $stmt = $db->prepare($sql4);
                $stmt->bindValue(':ip', $_SERVER['REMOTE_ADDR'], PDO::PARAM_STR);
                $stmt->bindValue(':id_reg', $rows[0]['id_reg'], PDO::PARAM_INT);
                $stmt->execute();

                // Сбрасываем параметры
                header('Location:' . BEZ_HOST . '?mode=profile');
                exit;
            } else
                echo showErrorMessage('Неверный пароль!');
        } else {
            echo showErrorMessage('Логин <b>' . $_POST['email'] . '</b> не найден!');
        }
    }
}
?>