<?php
/**
 * Created by PhpStorm.
 * User: Алексей
 * Date: 19.04.2017
 * Time: 13:01
 */

if ($_REQUEST['selLng'] == 'ru-ru') {
    $_SESSION['currentLng'] = "ru-ru";
    $_SESSION['rusLngSel'] = "selected";
    $_SESSION['engLngSel'] = "";
    if ($user == true) {
        $sql3 = 'UPDATE `bez_reg` SET `defaultLng` = :defaultLng WHERE `id_reg` = :id_reg'; // :r_comapny,
        $stmt = $db->prepare($sql3);
        $stmt->bindValue(':defaultLng', $_SESSION['currentLng'], PDO::PARAM_STR);
        $stmt->bindValue(':id_reg', $_SESSION['id_reg'], PDO::PARAM_INT);
        $stmt->execute();
    }
} else {
    $_SESSION['currentLng'] = "en-en";
    $_SESSION['engLngSel'] = "selected";
    $_SESSION['rusLngSel'] = "";
    if ($user == true) {
        $sql3 = 'UPDATE `bez_reg` SET `defaultLng` = :defaultLng WHERE `id_reg` = :id_reg'; // :r_comapny,
        $stmt = $db->prepare($sql3);
        $stmt->bindValue(':defaultLng', $_SESSION['currentLng'], PDO::PARAM_STR);
        $stmt->bindValue(':id_reg', $_SESSION['id_reg'], PDO::PARAM_INT);
        $stmt->execute();
    }
}
header('Location:https://industrialorder.com' . $_SERVER['REQUEST_URI']);

?>