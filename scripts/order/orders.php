<?php

$pageTitle = 'Мои заказы';
$h1Title = 'Все заказы пользователя';
$pageDesc = '';

/**
 * Обработчик вывода заказов пользователя
 * Site: http://kazin.pw
 * Вывод заказов пользователя для редактирования
 */

//Проверяем зашел ли пользователь
if ($user === false) {
    header('Location:' . BEZ_HOST . '?mode=error&errorNum=8');
    exit;
}
if ($_SESSION['allready'] === NULL) {
    header('Location:' . BEZ_HOST . '?mode=error&errorNum=7');
    exit;
} else
    if ($user === true) {
        if ($delID = @$_REQUEST['did']) {
            $sql3 = 'DELETE FROM `bez_orders` WHERE `id_order`=:id_order AND `o_company`=:r_company';
            $stmt = $db->prepare($sql3);
            $stmt->bindValue(':id_order', $delID);
            $stmt->bindValue(':r_company', $_SESSION['r_company']);
            $stmt->execute();
            //header('Location:' . BEZ_HOST . '?mode=error&errorNum=5');
            //exit;
        }
        //Запрос на выборку контента согласно названию компании
        $sql = 'SELECT * FROM `' . BEZ_DBPREFIX . 'orders` WHERE `o_company`=:r_company AND DATE(end_date)>=CURDATE() ORDER BY id_order DESC';
        $stmt = $db->prepare($sql);
        $stmt->bindValue(':r_company', $_SESSION['r_company'], PDO::PARAM_STR);
        //Выводим контент
        if ($stmt->execute()) {
            $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
            foreach ($rows as $val) {
                $folder = 'uploads/orders/';
                $sql2 = 'SELECT * FROM `' . BEZ_DBPREFIX . 'files` WHERE `order_num`=:order_num';
                $stmt = $db->prepare($sql2);
                $stmt->bindValue(':order_num', $val['id_order'], PDO::PARAM_INT);
                $stmt->execute();
                $rows2 = $stmt->fetchAll(PDO::FETCH_ASSOC);
                $result = count($rows2);

                $s_select = explode(", ", $val['o_section']);
                /* foreach ($s_select as $val1) {
                    $sql1 = 'SELECT * FROM `' . BEZ_DBPREFIX . 'cat` WHERE cat_id=' . $val1; // . ' ORDER BY cat_id ASC'
                    $stmt1 = $db->prepare($sql1);
                    //Выводим контент
                    if ($stmt1->execute()) {
                        $rows1 = $stmt1->fetchAll(PDO::FETCH_ASSOC);
                        foreach ($rows1 as $val11) {
                            $section1 .= '<span class="label label-default">' . $val11['cat_name'] . '</span> ';
                        }
                    }
                } */

                foreach ($s_select as $val1){
                    if($_SESSION['currentLng']=="ru-ru"){
                        $sql1 = 'SELECT * FROM `class_new` WHERE id=' . $val1;
                    } else {
                        $sql1 = 'SELECT * FROM `class_new_eng` WHERE id=' . $val1;
                    }
                    //$sql1='SELECT * FROM `class_okved` WHERE id=' . $val1;
                    $stmt1=$db->prepare($sql1);
                    if($stmt1->execute()){
                        $rows1=$stmt1->fetchAll(PDO::FETCH_ASSOC);
                        foreach ($rows1 as $val11){
                            $section1.='<strong><span class="label label-default">' . $val11['name'] . '</span>&nbsp</strong>';
                        }
                    }
                }

                $table .= "<div class='panel panel-default'>";
                $table .= "<div class='panel-heading'>";
                $table .= "<span class='pull-right'>" . $label_EndDate . ": " . date('d-m-Y', strtotime($val['end_date'])) . "</span><h3 class='panel-title'><a href='" . BEZ_HOST . "?mode=order_view&vid=" . $val['id_order'] . "'>Заказ №" . $val['id_order'] . " от " . date('d-m-Y', strtotime($val['add_date'])) . "</a></h3>";
                $table .= "</div>";
                $table .= "<div class='panel-body'>" . $val['content'] . "<div class='pull-right'><a href='" . BEZ_HOST . "?mode=order_view&vid=" . $val['id_order'] . "' class='btn btn-info btn-xs'>" . $btn_View . "</a> <a href=" . BEZ_HOST . "?mode=order_edit&eid=" . $val['id_order'] . " class='btn btn-warning btn-xs'>" . $btn_Edit . "</a> <a href=" . BEZ_HOST . "?mode=orders&did=" . $val['id_order'] . " class='btn btn-danger btn-xs'>" . $btn_Delete . "</a></span></div></div>";
                $table .= "<div class='panel-footer'><span class='pull-right'>Файлов: " . $result . "</span>" . $section1 . "</div>";
                $table .= "</div>";
                $section1 = '';
            }
            //$table .= "</div>";
        }

        //Запрос на выборку контента согласно названию компании
        $sql5 = 'SELECT * FROM `' . BEZ_DBPREFIX . 'orders` WHERE `o_company`=:r_company AND DATE(end_date)<CURDATE() ORDER BY id_order DESC';
        $stmt = $db->prepare($sql5);
        $stmt->bindValue(':r_company', $_SESSION['r_company'], PDO::PARAM_STR);
        //Выводим контент
        if ($stmt->execute()) {
            $rows5 = $stmt->fetchAll(PDO::FETCH_ASSOC);
            foreach ($rows5 as $val) {
                $folder = 'uploads/orders/';
                $sql6 = 'SELECT * FROM `' . BEZ_DBPREFIX . 'files` WHERE `order_num`=:order_num';
                $stmt = $db->prepare($sql6);
                $stmt->bindValue(':order_num', $val['id_order'], PDO::PARAM_INT);
                $stmt->execute();
                $rows6 = $stmt->fetchAll(PDO::FETCH_ASSOC);
                $result2 = count($rows6);

                $s_select = explode(", ", $val['o_section']);
                /* foreach ($s_select as $val1) {
                    $sql1 = 'SELECT * FROM `' . BEZ_DBPREFIX . 'cat` WHERE cat_id=' . $val1; // . ' ORDER BY cat_id ASC'
                    $stmt1 = $db->prepare($sql1);
                    //Выводим контент
                    if ($stmt1->execute()) {
                        //$summary = '';
                        $rows1 = $stmt1->fetchAll(PDO::FETCH_ASSOC);
                        foreach ($rows1 as $val11) {
                            $section2 .= '<span class="label label-default">' . $val11['cat_name'] . '</span> ';
                        }
                    }
                } */

                foreach ($s_select as $val1){
                    if($_SESSION['currentLng']=="ru-ru"){
                        $sql1 = 'SELECT * FROM `class_new` WHERE id=' . $val1;
                    } else {
                        $sql1 = 'SELECT * FROM `class_new` WHERE id=' . $val1;
                    }
                    //$sql1='SELECT * FROM `class_okved` WHERE code=' . $val1;
                    $stmt1=$db->prepare($sql1);
                    if($stmt1->execute()){
                        $rows1=$stmt1->fetchAll(PDO::FETCH_ASSOC);
                        foreach ($rows1 as $val11){
                            $section2.='<strong>' . $val11['name'] . '</strong>';
                        }
                    }
                }

                //$table .= "<tr class=''>";
                //$table .= "<td>" . $val['id_order'] . "</td>";
                //$table .= "<td>" . $val['content'] . "</td>";
                //$table .= "<td>" . $val['o_type'] . "</td>";
                //$table .= "<td>" . $val['o_section'] . "</td>";
                //$table .= "<td>" . date('d.m.Y', strtotime($val['add_date'])) . "</td>";
                //$table .= "<td>" . date('d.m.Y', strtotime($val['end_date'])) . "</td>";
                //$table .= "<td>" . $result . "</td>";
                //$table .= "<td valign='middle' align='center'><a href=" . BEZ_HOST . "?mode=order_edit&eid=" . $val['id_order'] . "><span class='glyphicon glyphicon-remove'></span></a></td>";
                //$table .= "<td valign='middle' align='center'><a href=" . BEZ_HOST . "?mode=orders&did=" . $val['id_order'] . "><span class='glyphicon glyphicon-remove'></span></a></td>";
                //$table .= "</tr>";
                //////////////////////////////////////////////////////
                $table2 .= "<div class='panel panel-default'>";
                $table2 .= "<div class='panel-heading'>";
                $table2 .= "<span class='pull-right'>Дата окончания: " . date('d-m-Y', strtotime($val['end_date'])) . "</span><h3 class='panel-title'><a href='" . BEZ_HOST . "?mode=order_view&vid=" . $val['id_order'] . "'>Заказ №" . $val['id_order'] . " от " . date('d-m-Y', strtotime($val['add_date'])) . "</a></h3>";
                $table2 .= "</div>";
                //$table .= "<td>" . $cName . "</td>";
                $table2 .= "<div class='panel-body'>" . $val['content'] . "<div class='pull-right'><a href='" . BEZ_HOST . "?mode=order_view&vid=" . $val['id_order'] . "' class='btn btn-info btn-xs'>" . $btn_View . "</a></span></div></div>";
                $table2 .= "<div class='panel-footer'><span class='pull-right'>Файлов: " . $result2 . "</span>" . $section2 . "</div>";
                $table2 .= "</div>";
                //$table .= "<td>" . $val['o_section'] . "</td>";
                //$table .= "<td>" . date('d.m.Y', strtotime($val['add_date'])) . "</td>";
                //$table .= "<td>" . date('d.m.Y', strtotime($val['end_date'])) . "</td>";
                //$table .= "<td>" . $result . "</td>";
                //////////////////////////////////////////////////////
                $section2 = '';
            }
            //$table2 .= "</div>";
        }

    }
?>