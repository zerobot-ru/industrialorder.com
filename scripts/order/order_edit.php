<?php

$pageTitle = 'Редактирование заказа ' . $_REQUEST['eid'];
$h1Title = 'Страница редактирвоания заказа';
$pageDesc = '';

//Проверяем зашел ли пользователь
if ($user === false) {
    header('Location:' . BEZ_HOST . '?mode=error&errorNum=8');
    exit;
}
if ($user === true) {
    
    $folder = ROOT_DIR . '/uploads/orders/';
    $eid = htmlspecialchars(stripslashes($_REQUEST['eid']));
    $sql1 = 'SELECT * FROM `' . BEZ_DBPREFIX . 'files` WHERE `order_num`=:order_num';
    $stmt1 = $db->prepare($sql1);
    $stmt1->bindValue(':order_num', $_REQUEST['eid'], PDO::PARAM_STR);
    $stmt1->execute();
    $rows1 = $stmt1->fetchAll(PDO::FETCH_ASSOC);
    foreach($rows1 as $row1){
    $files .= '<div class="col-sm-6 col-sm-offset-3">' . $row1['file'] . '&nbsp;&nbsp;&nbsp;';
    $files .= '<a href = "?mode=order_edit&eid=' . $eid . '&delete=' . $row1['file'] . '"class = "href">Удалить</a></div><br>';
    }
    
    if(isset($_REQUEST['delete']))
{
$delete =  htmlspecialchars(stripslashes($_REQUEST['delete']));
$filename = $folder . $delete;
unlink($filename);
$sql_d = 'DELETE FROM `' . BEZ_DBPREFIX . 'files` WHERE `file`=:file';
$stmt_d = $db->prepare($sql_d);
$stmt_d->bindValue(':file', $delete, PDO::PARAM_STR);
$stmt_d->execute();
header("location:?mode=order_edit&eid=$eid");
}
    
    //echo 'SESSION: ' . $_SESSION['r_company'];
    //Запрос на выборку контента согласно роли
    $sql = 'SELECT * FROM `' . BEZ_DBPREFIX . 'orders` WHERE `id_order`=:id_order AND `o_company`=:r_company';
    $stmt = $db->prepare($sql);
    $stmt->bindValue(':id_order', $_REQUEST['eid'], PDO::PARAM_STR);
    $stmt->bindValue(':r_company', $_SESSION['r_company']);

    $stmt->execute();
    $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
    if ($rows[0]['o_company'] == $_SESSION['r_company']) {
        $id_order = $rows[0]['id_order'];
        $content = nl2br($rows[0]['content']);
        $section = $rows[0]['o_section'];
        $company = $rows[0]['o_company'];
        $add_date = $rows[0]['add_date'];
        $end_date = $rows[0]['end_date'];
        $o_type = $rows[0]['o_type'];

        $sql5 = 'SELECT order_num FROM `' . BEZ_DBPREFIX . 'files` WHERE `order_num` = :order_num';
        $stmt = $db->prepare($sql5);
        $stmt->bindValue(':order_num', $rows[0]['id_order'], PDO::PARAM_INT);
        $stmt->execute();
        $rows5 = $stmt->fetchAll(PDO::FETCH_ASSOC);

        $result = count($rows5);

        if (isset($_POST['submit'])) {
      $ext = array(".gif", ".jpg", ".jpeg", ".png", ".tif", ".pdf", ".xls", ".xlsx", ".doc", ".docx", ".odt", ".zip", ".rar");
            $folder = ROOT_DIR . '/uploads/orders/';
            $result = count($_FILES);
  
            if ($result > 20) {
                echo '<div class="alert alert-danger" role="alert">Слишком много файлов!</div>';
            } else {

                for ($i = 0; $i < count($_FILES['add_file']['name']); $i++) {
                    // Получаем расширение файла
                    $file_ext[$i] = strtolower(strrchr($_FILES['add_file']['name'][$i], '.'));
                    // Генерируем случайное число
                    $file_name = uniqid(rand(10000, 99999));
                    // Формируем путь на сервере
                    $uploadedFile[$i] = $folder . $file_name . $file_ext[$i];
                    $fileName = $file_name . $file_ext[$i];

                    if (!in_array($file_ext[$i], $ext)) {
                        echo "<div class=\"alert alert-danger\" role=\"alert\">Такое нельзя загружать.</div>";
                    } else {

                        $sql3 = 'INSERT INTO `bez_files` SET file=:file, order_num=:order_num'; // :r_comapny,
                        $stmt = $db->prepare($sql3);
                        $stmt->bindValue(':file', $fileName, PDO::PARAM_STR);
                        $stmt->bindValue(':order_num', $_REQUEST['eid'], PDO::PARAM_INT);
                        $stmt->execute();
                        echo 'fileName: ' . $fileName . '<br>';
                        echo 'f_company: ' . $company_id . '<br>';
                        echo 'ID: ' . $_REQUEST['eid'] . '<br>';
                        
                        if (is_uploaded_file($_FILES['add_file']['tmp_name'][$i])) {
                            if (move_uploaded_file($_FILES['add_file']['tmp_name'][$i], $uploadedFile[$i])) {
                                echo "<div class=\"alert alert-success\" role=\"alert\">Файл загружен.</div>";
                            } else {
                                echo "<div class=\"alert alert-danger\" role=\"alert\">Во время загрузки файла произошла ошибка.</div>";
                            }
                        } else {
                            echo "<div class=\"alert alert-danger\" role=\"alert\">Файл не загружен.</div>";
                        }
                        
                    }
                }
            } 
            
            
            
        
        
            /* Создаем запрос на запись в базу
              новой информации от пользователя */
            $sql2 = 'UPDATE `' . BEZ_DBPREFIX . 'orders` SET content=:content WHERE id_order=:id_order';
            //Подготавливаем PDO выражение для SQL запроса
            $stmt = $db->prepare($sql2);
            $stmt->bindValue(':id_order', $_POST['id_order'], PDO::PARAM_STR);
            $stmt->bindValue(':content', $_POST['content'], PDO::PARAM_STR);
            $stmt->execute();

            header('Location:' . BEZ_HOST . '?mode=orders');
            exit;
        }
       
        
    } else {
        header('Location:' . BEZ_HOST . '?mode=error&errorNum=8');
        exit;
    }
}
?>