<?php

if ($_SESSION['currentLng'] == "ru-ru") {
    $pageTitle = 'Добавить заказ' . $label_PTitle;
    $pageDesc = 'Найти поставщиков и подрядчиков стало проще - разместите заказ и его увидят представители промышленных предприятий по всему Миру.';
} else {
    $pageTitle = 'Add order' . $label_PTitle;
    $pageDesc = 'Find suppliers and contractors is easier - place an order and it will be seen by representatives of industrial enterprises around the world.';
}

$yourIP = $_SERVER['REMOTE_ADDR'];

if ($user === false) {
    header('Location:' . BEZ_HOST . '?mode=error&errorNum=8');
    exit;
}

if ($user === true) {
    if ($_SESSION['allready'] === NULL) {
        header('Location:' . BEZ_HOST . '?mode=error&errorNum=7');
        exit;
    } else {
        //include 'scripts/order/order_add.html';

        /* $sql1 = 'SELECT * FROM `' . BEZ_DBPREFIX . 'type` ORDER BY type_id ASC';
        $stmt1 = $db->prepare($sql1);
        //Выводим контент
        if ($stmt1->execute()) {
            $summary = '';
            $rows1 = $stmt1->fetchAll(PDO::FETCH_ASSOC);
            foreach ($rows1 as $val1) {
                $sql22 = 'SELECT * FROM `' . BEZ_DBPREFIX . 'cat` WHERE id_type=' . $val1['type_id'] . ' ORDER BY cat_id ASC';
                $stmt22 = $db->prepare($sql22);
                $summary .= '<optgroup label="' . $val1['type_name'] . '">'; // '<input type="checkbox" value="' . $val1['type_id'] . '">' . $val1['type_name']
                //Выводим контент
                if ($stmt22->execute()) {
                    $rows22 = $stmt22->fetchAll(PDO::FETCH_ASSOC);
                    foreach ($rows22 as $val22) {
                        $summary .= '<option value="' . $val22['cat_id'] . '">' . $val22['cat_name'] . '</option>';
                    }
                }
                $summary .= '</optgroup>';
            }
        }

        $sql1 = 'SELECT * FROM `class_okved` ORDER BY name ASC';
        $stmt1 = $db->prepare($sql1);
        if ($stmt1->execute()) {
            $summary = '';
            $rows1 = $stmt1->fetchAll(PDO::FETCH_ASSOC);
            foreach ($rows1 as $val1) {
                $summary .= '<option value="' . $val1['code'] . '">' . $val1['name'] . '</option>';
            }
        } */
        
         if ($_SESSION['currentLng'] == "ru-ru") {
             $text = "В данном разделе размещаются только потребности компаний. Запрещено размещать предложения о продаже, услугах и т.п. При нарушении правил размещения, заказ будет удален.";
         }

        if ($_SESSION['currentLng'] == "ru-ru") {
            $sql1 = 'SELECT * FROM `type_new` ORDER BY id ASC';
        } else {
            $sql1 = 'SELECT * FROM `type_new_eng` ORDER BY id ASC';
        }

     
if(isset($_REQUEST['empty'])){
    $summary1 = "Ничего не найдено";
}
        $stmt1 = $db->prepare($sql1);
        //$stmt->bindValue(':order_num', $val['id_order'], PDO::PARAM_INT);
        $stmt1->execute();
        $rows1 = $stmt1->fetchAll(PDO::FETCH_ASSOC);
        $res .= '<div id = "invisible">';
        
        
foreach($rows1 as $row1){
    $res .= '<div class ="option">';
    $res .= '<div class = "option2"><strong id ="strong">'. $row1['name'] . '</strong></div>';
 
    
    $sql1111 = 'SELECT * FROM `class_new` WHERE parent=:parent';
        $stmt1111 = $db->prepare($sql1111);
        $stmt1111->bindValue(':parent', $row1['id'], PDO::PARAM_INT);
        $stmt1111->execute();
        $rows1111 = $stmt1111->fetchAll(PDO::FETCH_ASSOC);
    $res .= '<ul class = "option1">';
    foreach($rows1111 as $row1111){
        $res .=  '<input type="checkbox" class="subsection" style="float:left; transform:scale(1.5)" name = "' . $row1111["id"] . '"><li style = " margin-left: 20px;">' . $row1111["name"] . '</li>';
        
        
    }
    $res .= '</ul></div>';
}
 $res .= '</div>';
 
 
 
        //$sql1 = 'SELECT * FROM `type_okved` ORDER BY id ASC';
        $stmt1 = $db->prepare($sql1);
        if ($stmt1->execute()) {
            $summary = '';
            $rows1 = $stmt1->fetchAll(PDO::FETCH_ASSOC);
            foreach ($rows1 as $val1) {
                /* $summary .= '<option value="' . $val1['code'] . '">' . $val1['name'] . '</option>'; */
                if ($_SESSION['currentLng'] == "ru-ru") {
                    $sql22 = 'SELECT * FROM `class_new` WHERE parent=' . $val1['id'];
                } else {
                    $sql22 = 'SELECT * FROM `class_new_eng` WHERE parent=' . $val1['id'];
                }
                //$sql22 = 'SELECT * FROM `class_okved` WHERE parent_id=' . $val1['id']; // . ' ORDER BY cat_id ASC';
                $stmt22 = $db->prepare($sql22);
                $summary .= '<optgroup label="' . $val1['name'] . '">'; // '<input type="checkbox" value="' . $val1['type_id'] . '">' . $val1['type_name']
                //Выводим контент
                if ($stmt22->execute()) {
                    $rows22 = $stmt22->fetchAll(PDO::FETCH_ASSOC);
                    foreach ($rows22 as $val22) {
                        $summary .= '<option value="' . $val22['id'] . '">' . $val22['name'] . '</option>';
                    }
                }
                $summary .= '</optgroup>';
            }
        }


        if (isset($_POST['submit'])) {

            $sql = 'SELECT company_id FROM `' . BEZ_DBPREFIX . 'reg` WHERE `login`=:email';
            $stmt = $db->prepare($sql);
            $stmt->bindValue(':email', $_SESSION['login'], PDO::PARAM_STR);
            $stmt->execute();
            $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
            $company_id = $rows[0]['company_id'];
            $add_date = date("Y-m-d"); // текущая дата для поля add_date
            // TODO: Переписать этот запрос на простую выборку из массива $_SESSION

            $sections = htmlspecialchars(stripslashes($_POST['cat']));
            $section = explode(" ; ", $sections);
print_r($sections);
            $sql12 = "SELECT id FROM class_new WHERE name=:name";
            $res12 = $db -> prepare($sql12);
            $res12 -> bindValue(':name', $section[0], PDO::PARAM_STR);
            $res12-> execute();
            $rows12 =  $res12->fetch(PDO::FETCH_ASSOC);
            $length = count($section);
            
            
            /* Если все хорошо, пишем данные в базу */
            $sql2 = 'INSERT INTO `bez_orders` SET o_title = :o_title, content = :content,
        o_type = :o_type,
        o_section=:o_section,
        o_company=:o_company,
        add_date=:add_date,
        end_date=:end_date,
        o_user=:o_user';
            // Подготавливаем PDO выражение для SQL запроса
            $stmt = $db->prepare($sql2);
            //$category = implode(", ", $_POST['cat']);
            $stmt->bindValue(':o_title', $_POST['o_title'], PDO::PARAM_STR);
            $stmt->bindValue(':content', $_POST['content'], PDO::PARAM_STR);
            $stmt->bindValue(':o_type', 'Заказ', PDO::PARAM_STR);
            $stmt->bindValue(':o_section', $rows12['id'], PDO::PARAM_STR);
            $stmt->bindValue(':o_company', $company_id, PDO::PARAM_STR);
            $stmt->bindValue(':add_date', $add_date, PDO::PARAM_STR);
            $stmt->bindValue(':end_date', $_POST['end_date'], PDO::PARAM_STR);
            $stmt->bindValue(':o_user',$_SESSION['id_reg']);
            $stmt->execute();
            $newID = $db->lastInsertId();
            
            
            for($i = 1; $i < $length; $i++){
               if($section[$i] != ""){
             $sql13 = "SELECT id FROM class_new WHERE name=:name";
            $res13 = $db -> prepare($sql13);
            $res13 -> bindValue(':name', $section[$i], PDO::PARAM_STR);
            $res13-> execute();
            $rows13 =  $res13->fetch(PDO::FETCH_ASSOC);
            $id =  $rows13['id'];
            
            $sql11 = "UPDATE bez_orders SET o_section=CONCAT(o_section, ', $id') WHERE id_order=:id_order";
            $res11 = $db -> prepare($sql11);
            $res11 -> bindValue(':id_order', $newID, PDO::PARAM_STR);
            $res11-> execute();
        }
            }
            $sql4 = "SELECT o_section FROM bez_orders WHERE id_order=:id_order";
            $res4 = $db -> prepare($sql4);
            $res4 -> bindValue(':id_order', $newID, PDO::PARAM_STR);
            $res4-> execute();
            $rows6 =  $res4->fetchAll(PDO::FETCH_ASSOC);
            $section = $rows6[0]['o_section'];
            
            $sql5 = "SELECT id_company FROM bez_companys WHERE c_section=:c_section";
            $res1 = $db -> prepare($sql5);
            $res1 -> bindValue(':c_section', $section, PDO::PARAM_STR);
            $res1-> execute();
            $rows7 =  $res1->fetchAll(PDO::FETCH_ASSOC);
            foreach($rows7 as $row7){
                
            $sql6= "SELECT login, pass FROM bez_reg WHERE company_id=:company_id";
            $res2 = $db -> prepare($sql6);
            $res2 -> bindValue(':company_id', $row7['id_company'], PDO::PARAM_STR);
            $res2-> execute();
            $rows8 =  $res2->fetchAll(PDO::FETCH_ASSOC);
            $to = $rows8[0]['login']; 
            $subject = 'Заказ';
            $message = 'Здравствуйте.' . PHP_EOL . 'По вашей сфере деятельности поступил новый заказ, для ознакомления зайдите в профиль своей компании'.  PHP_EOL . 'https://industrialorder.com?mode=order_view&vid='. $newID . '&email=' . $rows8[0]["login"] . '&pass=' . $rows8[0]["pass"]; 
            $headers = 'From: robot@industrialorder.com' . PHP_EOL .
            'Reply-To: robot@industrialorder.com' . PHP_EOL;
mail($to, $subject, $message, $headers);
            }
            //$rows8[0]['login'] 'chaldushkin90@mail.ru'
            // Начало обработчика загрузки файлов

            $ext = array(".gif", ".jpg", ".jpeg", ".png", ".tif", ".pdf", ".xls", ".xlsx", ".doc", ".docx", ".odt", ".zip", ".rar");
            $folder = ROOT_DIR . '/uploads/orders/';
            $result = count($_FILES);

            if ($result > 20) {
                echo '<div class="alert alert-danger" role="alert">Слишком много файлов!</div>';
            } else {

                for ($i = 0; $i < count($_FILES['uploadFile']['name']); $i++) {

                    // Получаем расширение файла
                    $file_ext[$i] = strtolower(strrchr($_FILES['uploadFile']['name'][$i], '.'));
                    // Генерируем случайное число
                    $file_name = uniqid(rand(10000, 99999));
                    // Формируем путь на сервере
                    $uploadedFile[$i] = $folder . $file_name . $file_ext[$i];
                    $fileName = $file_name . $file_ext[$i];

                    if (!in_array($file_ext[$i], $ext)) {
                        echo "<div class=\"alert alert-danger\" role=\"alert\">Такое нельзя загружать.</div>";
                    } else {

                        $sql3 = 'INSERT INTO `bez_files` SET file=:file, order_num=:order_num'; // :r_comapny,
                        $stmt = $db->prepare($sql3);
                        $stmt->bindValue(':file', $fileName, PDO::PARAM_STR);
                        $stmt->bindValue(':order_num', $newID, PDO::PARAM_INT);
                        $stmt->execute();
                        echo 'fileName: ' . $fileName . '<br>';
                        echo 'f_company: ' . $company_id . '<br>';
                        echo 'ID: ' . $newID . '<br>';
                        if (is_uploaded_file($_FILES['uploadFile']['tmp_name'][$i])) {
                            if (move_uploaded_file($_FILES['uploadFile']['tmp_name'][$i], $uploadedFile[$i])) {
                                echo "<div class=\"alert alert-success\" role=\"alert\">Файл загружен.</div>";
                            } else {
                                echo "<div class=\"alert alert-danger\" role=\"alert\">Во время загрузки файла произошла ошибка.</div>";
                            }
                        } else {
                            echo "<div class=\"alert alert-danger\" role=\"alert\">Файл не загружен.</div>";
                        }
                    }
                }
            }
            // Окончание обработчика загрузки файлов
            // Сбрасываем параметры
            header('Location:' . BEZ_HOST . '?mode=orders');
            exit;
        }
            
            
    }
}

?>