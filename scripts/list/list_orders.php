<?php
/**
 * Created by PhpStorm.
 * User: Aleksej Kazin
 * Date: 18.04.2017
 * Time: 23:29
 */

if ($_SESSION['currentLng'] == "ru-ru") {
    $pageTitle = 'Список всех заказов' . $label_PTitle;
    $pageDesc = 'Список всех заказов';
} else {
    $pageTitle = 'List of all orders' . $label_PTitle;
    $pageDesc = 'List of all orders';
}

$sql = 'SELECT * FROM `' . BEZ_DBPREFIX . 'orders` ORDER BY `id_order` ASC';
$stmt = $db->prepare($sql);
$stmt->execute();
$row = $stmt->fetchAll(PDO::FETCH_ASSOC);

$i = 1;

foreach ($row as $val) {

    $id = $val['id_order'];
    $name = htmlspecialchars($val['o_title']);

    $list_orders .= $i . ' - ' . $id . ' - <a href="' . BEZ_HOST . '?mode=order_view&vid=' . $id . '">' . $name . '</a><br>';
    $i++;
}