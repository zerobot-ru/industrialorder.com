<?php
/**
 * Created by PhpStorm.
 * User: Aleksej Kazin
 * Date: 18.04.2017
 * Time: 23:29
 */

if ($_SESSION['currentLng'] == "ru-ru") {
    $pageTitle = 'Список всех компаний' . $label_PTitle;
    $pageDesc = 'Список всех компаний';
} else {
    $pageTitle = 'List of all companies' . $label_PTitle;
    $pageDesc = 'List of all companies';
}

$sql = 'SELECT * FROM `' . BEZ_DBPREFIX . 'companys` ORDER BY `id_company` ASC';
$stmt = $db->prepare($sql);
$stmt->execute();
$row = $stmt->fetchAll(PDO::FETCH_ASSOC);

$i = 1;

$list_companys='<div class="table-responsive"><table class="table table-bordered table-striped">';
$list_companys.='<thead> <tr> <th>#</th> <th>id</th> <th>Title</th> <th>Job type</th> <th>Date</th> </tr> </thead>';

foreach ($row as $val) {

    $sql1='SELECT * FROM `class_new` WHERE id=' . $val['c_section'];
    $stmt1=$db->prepare($sql1);
    if($stmt1->execute()){
        $rows1=$stmt1->fetchAll(PDO::FETCH_ASSOC);
        foreach ($rows1 as $val11){
            $section1='<strong>' . $val11['name'] . '</strong>';
        }
    }

    $id = $val['id_company'];
    $name = htmlspecialchars($val['c_name']);
    $job = $section1;
    $date=date('d.m.Y', strtotime($val['reg_date']));

    $list_companys .= '<tr> <th scope="row">' . $i . '</th><td>' . $id . '</td><td><a href="' . BEZ_HOST . '?mode=company_view&vid=' . $id . '" target="_blank">' . $name . '</a></td><td>' . $job . '</td><td>' . $date . '</td></tr>';
    $i++;
}

$list_companys.='</table></div>';