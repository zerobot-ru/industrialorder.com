<?php

if ($_SESSION['currentLng'] == "ru-ru") {
    $pageTitle = 'Поиск компаний по виду деятельности' . $label_PTitle;
    $pageDesc = 'Наша база производителей промышленной продукции включает в себя предприятия со всего Мира. Вы можете найти подрядчика в своем регионе и согласовать с ним детали заказа';
} else {
    $pageTitle = 'Company search by activity type' . $label_PTitle;
    $pageDesc = 'Our base of manufacturers of industrial products includes enterprises from all over the world. You can find a contractor in your region and coordinate with him the details of the order';
}

/**
 * Обработчик вывода всех компаний
 */

/* $sql1 = 'SELECT * FROM `' . BEZ_DBPREFIX . 'type` ORDER BY type_id ASC';
$stmt1 = $db->prepare($sql1);
//Выводим контент
if ($stmt1->execute()) {
    $summary = '';
    $rows1 = $stmt1->fetchAll(PDO::FETCH_ASSOC);
    foreach ($rows1 as $val1) {
        $sql22 = 'SELECT * FROM `' . BEZ_DBPREFIX . 'cat` WHERE id_type=' . $val1['type_id'] . ' ORDER BY cat_id ASC';
        $stmt22 = $db->prepare($sql22);
        $summary .= '<optgroup label="' . $val1['type_name'] . '">'; // '<input type="checkbox" value="' . $val1['type_id'] . '">' . $val1['type_name']
        //Выводим контент
        if ($stmt22->execute()) {
            $rows22 = $stmt22->fetchAll(PDO::FETCH_ASSOC);
            foreach ($rows22 as $val22) {
                $summary .= '<option value="' . $val22['cat_id'] . '">' . $val22['cat_name'] . '</option>';
            }
        }
        $summary .= '</optgroup>';
    }
} */

//include 'bd.php';
 $summary = '';
include 'bd.php';
if($_SESSION['currentLng']=="ru-ru"){
    $sql111 = 'SELECT * FROM `type_new`';
} else {
    $sql111 = 'SELECT * FROM `type_new_eng`';
}
if(isset($_REQUEST['empty'])){
    $summary1 = "Ничего не найдено";
}
 $stmt111 = $db->prepare($sql111);
        //$stmt->bindValue(':order_num', $val['id_order'], PDO::PARAM_INT);
        $stmt111->execute();
        $rows111 = $stmt111->fetchAll(PDO::FETCH_ASSOC);
        $res .= '<div id = "invisible">';
        
        
foreach($rows111 as $row111){
    $res .= '<div class ="option">';
    $res .= '<div class = "option2"><strong id ="strong">'. $row111['name'] . '</strong></div>';
 
    if ($_SESSION['currentLng'] == "ru-ru") {
    $sql1111 = 'SELECT * FROM `class_new` WHERE parent=:parent';
    } else {
        $sql1111 = 'SELECT * FROM `class_new_eng` WHERE parent=:parent';
    }
        $stmt1111 = $db->prepare($sql1111);
        $stmt1111->bindValue(':parent', $row111['id'], PDO::PARAM_INT);
        $stmt1111->execute();
        $rows1111 = $stmt1111->fetchAll(PDO::FETCH_ASSOC);
    $res .= '<ul class = "option1">';
    foreach($rows1111 as $row1111){
        $res .=  '<input type="checkbox" class="subsection" style="float:left; transform:scale(1.5)" name = "' . $row1111["id"] . '"><li style = " margin-left: 20px;">' . $row1111["name"] . '</li>';
        
        
    }
    $res .= '</ul></div>';
}
 $res .= '</div>';
 
 $stmt1 = $db->prepare($sql1);
if ($stmt1->execute()) {
    $summary = '';
    $rows1 = $stmt1->fetchAll(PDO::FETCH_ASSOC);
    foreach ($rows1 as $val1) {
        /* $summary .= '<option value="' . $val1['code'] . '">' . $val1['name'] . '</option>'; */
        if($_SESSION['currentLng']=="ru-ru"){
            $sql22 = 'SELECT * FROM `class_new` WHERE parent=' . $val1['id'];
        } else {
            $sql22 = 'SELECT * FROM `class_new_eng` WHERE parent=' . $val1['id'];
        }

         // . ' ORDER BY cat_id ASC';
        $stmt22 = $db->prepare($sql22);
        $summary .= '<optgroup label="' . $val1['name'] . '">'; // '<input type="checkbox" value="' . $val1['type_id'] . '">' . $val1['type_name']
        //Выводим контент
        if ($stmt22->execute()) {
            $rows22 = $stmt22->fetchAll(PDO::FETCH_ASSOC);
            foreach ($rows22 as $val22) {
                $summary .= '<option value="' . $val22['id'] . '">' . $val22['name'] . '</option>';
            }
        }
        $summary .= '</optgroup>';
    }
}



if(!isset($_REQUEST['page'])){
    $page = 1;
}
else {
    $page = htmlspecialchars(stripcslashes($_REQUEST['page']));
}
    $offset = ($page-1) * 10;
   $sql31 = "SELECT * FROM `" . BEZ_DBPREFIX . "companys`";
$stmt31 = $db->prepare($sql31);
    $stmt31->execute();
    $rows31 = $stmt31->fetchAll(PDO::FETCH_ASSOC);
    
$sql2 = "SELECT * FROM `" . BEZ_DBPREFIX . "companys` ORDER BY id_company DESC LIMIT 10 OFFSET $offset";
$stmt2 = $db->prepare($sql2);
    $stmt2->execute();
    $rows2 = $stmt2->fetchAll(PDO::FETCH_ASSOC);
    $all_companys .= "<div style ='text-align:center'><h3>" . $label_AllCompanys . ": </h3></div>";
    foreach($rows2 as $row2){
        if ($_SESSION['currentLng'] == "ru-ru") {
        $sql3 = "SELECT * FROM class_new WHERE id=:id";
        } else {
            $sql3 = "SELECT * FROM class_new_eng WHERE id=:id";
        }
        $stmt3 = $db->prepare($sql3);
        $stmt3 -> bindValue(':id', $row2['c_section'], PDO::PARAM_STR);
        $stmt3->execute();
        $rows3 = $stmt3->fetch(PDO::FETCH_ASSOC);
        if($row2['c_logo'] === NULL) {
             $logo = "";
         }
         else {$logo = BEZ_HOST . 'uploads/logo/' . htmlspecialchars($row2['c_logo']);}
        $all_companys .= "<div class='panel panel-default'>";
        $all_companys .= "<div class='panel-heading'>";
        $all_companys .= "<!-- span class='pull-right'></span -->";
        $all_companys .= "<h3 class='panel-title'><a href ='?mode=company_view&vid=" . $row2['id_company'] . "'>" . $row2['c_name'] . "</a></h3>";
        $all_companys .= "</div><div class='panel-body'>";
        $all_companys .= '<div class="col-md-8"><img src="' . $logo . '" class="imgl img-rounded c-logo"></div>';
        $all_companys .= '<div class="col-md-8">';
        
        $all_companys .= $row2['full_desc'] . "</div>";
        $all_companys .= "<div class='col-md-4'><span class='pull-right'><ul>";
        $all_companys .= "<li><a>" . $row2['email'] . "</a></li>";
        $all_companys .= "<li>" . $row2['site'] . "</li>";
        $all_companys .= "<li>" . $row2['c_tel'] . "</li>";
        $all_companys .= "</ul></span>";
        $all_companys .= "</div></div><div class='panel-footer'>" . $rows3['name'] . "</div>";
        $all_companys .= "</div>";
    }

    
    $length = count($rows31); //Количество тем в форуме
$pages = ceil(($length) / 10); //Количество страниц

    if($length >= 10) {
        $show .=$length;
        $show .= "<div class = 'col-md-offset-6'>";
     
    if($page != 1) {
        $show .="<a href='?mode=all_companys&page=1' title = 'В начало'><<</a> ";
        $show .="<a href='?mode=all_companys&page=" . ($page - 1) . "' title = 'Предыдущая'><</a> ";
    }
//Показать текущую страницу и +- 3
    if(($page - 4) > 0) {
    $show .= " ... ";
    }
    if(($page - 3) > 0) {
         $show .="<a href='?mode=all_companys&page=" . ($page - 3) . "'>" . ($page - 3) . " </a>";
    }
    if(($page - 2) > 0) {
         $show .="<a href='?mode=all_companys&page=" . ($page - 2) . "'>" . ($page - 2) . " </a>";
    }
    if(($page - 1) > 0) {
         $show .="<a href='?mode=all_companys&page=" . ($page - 1) . "'>" . ($page - 1) . " </a>";
    }
    if(($page) > 0) {
         $show .="<a href='?mode=all_companys&page=" . $page . "'><strong>" . $page . " </strong></a>";
    }
    if(($page + 1) < $pages+1) {
         $show .="<a href='?mode=all_companys&page=" . ($page + 1) . "'>" . ($page + 1) . " </a>";
    }
    if(($page + 2) < $pages+1) {
         $show .="<a href='?mode=all_companys&page=" . ($page + 2) . "'>" . ($page + 2) . " </a>";
    }
    if(($page + 3) < $pages+1) {
         $show .="<a href='?mode=all_companys&page=" . ($page + 3) . "'>" . ($page + 3) . " </a>";
    }
    if(($page + 3) < $pages) {
        $show .= " ... ";
    }
    
    /*for($i = 0; $i < $pages; $i++) {
        $show .="<a href='?mode=view_question&g_subject=2&page=" . ($i + 1) . "'>" . ($i+1) . " </a>";
}*/
    if($page != $pages) {
        $show .="<a href='?mode=all_companys&page=" . ($page + 1) . "' title = 'Следующая'>></a> ";
        $show .="<a href='?mode=all_companys&page=". $pages . "' title = 'В конец'>>></a> ";
}
    $show .="</div>";
    }
?>