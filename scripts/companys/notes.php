<?php
if(isset($_REQUEST['submit_note'])) {
    $note = htmlspecialchars(stripslashes($_REQUEST['note']));
    $vid = htmlspecialchars(stripslashes($_REQUEST['vid']));
    
    $notes_sql = "INSERT INTO `bez_notes` SET id_company=:id_company, my_company=:my_company, note=:note";
$notes_pdo = $db -> prepare($notes_sql);
$notes_pdo -> bindValue(':id_company', $vid, PDO::PARAM_STR);
$notes_pdo -> bindValue(':my_company', $_SESSION['r_company'], PDO::PARAM_STR);
$notes_pdo -> bindValue(':note', $note, PDO::PARAM_STR);
$notes_pdo -> execute();
}