<?php

if(!isset($_REQUEST['page'])){
    $page = 1;
}
else {
    $page = htmlspecialchars(stripcslashes($_REQUEST['page']));
}
    $offset = ($page-1) * 10;
   $sql31 = "SELECT * FROM `" . BEZ_DBPREFIX . "companys`";
$stmt31 = $db->prepare($sql31);
    $stmt31->execute();
    $rows31 = $stmt31->fetchAll(PDO::FETCH_ASSOC);
    
$sql2 = "SELECT * FROM `" . BEZ_DBPREFIX . "companys` ORDER BY id_company DESC LIMIT 10 OFFSET $offset";
$stmt2 = $db->prepare($sql2);
    $stmt2->execute();
    $rows2 = $stmt2->fetchAll(PDO::FETCH_ASSOC);
    $all_companys .= "<div style ='text-align:center'><h3>" . $label_AllCompanys . ": </h3></div>";
    
    foreach($rows2 as $row2){
         if ($_SESSION['currentLng'] == "ru-ru") {
        $sql3 = "SELECT * FROM class_new WHERE id=:id";
         } else {
             $sql3 = "SELECT * FROM class_new_eng WHERE id=:id";
         }
        $stmt3 = $db->prepare($sql3);
        $stmt3 -> bindValue(':id', $row2['c_section'], PDO::PARAM_STR);
        $stmt3->execute();
        $rows3 = $stmt3->fetch(PDO::FETCH_ASSOC);
    
         if($row2['c_logo'] === NULL) {
             $logo = "";
         }
         else {$logo = BEZ_HOST . 'uploads/logo/' . htmlspecialchars($row2['c_logo']);}
        $all_companys .= "<div class='panel panel-default'>";
        $all_companys .= "<div class='panel-heading'>";
        $all_companys .= "<!-- span class='pull-right'></span -->";
        $all_companys .= "<h3 class='panel-title'><a href ='?mode=company_view&vid=" . $row2['id_company'] . "'>" . $row2['c_name'] . "</a></h3>";
        $all_companys .= "</div><div class='panel-body'>";
        $all_companys .= '<div class="col-md-8"><img src="' . $logo . '" class="imgl img-rounded c-logo"></div>';
        $all_companys .= '<div class="col-md-8">';
        
        $all_companys .= $row2['full_desc'] . "</div>";
        $all_companys .= "<div class='col-md-4'><span class='pull-right'><ul>";
        $all_companys .= "<li><a>" . $row2['email'] . "</a></li>";
        $all_companys .= "<li>" . $row2['site'] . "</li>";
        $all_companys .= "<li>" . $row2['c_tel'] . "</li>";
        $all_companys .= "</ul></span>";
        $all_companys .= "</div></div><div class='panel-footer'>" . $rows3['name'] . "</div>";
        
        $all_companys .= "</div>";
    }

    
    $length = count($rows31); //Количество тем в форуме
$pages = ceil(($length) / 10); //Количество страниц

    if($length >= 10) {
        $show .= "<div class = 'col-md-offset-6'>";
     
    if($page != 1) {
        $show .="<a href='?mode=name_search&page=1' title = 'В начало'><<</a> ";
        $show .="<a href='?mode=name_search&page=" . ($page - 1) . "' title = 'Предыдущая'><</a> ";
    }
//Показать текущую страницу и +- 3
    if(($page - 4) > 0) {
    $show .= " ... ";
    }
    if(($page - 3) > 0) {
         $show .="<a href='?mode=name_search&page=" . ($page - 3) . "'>" . ($page - 3) . " </a>";
    }
    if(($page - 2) > 0) {
         $show .="<a href='?mode=name_search&page=" . ($page - 2) . "'>" . ($page - 2) . " </a>";
    }
    if(($page - 1) > 0) {
         $show .="<a href='?mode=name_search&page=" . ($page - 1) . "'>" . ($page - 1) . " </a>";
    }
    if(($page) > 0) {
         $show .="<a href='?mode=name_search&page=" . $page . "'><strong>" . $page . " </strong></a>";
    }
    if(($page + 1) < $pages+1) {
         $show .="<a href='?mode=name_search&page=" . ($page + 1) . "'>" . ($page + 1) . " </a>";
    }
    if(($page + 2) < $pages+1) {
         $show .="<a href='?mode=name_search&page=" . ($page + 2) . "'>" . ($page + 2) . " </a>";
    }
    if(($page + 3) < $pages+1) {
         $show .="<a href='?mode=name_search&page=" . ($page + 3) . "'>" . ($page + 3) . " </a>";
    }
    if(($page + 3) < $pages) {
        $show .= " ... ";
    }
    
    /*for($i = 0; $i < $pages; $i++) {
        $show .="<a href='?mode=view_question&g_subject=2&page=" . ($i + 1) . "'>" . ($i+1) . " </a>";
}*/
    if($page != $pages) {
        $show .="<a href='?mode=all_companys&page=" . ($page + 1) . "' title = 'Следующая'>></a> ";
        $show .="<a href='?mode=all_companys&page=". $pages . "' title = 'В конец'>>></a> ";
}
    $show .="</div>";
    }
?>