<?php

$pageTitle = 'Результаты поиска компаний';
$h1Title = 'Результаты поиска компаний по запросу "' . $_POST['cat'] . '"';
$pageDesc = '';
$back = $_SERVER['HTTP_REFERER'];
$bread = "<ol class='breadcrumb'>
  <li><a href='" . BEZ_HOST . "?mode=index'>" . $label_Index . "</a></li>
  <li><a href='" . BEZ_HOST . "?mode=all_companys'>Поиск компаний по виду деятельности</a></li>
  <li class='active'>Результат поиска</li>
</ol>";

//Проверяем зашел ли пользователь
if ($user === false) {
    header('Location:' . BEZ_HOST . '?mode=error&errorNum=8');
    exit;
}
if ($user === true) {

    /* echo '<header id="head" class="secondary"></header>

<!-- container -->
<div class="container">

    <ol class="breadcrumb">
        <li><a href="<?php echo BEZ_HOST; ?>?mode=index">Главная</a></li>
        <li class="active">Поиск компании</li>
    </ol>

    <div class="row">

        <!-- Article main content -->
        <article class="col-xs-12 maincontent">
            <header class="page-header">
                <h1 class="page-title">Поиск компании</h1>
            </header>
            <div class="col-md-12">'; */

 if ($_SESSION['currentLng'] == "ru-ru") {   
 $sql5 = "SELECT * FROM class_new";
 } else {
     $sql5 = "SELECT * FROM class_new_eng";
 }
            $stmt5 = $db->prepare($sql5);
            $stmt5->execute();
            $rows5 = $stmt5->fetchAll(PDO::FETCH_ASSOC);
            $name = $row5[0]['name'];
            //$array = [];
            foreach($rows5 as $row5){
                $name = $row5['id'];
                $checked = $_REQUEST["$name"];
                if($checked == 'on'){
                    $array .= $row5['id'] . ", ";
                   // echo $row5['name'] . "<br>";
                }
            }
            
    $o_type = $_POST['o_type'];
    $o_section = $_POST['cat'];
     //$length = count($array);
     $str = explode(", ", $array);
     $len_str = count($str);
     $j=0;
     $array_res = [];
     for($i = 0; $i < $len_str; $i++) {
        if($str[$i] != ""){
    $sql = "SELECT * FROM `" . BEZ_DBPREFIX . "companys` WHERE (`c_section` LIKE '$str[$i], %') OR (c_section LIKE '%, $str[$i]') OR (c_section LIKE '$str[$i]') OR (c_section LIKE '%,$str[$i], %')";
        }
    $stmt = $db->prepare($sql);
    $stmt -> execute();
//$o_type,
   
        
    //if ($stmt->execute(array($str[$i]))) { //$o_section
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        foreach ($rows as $val) {
            if(in_array($val['c_name'], $array_res)) {
                continue;
            } else {
            $s_select = explode(", ", $val['c_section']);
            /* foreach ($s_select as $val1) {
                $sql1 = 'SELECT * FROM `' . BEZ_DBPREFIX . 'cat` WHERE cat_id=' . $val1; // . ' ORDER BY cat_id ASC'
                $stmt1 = $db->prepare($sql1);
                //Выводим контент
                if ($stmt1->execute()) {
                    $rows1 = $stmt1->fetchAll(PDO::FETCH_ASSOC);
                    foreach ($rows1 as $val11) {
                        $section1 .= '<span class="label label-default">' . $val11['cat_name'] . '</span> ';
                    }
                }
            } */
         
            foreach ($s_select as $val1){
                if ($_SESSION['currentLng'] == "ru-ru") {  
                $sql1='SELECT * FROM `class_new` WHERE id=' . $val1;
                } else {
                    $sql1='SELECT * FROM `class_new_eng` WHERE id=' . $val1;
                }
                $stmt1=$db->prepare($sql1);
                if($stmt1->execute()){
                    $rows1=$stmt1->fetchAll(PDO::FETCH_ASSOC);
                    foreach ($rows1 as $val11){
                        $section1.='<strong>' . $val11['name'] . '</strong>';
                    }
                }
            }

            $name = $val['c_name'];
            $section = $val['c_section'];

            $short_desc = nl2br($val['short_desc']);
            $full_desc = nl2br(mb_substr(strip_tags($val['full_desc']),0,400)) . '...';
            $address = $val['country'] . ', ' . $val['city'];
            $service = $val['service'];
            $number = $val['id_company'];
            //$c_logo = BEZ_HOST . 'uploads/logo/' . htmlspecialchars($val['c_logo']);
            if ($val['c_logo'] == NULL) {
                $c_logo = BEZ_HOST . 'uploads/logo/empty.jpg';
            } else {
                $c_logo = BEZ_HOST . 'uploads/logo/' . htmlspecialchars($val['c_logo']);
            }
            $email = $val['email'];
            $site = $val['site'];
            $c_tel = $val['c_tel'];

            
            $table .= "<div class='panel panel-default'>";
            $table .= "<div class='panel-heading'>";
            $table .= "<span class='pull-right'>" . $address . "</span>";
            $table .= "<h3 class='panel-title'><a href='" . BEZ_HOST . "?mode=company_view&vid=" . $number . "' target='_blank'>" . $name . "</a></h3>";
            $table .= "</div><div class='panel-body'>";
            $table .= '<div class="col-md-8"><img src="' . $c_logo . '" class="imgl img-rounded c-logo">';
            //$table .= '<td>' . $name . '</td>';
            //$table .= "<td>" . $section . "</td>";
            $table .= $short_desc . "<br>" . $full_desc . "</div>";
            $table .= "<div class='col-md-4'><span class='pull-right'><ul>";
            $table .= "<li><a href='mailto:" . $email . "'>" . $email . "</a></li>";
            $table .= "<li><noindex><a href='" . $site . "' rel='nofollow'>" . $site . "</a></noindex></li>";
            $table .= "</ul><ul>";
            $table .= "<li><a href='tel:" . $c_tel . "'>" . $c_tel . "</a></li>";
            $table .= "</ul></span>";
            $table .= "</div></div><div class='panel-footer'>" . $section1 . ": " . $val['service'] . "</div>";
            $table .= "</div>";
            $section1='';
           $j++;
           $array_res[] = $val['c_name'];
           
            }  
            
      //  }
        }

    }
    
    if(!isset($j)){
        $j = 0;
    }
    if ($_SESSION['currentLng'] == "ru-ru") {
    $some = "<div><strong><h3>По вашему заказу найдено: " . count($array_res) . "заказов</h3></strong></div><br>";
        } else {
    $some = "<div><strong><h3>Your order was found: " . count($array_res) . "companys</h3></strong></div><br>";
        }
 
        
        
        echo '</div>
            </div>
        </article>
        <!-- /Article -->

</div>    <!-- /container -->';
    
}

?>