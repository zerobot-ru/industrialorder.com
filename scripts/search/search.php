<?php

$pageTitle = 'Результат поиска заказов';
$h1Title = 'Результат поиска заказов по запросу "Секция: ' . $_POST['o_type'] . '", "Тип: ' . $_POST['o_section'] . '"';
$pageDesc = '';
$back = $_SERVER['HTTP_REFERER'];
$bread = " <ol class='breadcrumb'>
  <li><a href='" . BEZ_HOST . "?mode=index'>" . $label_Index . "</a></li>
  <li><a href='" . BEZ_HOST . "?mode=all_orders'>" . $label_OSearch . "</a></li>
  <li class='active'>Результат поиска</li>

</ol>";

if ($user === false) {
    header('Location:' . BEZ_HOST . '?mode=error&errorNum=8');
    exit;
}
if ($user === true) {

    echo '<header id="head" class="secondary"></header>

<!-- container -->
<div class="container">
    <div class ="col-sm-11 col-sm-offset-1">
    <ol class="breadcrumb">
        <li><a href="<?php echo BEZ_HOST; ?>?mode=index">' . $label_Index . '</a></li>
        <li class="active">' . $label_OSearch . '</li>
        
    </ol>';
     echo "<ul style='list-style-type: none'>
         <h4><li class='active'><a href='" . $back . "'><strong>" . $label_Back . "</strong></a></li></h4>
    </ul></div>";
    
    echo '<div class="row">

        <!-- Article main content -->
        <article class="col-md-10 col-md-offset-1 maincontent">
            <header class="page-header">';
   
    echo '<h1 class="page-title">' . $label_OSearch  . '</h1>
            </header>';
if ($_SESSION['currentLng'] == "ru-ru") {
    $sql5 = "SELECT * FROM class_new";
} else {
    $sql5 = "SELECT * FROM class_new_eng";
}
            $stmt5 = $db->prepare($sql5);
            $stmt5->execute();
            $rows5 = $stmt5->fetchAll(PDO::FETCH_ASSOC);
            $name = $row5[0]['name'];
            //$array = [];
            foreach($rows5 as $row5){
                $name = $row5['id'];
                $checked = $_REQUEST["$name"];
                if($checked == 'on'){
                    $array .= $row5['id'] . ", ";
                   // echo $row5['name'] . "<br>";
                }
            }
            
     $o_type = $_POST['o_type'];
    $o_sections = $_POST['cat'];
    $o_section = explode(", ", $array);
     $length = count($array);
     $array_res = [];
    for($i = 0; $i < $length; $i++){
        
    $sql = "SELECT * FROM `" . BEZ_DBPREFIX . "orders` WHERE (`o_section` LIKE '%$o_section[$i],%') OR (o_section LIKE '%, $o_section[$i]%') OR (o_section='$o_section[$i]')";
    $stmt = $db->prepare($sql);
    $stmt -> execute();
    
//$o_type,
   
        
    //if ($stmt->execute(array($o_section[$i]))) { //$o_section
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $j = 0;
        foreach ($rows as $val) {
              if(in_array($val['id_order'], $array_res)) {
                continue;
            } else {
            $s_select = explode(", ", $val['o_section']);
            /* foreach ($s_select as $val1) {
                $sql1 = 'SELECT * FROM `' . BEZ_DBPREFIX . 'cat` WHERE cat_id=' . $val1; // . ' ORDER BY cat_id ASC'
                $stmt1 = $db->prepare($sql1);
                //Выводим контент
                if ($stmt1->execute()) {
                    $rows1 = $stmt1->fetchAll(PDO::FETCH_ASSOC);
                    foreach ($rows1 as $val11) {
                        $section1 .= '<span class="label label-default">' . $val11['cat_name'] . '</span> ';
                    }
                }
            } */

            foreach ($s_select as $val1){
                if ($_SESSION['currentLng'] == "ru-ru") {
                $sql1='SELECT * FROM `class_new` WHERE id=' . $val1;
                } else {
                $sql1='SELECT * FROM `class_new_eng` WHERE id=' . $val1;    
                }
                $stmt1=$db->prepare($sql1);
                if($stmt1->execute()){
                    $rows1=$stmt1->fetchAll(PDO::FETCH_ASSOC);
                    foreach ($rows1 as $val11){
                        $section1.='<strong class="label label-default">' . $val11['name'] . '</strong>&nbsp;';
                    }
                }
            }

            $type = $val['o_type'];
            $section = $val['o_section'];

            $sql2 = 'SELECT * FROM `' . BEZ_DBPREFIX . 'files` WHERE `order_num`=:order_num';
            $stmt = $db->prepare($sql2);
            $stmt->bindValue(':order_num', $val['id_order'], PDO::PARAM_INT);
            $stmt->execute();
            $rows2 = $stmt->fetchAll(PDO::FETCH_ASSOC);
            $result = count($rows2);
            $sql3 = 'SELECT * FROM `' . BEZ_DBPREFIX . 'companys` WHERE `id_company`=:id_company';
            $stmt = $db->prepare($sql3);
            $stmt->bindValue(':id_company', $val['o_company'], PDO::PARAM_INT);
            $stmt->execute();
            $rows3 = $stmt->fetchAll(PDO::FETCH_ASSOC);
            $cName = $rows3[0]['c_name'];

            $sql3 = 'SELECT c_name, country, city FROM `' . BEZ_DBPREFIX . 'companys` WHERE `id_company` = :id_company';
            $stmt = $db->prepare($sql3);
            $stmt->bindValue(':id_company', $val['o_company'], PDO::PARAM_INT);
            $stmt->execute();
            $rows3 = $stmt->fetchAll(PDO::FETCH_ASSOC);

            $cName = $rows3[0]['c_name'];
            //$subContent = substr(***, 0, 20) . "...";

            
            
            
           
            
           
            
            $table .= "<div class='panel panel-default'>";
            $table .= "<div class='panel-heading'>";
            $table .= "<span class='pull-right'>" . $rows3[0]['country'] . ", " . $rows3[0]['city'] . "</span><h3 class='panel-title'><a href='" . BEZ_HOST . "?mode=order_view&vid=" . $val['id_order'] . "'>" . $val['o_title'] . " - " . $label_OrderNumber . " " . $val['id_order'] . " " . $label_From . " " . date('d-m-Y', strtotime($val['add_date'])) . "</a></h3>";
            $table .= "</div>";
            //$table .= "<td>" . $cName . "</td>";
            $table .= "<div class='panel-body'>" . $val['content'] . "</div>";
            $table .= "<div class='panel-footer'><span class='pull-right'>" . $label_FilesSearch . ": " . $result . "</span>" . $section1 . "</div>";
            $table .= "</div>";
            //$table .= "<td>" . $val['o_section'] . "</td>";
            //$table .= "<td>" . date('d.m.Y', strtotime($val['add_date'])) . "</td>";
            //$table .= "<td>" . date('d.m.Y', strtotime($val['end_date'])) . "</td>";
            //$table .= "<td>" . $result . "</td>";
            $section1='';
            $j++;
             $array_res[] = $val['c_name'];
            }
        }  
            
        //}
        
    }
    if(!isset($j)){
        $j=0;
    }
    if ($_SESSION['currentLng'] == "ru-ru") {
        echo "<div><strong><h3>По вашему заказу найдено: $j заказов</h3></strong></div><br>";
    } else {
        echo "<div><strong><h3>Your order was found: $j orders</h3></strong></div><br>";
    }
         echo $table;
        
        echo '</div>
            </div>
        </article>
        <!-- /Article -->

</div>    <!-- /container -->';
    
}
?>
