<?php
if ($user === false) {
    header('Location:' . BEZ_HOST . '?mode=auth');
    exit;
}
 else {
    $g_id = htmlspecialchars(stripslashes($_REQUEST['g_subject']));
    $sql = "SELECT * FROM `bez_groups` WHERE id=:g_subject";
    $pdo = $db -> prepare($sql);
    $pdo -> bindValue(':g_subject', $g_id, PDO::PARAM_STR);
    $pdo -> execute();
    $questions = $pdo -> fetch(PDO::FETCH_ASSOC);
    $show .= "<div class='panel panel-default'>";
    $show .= "<div class='panel-heading'>";
    $show .= "<h3 class='panel-title'>" . $label_Theme . ": " . $questions['g_subject'] . "<br></h3>";
    $show .= "</div><div class='panel-body'>";
    $show .= '<div class="col-md-8">';
    $show .= $questions['g_question'] . "</div>";
    $show .= "<div class='col-md-4'><span class='pull-right'><ul>";
    $show .= "<li>" . $label_Date . ": " . $date = date("d-mY", strtotime($questions['g_time'])) . "</li><li>email: " . $questions['g_login'] . "</li>";
    $show .= "</ul></span>";
    $show .= "</div></div>";
    $show .= "</div>";
    if(isset($_REQUEST[page])){
        $page = $_REQUEST[page];
    }
    else {
        $page = 1;
    }
    $offset = ($page - 1)*50;
    $show .= "<h4>" . $label_AnswersForum . ": </h4><br>";
        $select1 = "SELECT * FROM `bez_groups_answer` WHERE id_subject=:g_id LIMIT 50 OFFSET $offset";
        $pdo3 = $db -> prepare($select1);
        $pdo3 -> bindValue(':g_id', $g_id, PDO::PARAM_INT);
        $pdo3 -> execute();
        $result1 = $pdo3 -> fetchall(PDO::FETCH_ASSOC);
        foreach($result1 as $res1) {
            $time1 = strtotime($res1['time']);
            $date1 = date("d-m-Y H:i", $time1);
             
            $show .= "<div class='panel panel-default'>";
            $show .= "<div class='panel-heading'>";
            $show .= "<h3 class='panel-title'>Написал: " . $res1['g_login'] . "<br></h3>";
            $show .= "</div><div class='panel-body'>";
            $show .= '<div class="col-md-8">';
            $show .= $res1['g_answer'] . "</div>";
            $show .= "<div class='col-md-4'><span class='pull-right'><ul>";
            $show .= "<li>Дата: " . $date1 . "</li>";
            $show .= "</ul></span>";
            $show .= "</div></div>";
            $show .= "</div>";
        }
        
        $select = "SELECT * FROM `bez_groups` WHERE id=:g_id";
        $pdo2 = $db -> prepare($select);
        $pdo2 -> bindValue(':g_id', $g_id, PDO::PARAM_STR);
        $pdo2 -> execute();
        $result = $pdo2 -> fetch(PDO::FETCH_ASSOC);
        
    if(isset($_REQUEST["submit"])) {
        $answer = htmlspecialchars(stripslashes($_REQUEST["answer"]));
        $sql1 = "INSERT INTO `bez_groups_answer` SET id_subject=:id_subject, g_login=:g_login, g_answer=:g_answer";
        $pdo1 = $db -> prepare($sql1);
        $pdo1 -> bindValue(':id_subject', $g_id, PDO::PARAM_STR);
        $pdo1 -> bindValue(':g_login', $_SESSION['login'], PDO::PARAM_STR);
        $pdo1 -> bindValue(':g_answer', $answer, PDO::PARAM_STR);
        $pdo1 -> execute();
        header("location:" . BEZ_HOST . "?" . $_SERVER['QUERY_STRING']);
        }
 $subject = htmlspecialchars(stripslashes($_REQUEST['g_subject']));
$select2 = "SELECT id FROM `bez_groups_answer` WHERE id_subject=:id_subject";
$pdo4 = $db -> prepare($select2);
$pdo4 -> bindValue(':id_subject', $subject, PDO::PARAM_STR);
$pdo4 -> execute();
$result3 = $pdo4 -> fetchall(PDO::FETCH_ASSOC);       

$length = count($result3); //Количество тем в форуме
$pages = ceil(($length + 1) / 50); //Количество страниц
    if($length > 49) {
        $show .= "<div class = 'col-md-offset-6'>";
     
    if($page != 1) {
        $show .="<a href='?mode=view_question&g_subject=" . $subject . "&page=1' title = 'В начало'><<</a> ";
        $show .="<a href='?mode=view_question&g_subject=" . $subject . "&page=" . ($page - 1) . "' title = 'Предыдущая'><</a> ";
    }
//Показать текущую страницу и +- 3
    if(($page - 4) > 0) {
    $show .= " ... ";
    }
    if(($page - 3) > 0) {
         $show .="<a href='?mode=view_question&g_subject=" . $subject . "&page=" . ($page - 3) . "'>" . ($page - 3) . " </a>";
    }
    if(($page - 2) > 0) {
         $show .="<a href='?mode=view_question&g_subject=" . $subject . "&page=" . ($page - 2) . "'>" . ($page - 2) . " </a>";
    }
    if(($page - 1) > 0) {
         $show .="<a href='?mode=view_question&g_subject=" . $subject . "&page=" . ($page - 1) . "'>" . ($page - 1) . " </a>";
    }
    if(($page) > 0) {
         $show .="<a href='?mode=view_question&g_subject=" . $subject . "&page=" . $page . "'>" . $page . " </a>";
    }
    if(($page + 1) < $pages+1) {
         $show .="<a href='?mode=view_question&g_subject=" . $subject . "&page=" . ($page + 1) . "'>" . ($page + 1) . " </a>";
    }
    if(($page + 2) < $pages+1) {
         $show .="<a href='?mode=view_question&g_subject=" . $subject . "&page=" . ($page + 2) . "'>" . ($page + 2) . " </a>";
    }
    if(($page + 3) < $pages+1) {
         $show .="<a href='?mode=view_question&g_subject=" . $subject . "&page=" . ($page + 3) . "'>" . ($page + 3) . " </a>";
    }
    if(($page + 3) < $pages) {
        $show .= " ... ";
    }
    
    /*for($i = 0; $i < $pages; $i++) {
        $show .="<a href='?mode=view_question&g_subject=2&page=" . ($i + 1) . "'>" . ($i+1) . " </a>";
}*/
    if($page != $pages) {
        $show .="<a href='?mode=view_question&g_subject=" . $subject . "&page=" . ($page + 1) . "' title = 'Следующая'>></a> ";
        $show .="<a href='?mode=view_question&g_subject=" . $subject . "&page=". $pages . "' title = 'В конец'>>></a> ";
}
    $show .="</div>";
    }
 }