<?php

/**
 * Created by PhpStorm.
 * User: Алексей
 * Date: 31.10.2016
 * Time: 14:24
 */
$pageTitle = 'Мои сообщения';
$h1Title = 'Сообщения пользователя';
$pageDesc = '';

if ($user === false) {
    header('Location:' . BEZ_HOST . '?mode=auth');
    exit;
}

if ($user === true) {

    $sql4 = 'SELECT ssid FROM `' . BEZ_DBPREFIX . 'reg` WHERE `id_reg`=:id_reg';
    $stmt = $db->prepare($sql4);
    $stmt->bindValue(':id_reg', $_SESSION['id_reg']); //, PDO::PARAM_INT
    $stmt->execute();
    $rows4 = $stmt->fetchAll(PDO::FETCH_ASSOC);

    if ($rows4[0]['ssid'] == session_id()) {

        //Запрос на выборку контента согласно роли
        $sql = 'SELECT * FROM (SELECT * FROM `' . BEZ_DBPREFIX . 'pm` WHERE (`u_to` = :u_to OR (u_from =:u_to AND pm_status=:pm_status) ) ORDER BY pm_date DESC)t GROUP BY pm_title'; //AND `pm_flag` = :pm_flag
        $stmt = $db->prepare($sql);
        $stmt->bindValue(':u_to', $_SESSION['id_reg'], PDO::PARAM_STR);
        //$stmt->bindValue(':u_from', $_SESSION['id_reg'], PDO::PARAM_STR);
        //$stmt->bindValue(':pm_flag', '0', PDO::PARAM_STR);
        $stmt->bindValue(':pm_status', '1', PDO::PARAM_STR);
        $stmt->execute();
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

      
            
        $table_incoming .= "<div class='table-responsive'><table class='table table-bordered table-condensed'>";
        //<th>От кого</th>
        $table_incoming .= "<tr>
                 
                 <th>" . $label_Theme . "</th>
                 
                 <th>" . $label_Date . "</th>
                 <th>" . $label_Action . "</th>
                 </tr>";
    //<th>Сообщение</th>    
        $i=0;
        foreach ($rows as $val) {   
     
            $sql_count='SELECT COUNT(*), pm_flag, pm_title FROM `'.BEZ_DBPREFIX.'pm` WHERE u_to =:u_to AND pm_flag = :pm_flag AND pm_title=:pm_title GROUP BY pm_title';
        $pdo=$db->prepare($sql_count);
        $pdo->bindValue(':u_to', $_SESSION['id_reg'], PDO::PARAM_STR);
        $pdo->bindValue(':pm_flag', '0', PDO::PARAM_STR);
        $pdo->bindValue(':pm_title', $val['pm_title'] , PDO::PARAM_STR);
        $pdo->execute();
        $rows_count=$pdo->fetchAll(PDO::FETCH_ASSOC);
        if(!$rows_count[0]["COUNT(*)"]){
          $rows_count[0]["COUNT(*)"] = 0;  
        }
        
        
        $sql_count1='SELECT COUNT(*), pm_flag FROM `'.BEZ_DBPREFIX.'pm` WHERE u_to =:u_to AND pm_title=:pm_title GROUP BY pm_title'; 
        $pdo1=$db->prepare($sql_count1);
        $pdo1->bindValue(':u_to', $_SESSION['id_reg'], PDO::PARAM_STR);
        $pdo1->bindValue(':pm_title', $val['pm_title'] , PDO::PARAM_STR);
        $pdo1->execute();
        $rows_count1=$pdo1->fetchAll(PDO::FETCH_ASSOC);
        if(!$rows_count1[0]["COUNT(*)"]){
          $rows_count1[0]["COUNT(*)"] = 0;  
        }
        
        
            $num = preg_replace('/\D/im', '', $val['pm_title']);
            $sql9 = 'SELECT * FROM `' . BEZ_DBPREFIX . 'orders` WHERE id_order=:id_order ';
            $stmt9 = $db->prepare($sql9);
            $stmt9->bindValue(':id_order', $num);
            $stmt9->execute();
            $rows9 = $stmt9->fetch(PDO::FETCH_ASSOC);
        
            $question_num = preg_replace("/[^0-9]/", '', $val['pm_title']);
            $sql3 = 'SELECT id_reg, name, company_id FROM `' . BEZ_DBPREFIX . 'reg` WHERE `id_reg` = :admin';
            $stmt = $db->prepare($sql3);
            $stmt->bindValue(':admin', $val['u_from']); //, PDO::PARAM_INT
            $stmt->execute();
            $rows2 = $stmt->fetchAll(PDO::FETCH_ASSOC);
            
            $sql_c='SELECT c_name FROM `'.BEZ_DBPREFIX.'companys` WHERE `id_company` = :id';
            $stmt=$db->prepare($sql_c);
            $stmt->bindValue(':id', $rows2[0]['company_id']);
            $stmt->execute();
            $rows_c=$stmt->fetchAll(PDO::FETCH_ASSOC);
            
            $tr = "";
            if(preg_match('/_/ism', $val['pm_hash']) == 1){
               $tr = 1; 
            }
            
            $table_incoming .= "<tr>";
            //$table_incoming .= "<td>" . $rows2[0]['name'] . " (" . $rows_c[0]['c_name'] . ")</td>";
            $table_incoming .= "<td>" . $val['pm_title'] . " " . $rows9['o_title']  . "</td>";
            //$table_incoming .= "<td>" . substr($val['pm_message'], 0, 50) . "...</td>"; // $suburl = substr($row['c_site'],0,20) . "...";
            $table_incoming .= "<td>" . date('d-m-Y H:i', strtotime($val['pm_date'])) . "</td>";
            $table_incoming .= "<td>" . "<a href=" . BEZ_HOST . "?mode=pm_view&pm_hash=" . $val['pm_hash'] . "&from=" . $rows2[0]['id_reg'] . "&is_company=" .$tr . ">" .  $label_Read . "(". $rows_count[0]["COUNT(*)"] . ")</a></td>"; // . "/" . $rows_count1[0]["COUNT(*)"]
            $table_incoming .= "</tr>";
            $i++;
        }

        
        $table_incoming .= "</table></div>";

        //Запрос на выборку контента согласно роли
        
        $sql = 'SELECT * FROM `' . BEZ_DBPREFIX . 'pm` WHERE `u_from` = :u_from AND `pm_status` = :pm_status ORDER BY pm_date DESC';
        $stmt = $db->prepare($sql);
        $stmt->bindValue(':u_from', $_SESSION['id_reg'], PDO::PARAM_STR);
        //$stmt->bindValue(':pm_flag', '0', PDO::PARAM_STR);
        $stmt->bindValue(':pm_status', '1', PDO::PARAM_STR);
        $stmt->execute();
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        
        
        $table_sending = "<div class='table-responsive'><table class='table table-bordered table-condensed'>";
        $table_sending .= "<tr>
                 <th>Кому</th>
                 <th>Тема</th>
                 
                 <th>Дата</th>
                 <th>Действие</th>
                 </tr>";
//<th>Сообщение</th>
        foreach ($rows as $val) {
            $question_num = preg_replace("/[^0-9]/", '', $val['pm_title']);
            $sql3 = 'SELECT id_reg, name, company_id FROM `' . BEZ_DBPREFIX . 'reg` WHERE `id_reg` = :admin';
            $stmt = $db->prepare($sql3);
            $stmt->bindValue(':admin', $val['u_to']); //, PDO::PARAM_INT
            $stmt->execute();
            $rows2 = $stmt->fetchAll(PDO::FETCH_ASSOC);

            $sql_c='SELECT c_name FROM `'.BEZ_DBPREFIX.'companys` WHERE `id_company` = :id';
            $stmt=$db->prepare($sql_c);
            $stmt->bindValue(':id', $rows2[0]['company_id']);
            $stmt->execute();
            $rows_c=$stmt->fetchAll(PDO::FETCH_ASSOC);
            
            $sql7 = 'SELECT * FROM `' . BEZ_DBPREFIX . 'pm` WHERE (`u_to` = :u_to AND  u_from =:u_from) OR (u_to = :u_from AND u_from=:u_to) AND `pm_title`=:pm_title ORDER BY pm_id DESC';
            $stmt7 = $db->prepare($sql7);
            $stmt7->bindValue(':pm_title', urldecode($order_num));
            $stmt7->bindValue(':u_to', $_SESSION['id_reg']);
            $stmt7->bindValue(':u_from', $row4['u_from']);
            $stmt7->execute();
            $rows7 = $stmt7->fetchall(PDO::FETCH_ASSOC);
            
            
            $table_sending .= "<tr>";
            $table_sending .= "<td>" . $val['c_name'] . " (" . $rows_c[0]['c_name'] . ")</td>";
            $table_sending .= "<td>" . $val['pm_title'] . "</td>";
           // $table_sending .= "<td>" . mb_substr($rows7[0]['pm_message'], 0, 50) . "...</td>"; // $suburl = substr($row['c_site'],0,20) . "...";
            $table_sending .= "<td>" . date('d-m-Y H:i', strtotime($val['pm_date'])) . "</td>";
            $table_sending .= "<td>" . "<a href=" . BEZ_HOST . "?mode=pm_view&pm_hash=" . $val['pm_hash'] . "&from=" . $rows2[0]['id_company'] . "&question_num=" . $question_num . ">Прочитать</a></td>";
            $table_sending .= "</tr>";
        }

        $table_sending .= "</table></div>";

        //Запрос на выборку контента согласно роли
        /*
        $sql = 'SELECT * FROM (SELECT * FROM `' . BEZ_DBPREFIX . 'pm` WHERE `u_to` = :u_to AND `pm_flag` = :pm_flag ORDER BY pm_date DESC)t GROUP BY pm_title';
        $stmt = $db->prepare($sql);
        $stmt->bindValue(':u_to', $_SESSION['id_reg'], PDO::PARAM_STR);
        //$stmt->bindValue(':u_from', $_SESSION['id_reg'], PDO::PARAM_STR);
        $stmt->bindValue(':pm_flag', '1', PDO::PARAM_STR);
        //$stmt->bindValue(':pm_status', '0', PDO::PARAM_STR);
        $stmt->execute();
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

            
        $table_read = "<div class='table-responsive'><table class='table table-bordered table-condensed'>";
        $table_read .= "<tr>
                 <th>От кого</th>
                 <th>Тема</th>
                 
                 <th>Дата</th>
                 <th>Действие</th>
                 </tr>";
//<th>Сообщение</th>
        foreach ($rows as $val) {
            $question_num = preg_replace("/[^0-9]/", '', $val['pm_title']);
            $sql3 = 'SELECT id_reg, name, company_id FROM `' . BEZ_DBPREFIX . 'reg` WHERE `id_reg` = :admin';
            $stmt = $db->prepare($sql3);
            $stmt->bindValue(':admin', $val['u_from']); //, PDO::PARAM_INT
            $stmt->execute();
            $rows2 = $stmt->fetchAll(PDO::FETCH_ASSOC);

            $sql_c='SELECT c_name FROM `'.BEZ_DBPREFIX.'companys` WHERE `id_company` = :id';
            $stmt=$db->prepare($sql_c);
            $stmt->bindValue(':id', $rows2[0]['company_id']);
            $stmt->execute();
            $rows_c=$stmt->fetchAll(PDO::FETCH_ASSOC);

          
            $sql7 = 'SELECT * FROM `' . BEZ_DBPREFIX . 'pm` WHERE (`u_to` = :u_to AND  u_from =:u_from) OR (u_to = :u_from AND u_from=:u_to) AND `pm_title`=:pm_title ORDER BY pm_id DESC';
            $stmt7 = $db->prepare($sql7);
            $stmt7->bindValue(':pm_title', urldecode($order_num));
            $stmt7->bindValue(':u_to', $_SESSION['id_reg']);
            $stmt7->bindValue(':u_from', $val['u_from']);
            $stmt7->execute();
            $rows7 = $stmt7->fetchall(PDO::FETCH_ASSOC);
       
            
            
            $table_read .= "<tr>";
            $table_read .= "<td>" . $val['name'] . " (" . $rows_c[0]['c_name'] . ")</td>";
            $table_read .= "<td>" . $val['pm_title'] . "</td>";
            //$table_read .= "<td>" . substr($rows7[0]['pm_message'], 0, 50) . "...</td>"; // $suburl = substr($row['c_site'],0,20) . "...";
            $table_read .= "<td>" . date('d.m.Y H:i', strtotime($val['pm_date'])) . "</td>";
            $table_read .= "<td>" . "<a href=" . BEZ_HOST . "?mode=pm_view&pm_hash=" . $val['pm_hash'] . "&from=" . $rows2[0]['id_reg'] . "&my=" . $my . ">Прочитать</a></td>";
            $table_read .= "</tr>";
        }

        $table_read .= "</table></div>";
*/
    } else {
        $sql5 = 'SELECT ip FROM `' . BEZ_DBPREFIX . 'reg` WHERE `id_reg` = :id_reg';
        $stmt = $db->prepare($sql5);
        $stmt->bindValue(':id_reg', $_SESSION['id_reg']); //, PDO::PARAM_INT
        $stmt->execute();
        $rows5 = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $ip = $rows5[0]['ip'];
        $_SESSION['user'] = false;
        header('Location:' . BEZ_HOST . '?mode=error&errorNum=3&ip=' . $ip);
        exit;
    }
}