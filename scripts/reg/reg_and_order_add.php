<?php

if ($_SESSION['currentLng'] == "ru-ru") {
    $pageTitle = 'Регистрация на сайте' . $label_PTitle;
    $pageDesc = 'Регистрация на сайте. Найдите своих клиентов и поставщиков, производителей оборудования и комплектующих, партнеров и инвесторов, сотрудников. В России и по всему Миру.';
} else {
    $pageTitle = 'Sign up page' . $label_PTitle;
    $pageDesc = 'Sign up page. Find your customers and suppliers, manufacturers of equipment and components, partners and investors, employees. In Russia and around the world.';
}


// Производим активацию аккаунта
if (isset($_GET['key'])) {
    // Проверяем ключ
    $sql = 'SELECT *
			FROM `' . BEZ_DBPREFIX . 'reg`
			WHERE `login` = :key';          // внесено изменение
    // Подготавливаем PDO выражение для SQL запроса
    $stmt = $db->prepare($sql);
    $stmt->bindValue(':key', $_GET['key'], PDO::PARAM_STR);
    $stmt->execute();
    $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

        // Получаем адрес пользователя
        $email = $rows[0]['login'];

        // Активируем аккаунт пользователя
        $sql = 'UPDATE `' . BEZ_DBPREFIX . 'reg`
				SET `status` = 1
				WHERE `login` = :email';
        // Подготавливаем PDO выражение для SQL запроса
        $stmt = $db->prepare($sql);
        $stmt->bindValue(':email', $email, PDO::PARAM_STR);
        $stmt->execute();

        // Отправляем письмо для активации
        $title = 'Ваш аккаунт на https://industrialorder.com/ успешно активирован';
        $message = 'Поздравляю Вас, Ваш аккаунт на https://industrialorder.com/ успешно активирован';

        sendMessageMail($email, BEZ_MAIL_AUTOR, $title, $message);


    
}

$sql_managers = 'SELECT * FROM agent_names';
        $manage = $db->prepare($sql_managers);
        $manage->execute();
        $mans = $manage->fetchAll(PDO::FETCH_ASSOC);
$managers.= '<option>Самостоятельная регистрация</option>';
        foreach($mans as $man) {
$managers.= '<option value = "'. $man['agent_id'] . '">' . $man['manager'] . '</option>';
        }
if ($_SESSION['currentLng'] == "ru-ru") {
    $pageTitle = 'Регистрация компании' . $label_PTitle;
    $pageDesc = 'Страница регистрации компании';
} else {
    $pageTitle = 'Company registration' . $label_PTitle;
    $pageDesc = 'Company registration page';
}

        
$summary = '';
if($_SESSION['currentLng']=="ru-ru"){
    $sql111 = 'SELECT * FROM `type_new`';
} else {
    $sql111 = 'SELECT * FROM `type_new_eng`';
}

 $stmt111 = $db->prepare($sql111);
        //$stmt->bindValue(':order_num', $val['id_order'], PDO::PARAM_INT);
        $stmt111->execute();
        $rows111 = $stmt111->fetchAll(PDO::FETCH_ASSOC);
        $res .= '<div id = "invisible">';
        
        
foreach($rows111 as $row111){
    $res .= '<div class ="option">';
    $res .= '<div class = "option2"><strong id ="strong">'. $row111['name'] . '</strong></div>';
 
    
    $sql1111 = 'SELECT * FROM `class_new` WHERE parent=:parent';
        $stmt1111 = $db->prepare($sql1111);
        $stmt1111->bindValue(':parent', $row111['id'], PDO::PARAM_INT);
        $stmt1111->execute();
        $rows1111 = $stmt1111->fetchAll(PDO::FETCH_ASSOC);
    $res .= '<ul class = "option1">';
    foreach($rows1111 as $row1111){
        $res .=  '<input type="checkbox" class="subsection" style="float:left; transform:scale(1.5)" name = "' . $row1111["id"] . '"><li style = " margin-left: 20px;">' . $row1111["name"] . '</li>';
        
        
    }
    $res .= '</ul></div>';
}
 $res .= '</div>';
 
 $stmt1 = $db->prepare($sql1);
if ($stmt1->execute()) {
    $summary = '';
    $rows1 = $stmt1->fetchAll(PDO::FETCH_ASSOC);
    foreach ($rows1 as $val1) {
        /* $summary .= '<option value="' . $val1['code'] . '">' . $val1['name'] . '</option>'; */
        if($_SESSION['currentLng']=="ru-ru"){
            $sql22 = 'SELECT * FROM `class_new` WHERE parent=' . $val1['id'];
        } else {
            $sql22 = 'SELECT * FROM `class_new_eng` WHERE parent=' . $val1['id'];
        }

         // . ' ORDER BY cat_id ASC';
        $stmt22 = $db->prepare($sql22);
        $summary .= '<optgroup label="' . $val1['name'] . '">'; // '<input type="checkbox" value="' . $val1['type_id'] . '">' . $val1['type_name']
        //Выводим контент
        if ($stmt22->execute()) {
            $rows22 = $stmt22->fetchAll(PDO::FETCH_ASSOC);
            foreach ($rows22 as $val22) {
                $summary .= '<option value="' . $val22['id'] . '">' . $val22['name'] . '</option>';
            }
        }
        $summary .= '</optgroup>';
    }
}

$sql = 'SELECT * FROM `' . BEZ_DBPREFIX . 'orders` WHERE DATE(end_date)>=CURDATE() ORDER BY id_order DESC LIMIT 10';
$stmt = $db->prepare($sql);

//Выводим контент
if ($stmt->execute()) {
    $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

    foreach ($rows as $val) {

        $s_select = explode(", ", $val['o_section']);

        foreach ($s_select as $val1){
            $sql1='SELECT * FROM `class_new` WHERE code=' . $val1;
            $stmt1=$db->prepare($sql1);
            if($stmt1->execute()){
                $rows1=$stmt1->fetchAll(PDO::FETCH_ASSOC);
                foreach ($rows1 as $val11){
                    $section1.='<strong>' . $val11['name'] . '</strong>';
                }
            }
        }

        $sql2 = 'SELECT order_num FROM `' . BEZ_DBPREFIX . 'files` WHERE `order_num` = :order_num';
        $stmt = $db->prepare($sql2);
        $stmt->bindValue(':order_num', $val['id_order'], PDO::PARAM_INT);
        $stmt->execute();
        $rows2 = $stmt->fetchAll(PDO::FETCH_ASSOC);

        $result = count($rows2);

        $sql3 = 'SELECT c_name, country, city FROM `' . BEZ_DBPREFIX . 'companys` WHERE `id_company` = :id_company';
        $stmt = $db->prepare($sql3);
        $stmt->bindValue(':id_company', $val['o_company'], PDO::PARAM_INT);
        $stmt->execute();
        $rows3 = $stmt->fetchAll(PDO::FETCH_ASSOC);

        $cName = $rows3[0]['c_name'];
        //$subContent = substr(***, 0, 20) . "...";

        $table .= "<div class='panel panel-default'>";
        $table .= "<div class='panel-heading'>";
        $table .= "<span class='pull-right'>" . $rows3[0]['country'] . ", " . $rows3[0]['city'] . "</span><h3 class='panel-title'><a href='" . BEZ_HOST . "?mode=order_view&vid=" . $val['id_order'] . "'>Заказ №" . $val['id_order'] . " от " . date('d.m.Y', strtotime($val['add_date'])) . "</a></h3>";
        $table .= "</div>";
        $table .= "<div class='panel-body'>" . $val['content'] . "</div>";
        $table .= "<div class='panel-footer'><span class='pull-right'>Файлов: " . $result . "</span>" . $section1 . "</div>";
        $table .= "</div>";
        $section1='';
    }
}


/* Если нажата кнопка на регистрацию,
  начинаем проверку */
if (isset($_POST['submit'])) {
    echo $_POST['email'];
    // Утюжим пришедшие данные
    if (empty($_POST['email']))
        $err[] = '<div class="alert alert-danger" role="alert">Поле Email не может быть пустым!</div>';
    else {
        if (emailValid($_POST['email']) === false)
            $err[] = '<div class="alert alert-danger" role="alert"><span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span><span class="sr-only">Error:</span>Не правильно введен E-mail</div>' . "\n";
    }

    if (count($err) > 0)
        echo showErrorMessage($err);
    else {
        /* Продолжаем проверять введенные данные
          Проверяем на совпадение пароли */
        /* if ($_POST['pass'] != $_POST['pass2'])
            $err[] = '<div class="alert alert-danger" role="alert">Пароли не совпадают</div>'; */

        // Проверяем наличие ошибок и выводим пользователю
        if (count($err) > 0)
            echo showErrorMessage($err);
        else {
            /* Проверяем существует ли у нас
              такой пользователь в БД */

            $sql = 'SELECT `login`
					FROM `' . BEZ_DBPREFIX . 'reg`
					WHERE `login` = :login';
            // Подготавливаем PDO выражение для SQL запроса
            $stmt = $db->prepare($sql);
            $stmt->bindValue(':login', $_POST['email'], PDO::PARAM_STR);

            $stmt->execute();

            $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

            if (count($rows) > 0)
                $err[] = '<div class="alert alert-danger" role="alert">К сожалению Логин: <b>' . $_POST['email'] . '</b> занят!</div>';

            // Проверяем наличие ошибок и выводим пользователю
            if (count($err) > 0)
                echo showErrorMessage($err);
            else {

                // Получаем ХЕШ соли
                //$salt = salt();

                // Солим пароль
                $pass1 = generate_password(6);
                $pass2 = md5($pass1);

                if(isset($_SESSION['currentLng'])){
                    $lang=$_SESSION['currentLng'];
                }else{
                    $lang=chkLng();
                }

                /* Если все хорошо, пишем данные в базу */
                $sql = 'INSERT INTO `bez_reg` SET `login` = :email,`pass` = :pass,`status` = 1,`defaultLng` = :defaultLng'; // внесены изменения
                // Подготавливаем PDO выражение для SQL запроса
                $stmt = $db->prepare($sql);
                $stmt->bindValue(':email', $_POST['email'], PDO::PARAM_STR);
                $stmt->bindValue(':pass', $pass2, PDO::PARAM_STR);
                $stmt->bindValue(':defaultLng', $lang, PDO::PARAM_STR);

                $stmt->execute();

                $getID = $db->lastInsertId();

                // Отправляем письмо для активации
                $url = BEZ_HOST . '?mode=reg&key=' . $_POST['email'];
                $title = 'Регистрация на https://industrialorder.com/';
                
                $message .= 'Спасибо за регистрацию.<br>' . PHP_EOL; // Для активации Вашего акаунта пройдите по ссылке <a href="' . $url . '">' . $url . '</a>
                $message .= 'Ваш логин: ' . $_POST['email'] . PHP_EOL . "<br>";
                $message .= 'Ваш пароль: ' . $pass1 .  PHP_EOL . "<br><br>";
                
                $message .= 'Thank you for registration.<br>' . PHP_EOL; // Для активации Вашего акаунта пройдите по ссылке <a href="' . $url . '">' . $url . '</a>
                $message .= 'Your login: ' . $_POST['email'] . PHP_EOL . "<br>";
                $message .= 'Your password: ' . $pass1 .  PHP_EOL . "<br>";




             

                sendMessageMail($_POST['email'], BEZ_MAIL_AUTOR, $title, $message);

                $_SESSION['user'] = true;
                $_SESSION['allready'] = NULL;
                $_SESSION['login'] = $_POST['email']; /* $_POST['email'] */
                $_SESSION['id_reg'] = $getID;

                $sql3 = 'UPDATE `bez_reg` SET `ssid` = :ssid WHERE `id_reg` = :id_reg'; // :r_comapny,
                $stmt = $db->prepare($sql3);
                $stmt->bindValue(':ssid', session_id(), PDO::PARAM_STR);
                $stmt->bindValue(':id_reg', $getID, PDO::PARAM_INT);
                $stmt->execute();

                $sql4 = 'UPDATE `bez_reg` SET `ip` = :ip WHERE `id_reg` = :id_reg'; // :r_comapny,
                $stmt = $db->prepare($sql4);
                $stmt->bindValue(':ip', $_SERVER['REMOTE_ADDR'], PDO::PARAM_STR);
                $stmt->bindValue(':id_reg', $getID, PDO::PARAM_INT);
                $stmt->execute();

            $order = htmlspecialchars(stripslashes($_REQUEST['order']));

            
            $array = $_POST['cat'];
                
                $c_select = explode(", ", $array);
                $len_sel = count($c_select);
                
              $sql0 = 'SELECT * FROM class_new WHERE name=:name';
                $stmt0 = $db->prepare($sql0);
                $stmt0->bindValue(':name', $c_select[0], PDO::PARAM_STR);
                $stmt0->execute();
                $rows0 = $stmt0->fetchAll(PDO::FETCH_ASSOC);
                $c_row = $rows0[0]['id'];
               $sql = 'SELECT id_reg FROM `' . BEZ_DBPREFIX . 'reg` WHERE `login`=:email';
                $stmt = $db->prepare($sql);
                $stmt->bindValue(':email', $_SESSION['login'], PDO::PARAM_STR);
                $stmt->execute();
                $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
                $id_reg = htmlspecialchars($rows[0]['id_reg']);
                /* Если все хорошо, пишем данные в базу */
                /* short_desc = :short_desc, ,
        c_urname = :c_urname,
        c_inn = :c_inn,
        c_kpp = :c_kpp,
        c_fio = :c_fio,
        c_dolg = :c_dolg,
        c_bank = :c_bank,
        c_bik = :c_bik,
        c_rs = :c_rs,
        c_ks = :c_ks */
                $sql2 = 'INSERT INTO `bez_companys` SET 
        c_section = :c_section,
        c_name = :c_name,
        full_desc = :full_desc,
        country = :country,
        city = :city,
        address = :address,
        c_tel = :c_tel,
        email = :email,
        site=:site,
        contact=:contact,
        service = :service,
        admin = :admin,
        agent_id = :agent_id';
                // Подготавливаем PDO выражение для SQL запроса
                $stmt = $db->prepare($sql2);
                //$category = implode(", ", $_POST['cat']);
                $stmt->bindValue(':c_section', $c_row, PDO::PARAM_STR);
                $stmt->bindValue(':c_name', $_POST['c_name'], PDO::PARAM_STR);
                //$stmt->bindValue(':short_desc', $_POST['short_desc'], PDO::PARAM_STR);
                $stmt->bindValue(':full_desc', $_POST['full_desc'], PDO::PARAM_STR);
                $stmt->bindValue(':country', $_POST['country'], PDO::PARAM_STR);
                $stmt->bindValue(':city', $_POST['city'], PDO::PARAM_STR);
                $stmt->bindValue(':address', $_POST['address'], PDO::PARAM_STR);
                $stmt->bindValue(':c_tel', $_POST['c_tel'], PDO::PARAM_STR);
                $stmt->bindValue(':email', $_POST['email'], PDO::PARAM_STR);
                $stmt->bindValue(':site', $_POST['site'], PDO::PARAM_STR);
                $stmt->bindValue(':contact', $_POST['contact'], PDO::PARAM_STR);
                $stmt->bindValue(':service', $_POST['service'], PDO::PARAM_STR);
                $stmt->bindValue(':admin', $id_reg, PDO::PARAM_STR);
                $stmt->bindValue(':agent_id', $_POST['agent_id'], PDO::PARAM_STR);
                //$stmt->bindValue(':c_urname', $_POST['c_urname'], PDO::PARAM_STR);
                //$stmt->bindValue(':c_inn', $_POST['c_inn'], PDO::PARAM_STR);
                //$stmt->bindValue(':c_kpp', $_POST['c_kpp'], PDO::PARAM_STR);
                //$stmt->bindValue(':c_fio', $_POST['c_fio'], PDO::PARAM_STR);
                //$stmt->bindValue(':c_dolg', $_POST['c_dolg'], PDO::PARAM_STR);
                //$stmt->bindValue(':c_bank', $_POST['c_bank'], PDO::PARAM_STR);
                //$stmt->bindValue(':c_bik', $_POST['c_bik'], PDO::PARAM_STR);
                //$stmt->bindValue(':c_rs', $_POST['c_rs'], PDO::PARAM_STR);
                //$stmt->bindValue(':c_ks', $_POST['c_ks'], PDO::PARAM_STR);

                $stmt->execute();
                $newID = $db->lastInsertId(); // при добавление новой компании присваиваем id так же присваиваем lastInsertId

                $_SESSION['r_company'] = $newID; // присваем одно из полей сессии lastInsertId; в массиве сессии появляется r_company

                echo $newID;
                /*
                $value =$stmt -> fetchall(PDO::FETCH_ASSOC);
                foreach($value as $val) {
                     if(in_array("", $val)) {
                          $valuation += 0.1;
                     }
                     echo $valuation;
                }
                 */
               
                // Начало обработчика загрузки файлов

                $folder = ROOT_DIR . '/uploads/logo/';
                $ext = array(".gif", ".jpg", ".jpeg", ".png");

                // Получаем расширение файла
                $file_extends = strtolower(strrchr($_FILES['userfile']['name'], '.'));
                // Генерируем случайное число
                $file_name = uniqid(rand(10000, 99999));
                // Формируем путь на сервере
                $uploadedFile = $folder . $file_name . $file_extends;
                $fileName = $file_name . $file_extends;

                if (!in_array($file_extends, $ext)) {
                    echo "<div class=\"alert alert-danger\" role=\"alert\">Такое нельзя загружать.</div>";
                } else {
                    $sql5 = 'UPDATE `bez_companys` SET c_logo = :c_logo WHERE id_company=:newID';
                    $stm = $db->prepare($sql5);
                    $stm->bindValue(':c_logo', $fileName, PDO::PARAM_STR);
                    $stm->bindValue(':newID', $newID, PDO::PARAM_STR);
                    $stm->execute();
                    if (is_uploaded_file($_FILES['userfile']['tmp_name'])) {
                        if (move_uploaded_file($_FILES['userfile']['tmp_name'], $uploadedFile)) {
                            echo "<div class=\"alert alert-success\" role=\"alert\">Файл загружен.</div>";
                            echo "<div class=\"alert alert-danger\" role=\"alert\">" . $sql5 . "</div>";
                            echo "<!-- file_extends: " . $file_extends . "<br>";
                            echo "file_name: " . $file_name . "<br>";
                            echo "uploadedFile: " . $uploadedFile . "<br>";
                            echo "fileName: " . $fileName . "<br>";
                            echo "newID: " . $newID . "<br> -->";
                        } else {
                            echo "<div class=\"alert alert-danger\" role=\"alert\">Во время загрузки файла произошла ошибка.</div>";
                        }
                    } else {
                        echo "<div class=\"alert alert-danger\" role=\"alert\">Файл не загружен.</div>";
                    }
                }

                // Конец обработчика загрузки файлов

                $sql3 = 'UPDATE `bez_reg` SET allready=:allready, company_id=:company_id WHERE id_reg=:id_reg';
                $stmt = $db->prepare($sql3);
                $stmt->bindValue(':allready', '1', PDO::PARAM_STR);
                $stmt->bindValue(':company_id', $newID, PDO::PARAM_STR);
                $stmt->bindValue(':id_reg', $id_reg, PDO::PARAM_STR);

                $stmt->execute();

                $_SESSION['allready'] = '1';

                $sql6 = 'SELECT * FROM `bez_companys` WHERE id_company=:id_company';
                $rev_pdo6 = $db -> prepare($sql6);
                $rev_pdo6 -> bindValue(':id_company', $newID, PDO::PARAM_STR);
                $rev_pdo6->execute();      
                 $value7 = $rev_pdo6 -> fetchall(PDO::FETCH_ASSOC);
                 $count = 0;
                 $array = array("c_section", "c_name", "short_desc",
                           "full_desc", "country", "city",
                           "address", "c_tel", "email", "site",
                           "contact", "service", "c_urname",
                           "c_inn", "c_kpp", "c_fio", "c_dolg",
                           "c_bank", "c_bik", "c_rs", "c_ks");
                        for($i = 0; $i < count($array); $i++) {
                            if( $value7[0][$array[$i]] !== 0 && $value7[0][$array[$i]] !== NULL && $value7[0][$array[$i]] !== 'http://' && $value7[0][$array[$i]] !== "") {
                                $count++;
                            }
                        }
                       $valuation = $count * 0.1;
                       $sql7 = "UPDATE `bez_companys` SET valuation=:valuation WHERE id_company=:id_company";
                $rev_pdo7 = $db -> prepare($sql7);
                $rev_pdo7 -> bindValue(':valuation', $valuation, PDO::PARAM_STR);
                $rev_pdo7 -> bindValue(':id_company', $newID, PDO::PARAM_STR);
                $rev_pdo7 -> execute();
                
                // Сбрасываем параметры
              for($i = 1; $i < $len_sel; $i++) { 
                if($c_select[$i] != ""){

                
                $sql11 = 'SELECT * FROM class_new WHERE name=:name';
                $stmt11 = $db->prepare($sql11);
                $stmt11->bindValue(':name', $c_select[$i], PDO::PARAM_STR);
                $stmt11->execute();
                $rows11 = $stmt11->fetchAll(PDO::FETCH_ASSOC);
                $c_row = $rows11[0]['id'];    

                $sql12 = 'SELECT c_section FROM bez_companys WHERE id_company=:id_company';
                $stmt12 = $db->prepare($sql12);
                $stmt12->bindValue(':id_company', $newID, PDO::PARAM_STR);
                $stmt12->execute();
                $rows12 = $stmt12->fetchAll(PDO::FETCH_ASSOC);
                $row12 = $rows12[0]['c_section'];
                        // "UPDATE `bez_companys` SET friends=CONCAT(friends, '$vid') WHERE id_company=:id_company";
                $sql10 = "UPDATE bez_companys SET c_section=CONCAT(c_section, ', $c_row') WHERE id_company=:id_company";
                $stmt10 = $db->prepare($sql10);
                $stmt10->bindValue(':id_company', $newID, PDO::PARAM_STR);
                $stmt10->execute();
                }
                }
                header('Location:' . BEZ_HOST . '?mode=order_add');
                exit;
           
            }
        }
    }
}
?>