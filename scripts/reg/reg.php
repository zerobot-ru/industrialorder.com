<?php
/**
 * Обработчик формы регистрации
 */

if ($_SESSION['currentLng'] == "ru-ru") {
    $pageTitle = 'Регистрация на сайте' . $label_PTitle;
    $pageDesc = 'Регистрация на сайте. Найдите своих клиентов и поставщиков, производителей оборудования и комплектующих, партнеров и инвесторов, сотрудников. В России и по всему Миру.';
} else {
    $pageTitle = 'Sign up page' . $label_PTitle;
    $pageDesc = 'Sign up page. Find your customers and suppliers, manufacturers of equipment and components, partners and investors, employees. In Russia and around the world.';
}

// Выводим сообщение об удачной регистрации
if (isset($_GET['status']) and $_GET['status'] == 'ok') {
    echo '<script>alert("Вы успешно зарегистрировались!")</script>';
    header('Location:' . BEZ_HOST . '?mode=auth');
}

// Выводим сообщение об удачной активации
if (isset($_GET['active']) and $_GET['active'] == 'ok')
    echo '<script>alert("Ваш аккаунт на https://industrialorder.com/ успешно активирован!")</script>';

// Производим активацию аккаунта
if (isset($_GET['key'])) {
    // Проверяем ключ
    $sql = 'SELECT *
			FROM `' . BEZ_DBPREFIX . 'reg`
			WHERE `login` = :key';          // внесено изменение
    // Подготавливаем PDO выражение для SQL запроса
    $stmt = $db->prepare($sql);
    $stmt->bindValue(':key', $_GET['key'], PDO::PARAM_STR);
    $stmt->execute();
    $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

    if (count($rows) == 0)
        $err[] = '<div class="alert alert-danger" role="alert">Ключ активации не верен!</div>';

    // Проверяем наличие ошибок и выводим пользователю
    if (count($err) > 0)
        echo showErrorMessage($err);
    else {
        // Получаем адрес пользователя
        $email = $rows[0]['login'];

        // Активируем аккаунт пользователя
        $sql = 'UPDATE `' . BEZ_DBPREFIX . 'reg`
				SET `status` = 1
				WHERE `login` = :email';
        // Подготавливаем PDO выражение для SQL запроса
        $stmt = $db->prepare($sql);
        $stmt->bindValue(':email', $email, PDO::PARAM_STR);
        $stmt->execute();

        // Отправляем письмо для активации
        $title = 'Ваш аккаунт на https://industrialorder.com/ успешно активирован';
        $message = 'Поздравляю Вас, Ваш аккаунт на https://industrialorder.com/ успешно активирован';

        sendMessageMail($email, BEZ_MAIL_AUTOR, $title, $message);

        /* Перенаправляем пользователя на
          нужную нам страницу */
        header('Location:' . BEZ_HOST . '?mode=reg&active=ok');
        exit;
    }
}

if ($user === FALSE) {
    include 'scripts/reg/reg_form.html';
} else {
    /* $err[] = 'Вы уже залогированы.';
    if (count($err) > 0) {
        echo showErrorMessage($err);
    } */
    header('Location:' . BEZ_HOST . '?mode=profile');
}

/* Если нажата кнопка на регистрацию,
  начинаем проверку */
if (isset($_POST['submit'])) {
    // Утюжим пришедшие данные
    if (empty($_POST['email']))
        $err[] = '<div class="alert alert-danger" role="alert">Поле Email не может быть пустым!</div>';
    else {
        if (emailValid($_POST['email']) === false)
            $err[] = '<div class="alert alert-danger" role="alert"><span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span><span class="sr-only">Error:</span>Не правильно введен E-mail</div>' . "\n";
    }

    /* if (empty($_POST['pass']))
        $err[] = '<div class="alert alert-danger" role="alert">Поле Пароль не может быть пустым</div>';

    if (empty($_POST['pass2']))
        $err[] = '<div class="alert alert-danger" role="alert">Поле Подтверждения пароля не может быть пустым</div>'; */

    // Проверяем наличие ошибок и выводим пользователю
    if (count($err) > 0)
        echo showErrorMessage($err);
    else {
        /* Продолжаем проверять введенные данные
          Проверяем на совпадение пароли */
        /* if ($_POST['pass'] != $_POST['pass2'])
            $err[] = '<div class="alert alert-danger" role="alert">Пароли не совпадают</div>'; */

        // Проверяем наличие ошибок и выводим пользователю
        if (count($err) > 0)
            echo showErrorMessage($err);
        else {
            /* Проверяем существует ли у нас
              такой пользователь в БД */

            $sql = 'SELECT `login`
					FROM `' . BEZ_DBPREFIX . 'reg`
					WHERE `login` = :login';
            // Подготавливаем PDO выражение для SQL запроса
            $stmt = $db->prepare($sql);
            $stmt->bindValue(':login', $_POST['email'], PDO::PARAM_STR);

            $stmt->execute();

            $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

            if (count($rows) > 0)
                $err[] = '<div class="alert alert-danger" role="alert">К сожалению Логин: <b>' . $_POST['email'] . '</b> занят!</div>';

            // Проверяем наличие ошибок и выводим пользователю
            if (count($err) > 0)
                echo showErrorMessage($err);
            else {

                // Получаем ХЕШ соли
                //$salt = salt();

                // Солим пароль
                $pass1 = generate_password(6);
                $pass2 = md5($pass1);

                if(isset($_SESSION['currentLng'])){
                    $lang=$_SESSION['currentLng'];
                }else{
                    $lang=chkLng();
                }

                /* Если все хорошо, пишем данные в базу */
                $sql = 'INSERT INTO `bez_reg` SET `login` = :email,`pass` = :pass,`status` = 1,`defaultLng` = :defaultLng'; // внесены изменения
                // Подготавливаем PDO выражение для SQL запроса
                $stmt = $db->prepare($sql);
                $stmt->bindValue(':email', $_POST['email'], PDO::PARAM_STR);
                $stmt->bindValue(':pass', $pass2, PDO::PARAM_STR);
                $stmt->bindValue(':defaultLng', $lang, PDO::PARAM_STR);

                $stmt->execute();

                $getID = $db->lastInsertId();

                // Отправляем письмо для активации
                $url = BEZ_HOST . '?mode=reg&key=' . $_POST['email'];
                $title = 'Регистрация на https://industrialorder.com/';
                
                $message .= 'Спасибо за регистрацию.<br>' . PHP_EOL; // Для активации Вашего акаунта пройдите по ссылке <a href="' . $url . '">' . $url . '</a>
                $message .= 'Ваш логин: ' . $_POST['email'] . PHP_EOL . "<br>";
                $message .= 'Ваш пароль: ' . $pass1 .  PHP_EOL . "<br><br>";
                
                $message .= 'Thank you for registration.<br>' . PHP_EOL; // Для активации Вашего акаунта пройдите по ссылке <a href="' . $url . '">' . $url . '</a>
                $message .= 'Your login: ' . $_POST['email'] . PHP_EOL . "<br>";
                $message .= 'Your password: ' . $pass1 .  PHP_EOL . "<br>";




             

                sendMessageMail($_POST['email'], BEZ_MAIL_AUTOR, $title, $message);

                $_SESSION['user'] = true;
                $_SESSION['allready'] = NULL;
                $_SESSION['login'] = $_POST['email']; /* $_POST['email'] */
                $_SESSION['id_reg'] = $getID;

                $sql3 = 'UPDATE `bez_reg` SET `ssid` = :ssid WHERE `id_reg` = :id_reg'; // :r_comapny,
                $stmt = $db->prepare($sql3);
                $stmt->bindValue(':ssid', session_id(), PDO::PARAM_STR);
                $stmt->bindValue(':id_reg', $getID, PDO::PARAM_INT);
                $stmt->execute();

                $sql4 = 'UPDATE `bez_reg` SET `ip` = :ip WHERE `id_reg` = :id_reg'; // :r_comapny,
                $stmt = $db->prepare($sql4);
                $stmt->bindValue(':ip', $_SERVER['REMOTE_ADDR'], PDO::PARAM_STR);
                $stmt->bindValue(':id_reg', $getID, PDO::PARAM_INT);
                $stmt->execute();

            $order = htmlspecialchars(stripslashes($_REQUEST['order']));
                // Сбрасываем параметры
                header('Location:' . BEZ_HOST . '?mode=company_add&order=' . $order);
                exit;
            }
        }
    }
}
?>