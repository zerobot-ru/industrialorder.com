<?php
/**
 * Created by PhpStorm.
 * User: Aleksej Kazin
 * Date: 21.04.2017
 * Time: 10:29
 */

if ($_SESSION['currentLng'] == "ru-ru") {
    $pageTitle = 'Добавить фотографии' . $label_PTitle;
    $pageDesc = 'Добавить фотографии';
} else {
    $pageTitle = 'Add photos' . $label_PTitle;
    $pageDesc = 'Add photos';
}

if ($user === false) {
    header('Location:' . BEZ_HOST . '?mode=auth');
    exit;
}
if ($user === true) {
    if (isset($_POST['submit'])) {

        // Начало обработчика загрузки файлов

        $ext = array(".gif", ".jpg", ".jpeg", ".png", ".tif", ".tiff");
        $folder = ROOT_DIR . '/uploads/companys/goods/';
        $result = count($_FILES);

        if ($result > 20) {
            echo '<div class="alert alert-danger" role="alert">Слишком много файлов!</div>';
        } else {

            for ($i = 0; $i < count($_FILES['uploadFile']['name']); $i++) {

                // Получаем расширение файла
                $file_ext[$i] = strtolower(strrchr($_FILES['uploadFile']['name'][$i], '.'));
                // Генерируем случайное число
                $file_name = uniqid(rand(10000, 99999));
                // Формируем путь на сервере
                $uploadedFile[$i] = $folder . $file_name . $file_ext[$i];
                $fileName = $file_name . $file_ext[$i];

                if (!in_array($file_ext[$i], $ext)) {
                    echo "<div class=\"alert alert-danger\" role=\"alert\">Такое нельзя загружать.</div>";
                } else {

                    $sql3 = 'INSERT INTO `bez_company_files` SET file=:file, company_id=:company_id, file_type=:file_type'; // :r_comapny,
                    $stmt = $db->prepare($sql3);
                    $stmt->bindValue(':file', $fileName, PDO::PARAM_STR);
                    $stmt->bindValue(':company_id', $_SESSION['r_company'], PDO::PARAM_INT);
                    $stmt->bindValue(':file_type', '0', PDO::PARAM_INT);
                    $stmt->execute();

                    if (is_uploaded_file($_FILES['uploadFile']['tmp_name'][$i])) {
                        if (move_uploaded_file($_FILES['uploadFile']['tmp_name'][$i], $uploadedFile[$i])) {
                            echo "<div class=\"alert alert-success\" role=\"alert\">Файл загружен.</div>";
                        } else {
                            echo "<div class=\"alert alert-danger\" role=\"alert\">Во время загрузки файла произошла ошибка.</div>";
                        }
                    } else {
                        echo "<div class=\"alert alert-danger\" role=\"alert\">Файл не загружен.</div>";
                    }
                }
            }
        }
        // Окончание обработчика загрузки файлов
        // Сбрасываем параметры
        header('Location:' . BEZ_HOST . '?mode=profile');
        exit;
    }
}