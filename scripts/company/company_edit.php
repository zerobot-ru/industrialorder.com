<?php
ini_set('display_errors','Off');
if ($_SESSION['currentLng'] == "ru-ru") {
    $pageTitle = 'Редактирование компании' . $label_PTitle;
    $pageDesc = 'Страница редактирования компании';
} else {
    $pageTitle = 'Company edit' . $label_PTitle;
    $pageDesc = 'Company edit page';
}

//Проверяем зашел ли пользователь
if ($user === false) {
    header('Location:' . BEZ_HOST . '?mode=auth');
    exit;
}
if ($user === true) {
     $sql = 'SELECT * FROM `' . BEZ_DBPREFIX . 'companys` WHERE `id_company`=:id_company';
    $stmt = $db->prepare($sql);
    $stmt->bindValue(':id_company', $_SESSION['r_company'], PDO::PARAM_STR);
    $stmt->execute();

    $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

    $id_company = $rows[0]['id_company'];
    $c_section = $rows[0]['c_section'];
    $c_name = htmlspecialchars($rows[0]['c_name']);
    $short_desc = nl2br($rows[0]['short_desc']);
    $full_desc = nl2br($rows[0]['full_desc']);
    $country = $rows[0]['country'];
    $city = $rows[0]['city'];
    $address = $rows[0]['address'];
    $c_tel = $rows[0]['c_tel'];
    $email = $rows[0]['email'];
    $site = $rows[0]['site'];
    $contact = $rows[0]['contact'];
    $service = $rows[0]['service'];
    
  if($_SESSION['currentLng']=="ru-ru"){
    $sql111 = 'SELECT * FROM `type_new`';
} else {
    $sql111 = 'SELECT * FROM `type_new_eng`';
}
if(isset($_REQUEST['empty'])){
    $summary1 = "Ничего не найдено";
}
 $stmt111 = $db->prepare($sql111);
        //$stmt->bindValue(':order_num', $val['id_order'], PDO::PARAM_INT);
        $stmt111->execute();
        $rows111 = $stmt111->fetchAll(PDO::FETCH_ASSOC);
        $res .= '<div id = "invisible">';
        
        
foreach($rows111 as $row111){
    $res .= '<div class ="option">';
    $res .= '<div class = "option2"><strong id ="strong">'. $row111['name'] . '</strong></div>';
 
    if ($_SESSION['currentLng'] == "ru-ru") {
    $sql1111 = 'SELECT * FROM `class_new` WHERE parent=:parent';
    } else {
         $sql1111 = 'SELECT * FROM `class_new_eng` WHERE parent=:parent';
    }
        $stmt1111 = $db->prepare($sql1111);
        $stmt1111->bindValue(':parent', $row111['id'], PDO::PARAM_INT);
        $stmt1111->execute();
        $rows1111 = $stmt1111->fetchAll(PDO::FETCH_ASSOC);
    $res .= '<ul class = "option1">';
    foreach($rows1111 as $row1111){
        $res .=  '<input type="checkbox" class="subsection" style="float:left; transform:scale(1.5)" name = "' . $row1111["id"] . '"><li style = " margin-left: 20px;">' . $row1111["name"] . '</li>';
        
        
    }
    $res .= '</ul></div>';
}
 $res .= '</div>';
 
 $stmt1 = $db->prepare($sql1);
if ($stmt1->execute()) {
    $summary = '';
    $rows1 = $stmt1->fetchAll(PDO::FETCH_ASSOC);
    foreach ($rows1 as $val1) {
        /* $summary .= '<option value="' . $val1['code'] . '">' . $val1['name'] . '</option>'; */
        if($_SESSION['currentLng']=="ru-ru"){
            $sql22 = 'SELECT * FROM `class_new` WHERE parent=' . $val1['id'];
        } else {
            $sql22 = 'SELECT * FROM `class_new_eng` WHERE parent=' . $val1['id'];
        }

         // . ' ORDER BY cat_id ASC';
        $stmt22 = $db->prepare($sql22);
        $summary .= '<optgroup label="' . $val1['name'] . '">'; // '<input type="checkbox" value="' . $val1['type_id'] . '">' . $val1['type_name']
        //Выводим контент
        if ($stmt22->execute()) {
            $rows22 = $stmt22->fetchAll(PDO::FETCH_ASSOC);
            foreach ($rows22 as $val22) {
                $summary .= '<option value="' . $val22['id'] . '">' . $val22['name'] . '</option>';
            }
        }
        $summary .= '</optgroup>';
    }
}


    if (isset($_POST['submit'])) {
        

            
        if(isset($_FILES['userfile']['name'])){
            $folder = ROOT_DIR . '/uploads/logo/';
            
        
        
                $ext = array(".gif", ".jpg", ".jpeg", ".png");

                // Получаем расширение файла
                $file_extends = strtolower(strrchr($_FILES['userfile']['name'], '.'));
                // Генерируем случайное число
                if(in_array($file_extends, $ext)) {
                     $sql6 = 'SELECT c_logo FROM `bez_companys` WHERE id_company=:newID';
                    $stm1 = $db->prepare($sql6);
                    $stm1->bindValue(':newID', $_SESSION['r_company'], PDO::PARAM_STR);
                    $stm1->execute();
                    $select_result1 = $stm1 -> fetch(PDO::FETCH_ASSOC);

        $unlink = $folder.$select_result1['c_logo'];
        unlink($unlink);
        
                $file_name = uniqid(rand(10000, 99999));
                // Формируем путь на сервере
                $uploadedFile = $folder . $file_name . $file_extends;
                $fileName = $file_name . $file_extends;
                
                    $sql5 = 'UPDATE `bez_companys` SET c_logo = :c_logo WHERE id_company=:newID';
                    $stm = $db->prepare($sql5);
                    $stm->bindValue(':c_logo', $fileName, PDO::PARAM_STR);
                    $stm->bindValue(':newID', $_SESSION['r_company'], PDO::PARAM_STR);
                    $stm->execute();
                
                 move_uploaded_file($_FILES['userfile']['tmp_name'], $uploadedFile);   
        }
        }
        
        
        
                $array = $_POST['cat'];
                $c_select = explode(" , ", $array);
                $len_sel = count($c_select);
               if ($_SESSION['currentLng'] == "ru-ru") {
              $sql0 = 'SELECT * FROM class_new WHERE name=:name';
               } else {
                   $sql0 = 'SELECT * FROM class_new_eng WHERE name=:name';
               }
                $stmt0 = $db->prepare($sql0);
                $stmt0->bindValue(':name', $c_select[0], PDO::PARAM_STR);
                $stmt0->execute();
                $rows0 = $stmt0->fetchAll(PDO::FETCH_ASSOC);
                $c_row = $rows0[0]['id'];
              /*
                $sql2 = 'UPDATE `bez_companys` SET c_section = :c_section';
                $stmt2 = $db->prepare($sql2);
                $stmt2 -> bindValue(':c_section', $c_row, PDO::PARAM_STR);
                $stmt2->execute();
*/
        $sql2 = 'UPDATE `' . BEZ_DBPREFIX . 'companys` SET
        c_section=:c_section,
        c_name=:c_name,
        full_desc=:full_desc,
        country=:country,
        city=:city,
        address=:address,
        c_tel=:c_tel,
        email=:email,
        site=:site,
        contact=:contact,
        service=:service WHERE id_company=:id_company';
        //Подготавливаем PDO выражение для SQL запроса
        $stmt = $db->prepare($sql2);
        $stmt->bindValue(':id_company', $_SESSION['r_company'], PDO::PARAM_STR);
        $stmt->bindValue(':c_section', $c_row, PDO::PARAM_STR);
        $stmt->bindValue(':c_name', $_POST['c_name'], PDO::PARAM_STR);
        $stmt->bindValue(':full_desc', $_POST['full_desc'], PDO::PARAM_STR);
        $stmt->bindValue(':country', $_POST['country'], PDO::PARAM_STR);
        $stmt->bindValue(':city', $_POST['city'], PDO::PARAM_STR);
        $stmt->bindValue(':address', $_POST['address'], PDO::PARAM_STR);
        $stmt->bindValue(':c_tel', $_POST['c_tel'], PDO::PARAM_STR);
        $stmt->bindValue(':email', $_POST['email'], PDO::PARAM_STR);
        $stmt->bindValue(':site', $_POST['site'], PDO::PARAM_STR);
        $stmt->bindValue(':contact', $_POST['contact'], PDO::PARAM_STR);
        $stmt->bindValue(':service', $_POST['service'], PDO::PARAM_STR);
        $stmt->execute();
        
         // Сбрасываем параметры
              for($i = 1; $i < $len_sel; $i++) { 
                if($c_select[$i] != ""){

                $sql11 = 'SELECT * FROM class_new WHERE name=:name';
                $stmt11 = $db->prepare($sql11);
                $stmt11->bindValue(':name', $c_select[$i], PDO::PARAM_STR);
                $stmt11->execute();
                $rows11 = $stmt11->fetchAll(PDO::FETCH_ASSOC);
                $c_row = $rows11[0]['id'];    

                $sql12 = 'SELECT c_section FROM bez_companys WHERE id_company=:id_company';
                $stmt12 = $db->prepare($sql12);
                $stmt12->bindValue(':id_company', $_SESSION['r_company'], PDO::PARAM_STR);
                $stmt12->execute();
                $rows12 = $stmt12->fetchAll(PDO::FETCH_ASSOC);
                $row12 = $rows12[0]['c_section'];
                        // "UPDATE `bez_companys` SET friends=CONCAT(friends, '$vid') WHERE id_company=:id_company";
                $sql10 = "UPDATE bez_companys SET c_section=CONCAT(c_section, ', $c_row') WHERE id_company=:id_company";
                $stmt10 = $db->prepare($sql10);
                $stmt10->bindValue(':id_company', $_SESSION['r_company'], PDO::PARAM_STR);
                $stmt10->execute();
                }
                }
                
        header('Location:' . BEZ_HOST . '?mode=profile');
        exit;
    }
}
?>