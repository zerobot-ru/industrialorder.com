<?php
/**
 * Created by PhpStorm.
 * User: Aleksej Kazin
 * Date: 21.03.2017
 * Time: 23:51
 */

$pageTitle = 'Администрирование компании';
$h1Title = '123';
$pageDesc = '';

if ($user==false){
    header('Location:' . BEZ_HOST . '?mode=auth');
    exit;
}

if($user==true) {

    $sql111 = 'SELECT * FROM `' . BEZ_DBPREFIX . 'reg` WHERE `company_id`=' . $_SESSION['r_company'] . ' ORDER BY id_reg ASC';
    $stmt = $db->prepare($sql111);
    $stmt->execute();
    $row111 = $stmt->fetchAll(PDO::FETCH_ASSOC);
    foreach ($row111 as $val111) {

        $e_name = $val111['name'];
        $e_department = $val111['department'];
        $e_position = $val111['position'];
        $e_tel = $val111['tel'];
        $e_email = $val111['login'];

        $employees .= $label_FullName . ': ' . $e_name . '<br>';
        $employees .= $label_Department . ': ' . $e_department . '<br>';
        $employees .= $label_Position . ': ' . $e_position . '<br>';
        $employees .= $label_Tel . ': ' . $e_tel . '<br>';
        $employees .= 'E-mail: ' . $e_email . '<hr>';

    }

    if (isset($_POST['submit'])) {
        // Утюжим пришедшие данные
        if (empty($_POST['email']))
            $err[] = '<div class="alert alert-danger" role="alert">Поле Email не может быть пустым!</div>';
        else {
            if (emailValid($_POST['email']) === false)
                $err[] = '<div class="alert alert-danger" role="alert"><span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span><span class="sr-only">Error:</span>Не правильно введен E-mail</div>' . "\n";
        }

        if (empty($_POST['pass']))
            $err[] = '<div class="alert alert-danger" role="alert">Поле Пароль не может быть пустым</div>';

        // Проверяем наличие ошибок и выводим пользователю
        if (count($err) > 0)
            echo showErrorMessage($err);
        else {

            // Проверяем наличие ошибок и выводим пользователю
            if (count($err) > 0)
                echo showErrorMessage($err);
            else {

                $sql = 'SELECT `login`
					FROM `' . BEZ_DBPREFIX . 'reg`
					WHERE `login` = :login';
                // Подготавливаем PDO выражение для SQL запроса
                $stmt = $db->prepare($sql);
                $stmt->bindValue(':login', $_POST['email'], PDO::PARAM_STR);

                $stmt->execute();

                $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

                if (count($rows) > 0)
                    $err[] = '<div class="alert alert-danger" role="alert">К сожалению Логин: <b>' . $_POST['email'] . '</b> занят!</div>';

                // Проверяем наличие ошибок и выводим пользователю
                if (count($err) > 0)
                    echo showErrorMessage($err);
                else {

                    $pass = md5($_POST['pass']);

                    /* Если все хорошо, пишем данные в базу */
                    $sql = 'INSERT INTO `bez_reg` SET `login` = :email, `pass` = :pass, `status` = 1, `name` = :name, `department` = :department, `position` = :position, `tel` = :tel, `allready` = 1, `company_id` = :company_id'; // внесены изменения
                    // Подготавливаем PDO выражение для SQL запроса
                    $stmt = $db->prepare($sql);
                    $stmt->bindValue(':email', $_POST['email'], PDO::PARAM_STR);
                    $stmt->bindValue(':pass', $pass, PDO::PARAM_STR);
                    $stmt->bindValue(':name', $_POST['user_name'], PDO::PARAM_STR);
                    $stmt->bindValue(':department', $_POST['department'], PDO::PARAM_STR);
                    $stmt->bindValue(':position', $_POST['position'], PDO::PARAM_STR);
                    $stmt->bindValue(':tel', $_POST['tel'], PDO::PARAM_STR);
                    $stmt->bindValue(':company_id', $_SESSION['r_company'], PDO::PARAM_STR);

                    $stmt->execute();

                    // Отправляем письмо для активации
                    $url = BEZ_HOST . '?mode=reg&key=' . $_POST['email'];
                    $title = 'Регистрация на https://industrialorder.com/';
                    $message = 'Для Вас создан аккаунт на сайте industrialorder.com\r\n<br><br>';
                    $message .= 'Данные для входа:\r\n<br><br>';
                    $message.='Логин: ' . $_POST['email'] . '\r\n<br>';
                    $message.='Пароль: ' . $_POST['pass'] . '\r\n<br><br>';
                    $message.='---';

                    sendMessageMail($_POST['email'], BEZ_MAIL_AUTOR, $title, $message);

                    // Сбрасываем параметры
                    header('Location:' . BEZ_HOST . '?mode=company_admin');
                    exit;
                }
            }
        }
    }

}