<?php
if ($_SESSION['currentLng'] == "ru-ru") {
    $pageTitle = 'Поиск заказов' . $label_PTitle;
    $pageDesc = 'В данном разделе публикуются заказы, которые оставили наши посетители. Если вы можете и готовы сделать заказ - связывайтесь с заказчиком напрямую';
} else {
    $pageTitle = 'Search orders' . $label_PTitle;
    $pageDesc = 'This section publishes the orders that our visitors left. If you can and are ready to make an order - contact the customer directly';
}

include 'bd.php';
if($_SESSION['currentLng']=="ru-ru"){
    $sql111 = 'SELECT * FROM `type_new`';
} else {
    $sql111 = 'SELECT * FROM `type_new_eng`';
}
if(isset($_REQUEST['empty'])){
    $summary1 = "Ничего не найдено";
}
        $stmt111 = $db->prepare($sql111);
        //$stmt->bindValue(':order_num', $val['id_order'], PDO::PARAM_INT);
        $stmt111->execute();
        $rows111 = $stmt111->fetchAll(PDO::FETCH_ASSOC);
        $res .= '<div id = "invisible">';
        
        
foreach($rows111 as $row111){
    $res .= '<div class ="option">';
    $res .= '<div class = "option2"><strong id ="strong">'. $row111['name'] . '</strong></div>';
 
    if ($_SESSION['currentLng'] == "ru-ru") {
    $sql1111 = 'SELECT * FROM `class_new` WHERE parent=:parent';
    } else {
         $sql1111 = 'SELECT * FROM `class_new_eng` WHERE parent=:parent';
    }
        $stmt1111 = $db->prepare($sql1111);
        $stmt1111->bindValue(':parent', $row111['id'], PDO::PARAM_INT);
        $stmt1111->execute();
        $rows1111 = $stmt1111->fetchAll(PDO::FETCH_ASSOC);
    $res .= '<ul class = "option1">';
    foreach($rows1111 as $row1111){
        $res .=  '<input type="checkbox" class="subsection" style="float:left; transform:scale(1.5)" name = "' . $row1111["id"] . '"><li style = " margin-left: 20px;">' . $row1111["name"] . '</li>';
        
        
    }
    $res .= '</ul></div>';
}
 $res .= '</div>';
 
 $stmt1 = $db->prepare($sql1);
if ($stmt1->execute()) {
    $summary = '';
    $rows1 = $stmt1->fetchAll(PDO::FETCH_ASSOC);
    foreach ($rows1 as $val1) {
        /* $summary .= '<option value="' . $val1['code'] . '">' . $val1['name'] . '</option>'; */
        if($_SESSION['currentLng']=="ru-ru"){
            $sql22 = 'SELECT * FROM `class_new` WHERE parent=' . $val1['id'];
        } else {
            $sql22 = 'SELECT * FROM `class_new_eng` WHERE parent=' . $val1['id'];
        }

         // . ' ORDER BY cat_id ASC';
        $stmt22 = $db->prepare($sql22);
        $summary .= '<optgroup label="' . $val1['name'] . '">'; // '<input type="checkbox" value="' . $val1['type_id'] . '">' . $val1['type_name']
        //Выводим контент
        if ($stmt22->execute()) {
            $rows22 = $stmt22->fetchAll(PDO::FETCH_ASSOC);
            foreach ($rows22 as $val22) {
                $summary .= '<option value="' . $val22['id'] . '">' . $val22['name'] . '</option>';
            }
        }
        $summary .= '</optgroup>';
    }
}

$sql = 'SELECT * FROM `' . BEZ_DBPREFIX . 'orders` WHERE DATE(end_date)>=CURDATE() ORDER BY id_order DESC LIMIT 10';
$stmt = $db->prepare($sql);

//Выводим контент
if ($stmt->execute()) {
    $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

    foreach ($rows as $val) {

        $s_select = explode(", ", $val['o_section']);
        /* foreach ($s_select as $val1) {
            $sql1 = 'SELECT * FROM `' . BEZ_DBPREFIX . 'cat` WHERE cat_id=' . $val1; // . ' ORDER BY cat_id ASC'
            $stmt1 = $db->prepare($sql1);
            //Выводим контент
            if ($stmt1->execute()) {
                $rows1 = $stmt1->fetchAll(PDO::FETCH_ASSOC);
                foreach ($rows1 as $val11) {
                    $section1 .= '<span class="label label-default">' . $val11['cat_name'] . '</span> ';
                }
            }
        } */

        foreach ($s_select as $val1){
            if ($_SESSION['currentLng'] == "ru-ru") {
            $sql1='SELECT * FROM `class_new` WHERE code=' . $val1;
            } else {
             $sql1='SELECT * FROM `class_new_eng` WHERE code=' . $val1;   
            }
            $stmt1=$db->prepare($sql1);
            if($stmt1->execute()){
                $rows1=$stmt1->fetchAll(PDO::FETCH_ASSOC);
                foreach ($rows1 as $val11){
                    $section1.='<strong>' . $val11['name'] . '</strong>';
                }
            }
        }

        $sql2 = 'SELECT order_num FROM `' . BEZ_DBPREFIX . 'files` WHERE `order_num` = :order_num';
        $stmt = $db->prepare($sql2);
        $stmt->bindValue(':order_num', $val['id_order'], PDO::PARAM_INT);
        $stmt->execute();
        $rows2 = $stmt->fetchAll(PDO::FETCH_ASSOC);

        $result = count($rows2);

        $sql3 = 'SELECT c_name, country, city FROM `' . BEZ_DBPREFIX . 'companys` WHERE `id_company` = :id_company';
        $stmt = $db->prepare($sql3);
        $stmt->bindValue(':id_company', $val['o_company'], PDO::PARAM_INT);
        $stmt->execute();
        $rows3 = $stmt->fetchAll(PDO::FETCH_ASSOC);

        $cName = $rows3[0]['c_name'];
        //$subContent = substr(***, 0, 20) . "...";

        $table .= "<div class='panel panel-default'>";
        $table .= "<div class='panel-heading'>";
        $table .= "<span class='pull-right'>" . $rows3[0]['country'] . ", " . $rows3[0]['city'] . "</span><h3 class='panel-title'><a href='" . BEZ_HOST . "?mode=order_view&vid=" . $val['id_order'] . "'>Заказ №" . $val['id_order'] . " от " . date('d-m-Y', strtotime($val['add_date'])) . "</a></h3>";
        $table .= "</div>";
        $table .= "<div class='panel-body'>" . $val['content'] . "</div>";
        $table .= "<div class='panel-footer'><span class='pull-right'>Файлов: " . $result . "</span>" . $section1 . "</div>";
        $table .= "</div>";
        $section1='';
    }
    //$table .= "</article><!-- /Article --></div></div><!-- /container -->";
    //echo $table;
}

if(!isset($_REQUEST['page'])){
    $page = 1;
}
else {
    $page = htmlspecialchars(stripcslashes($_REQUEST['page']));
}
    $offset = $page * 10;
$sql2 = "SELECT * FROM `" . BEZ_DBPREFIX . "orders` ORDER BY id_order DESC LIMIT 10 OFFSET $offset";
$stmt2 = $db->prepare($sql2);
    $stmt2->execute();
    $rows2 = $stmt2->fetchAll(PDO::FETCH_ASSOC);
    foreach($rows2 as $row2){
        if ($_SESSION['currentLng'] == "ru-ru") {
        $sql3 = 'SELECT * FROM class_new WHERE id=:id';
        } else {
            $sql3 = 'SELECT * FROM class_new_eng WHERE id=:id';
        }
        $stmt3 = $db->prepare($sql3);
        $stmt3 -> bindValue(':id',  $row2['o_section']);
        $stmt3->execute();
        $res1 = $stmt3 -> fetchAll(PDO::FETCH_ASSOC);
   
        $all_orders .= "<div class='panel panel-default'>";
        $all_orders .= "<div class='panel-heading'>";
        $all_orders .= "<!-- span class='pull-right'></span -->";
        $all_orders .= "<h3 class='panel-title'><a href='" . BEZ_HOST . "?mode=order_view&vid=" . $row2['id_order'] . "'>" . $row2['o_title'] . "</a></h3>";
        $all_orders .= "</div><div class='panel-body'>";
        $all_orders .= '<div class="col-md-8">';
        
        $all_orders .= $row2['content'] . "</div>";
        $all_orders .= "<div class='col-md-4'><span class='pull-right'><ul>";
        $all_orders .= "<li>" . $label_DateAdding . ": " . date('d-m-Y', strtotime($row2['add_date'])) . "</a></li>";
        $all_orders .= "</ul></span>";
        $all_orders .= "</div></div><div class='panel-footer'>" . $res1[0]['name'] . "</div>";
        $all_orders .= "</div>";
    }

    
    $sql3 = "SELECT * FROM `" . BEZ_DBPREFIX . "orders`";
$stmt3 = $db->prepare($sql3);
$stmt3->execute();
$rows3 = $stmt3 -> fetchall(PDO::FETCH_ASSOC);

    $length = count($rows3); //Количество тем в форуме
$pages = floor(($length) / 10); //Количество страниц
    if($length > 10) {
        $show .= "<div class = 'col-md-offset-6'>";
     
    if($page != 1) {
        $show .="<a href='?mode=all_orders&page=1' title = 'В начало'><<</a> ";
        $show .="<a href='?mode=all_orders&page=" . ($page - 1) . "' title = 'Предыдущая'><</a> ";
    }
//Показать текущую страницу и +- 3
    if(($page - 4) > 0) {
    $show .= " ... ";
    }
    if(($page - 3) > 0) {
         $show .="<a href='?mode=all_orders&page=" . ($page - 3) . "'>" . ($page - 3) . " </a>";
    }
    if(($page - 2) > 0) {
         $show .="<a href='?mode=all_orders&page=" . ($page - 2) . "'>" . ($page - 2) . " </a>";
    }
    if(($page - 1) > 0) {
         $show .="<a href='?mode=all_orders&page=" . ($page - 1) . "'>" . ($page - 1) . " </a>";
    }
    if(($page) > 0) {
         $show .="<a href='?mode=all_orders&page=" . $page . "'><strong>" . $page . " </strong></a>";
    }
    if(($page + 1) < $pages+1) {
         $show .="<a href='?mode=all_orders&page=" . ($page + 1) . "'>" . ($page + 1) . " </a>";
    }
    if(($page + 2) < $pages+1) {
         $show .="<a href='?mode=all_orders&page=" . ($page + 2) . "'>" . ($page + 2) . " </a>";
    }
    if(($page + 3) < $pages+1) {
         $show .="<a href='?mode=all_orders&page=" . ($page + 3) . "'>" . ($page + 3) . " </a>";
    }
    if(($page + 4) < $pages+1) {
        $show .= " ... ";
    }
    
    /*for($i = 0; $i < $pages; $i++) {
        $show .="<a href='?mode=view_question&g_subject=2&page=" . ($i + 1) . "'>" . ($i+1) . " </a>";
}*/
    if($page != $pages) {
        $show .="<a href='?mode=all_orders&page=" . ($page + 1) . "' title = 'Следующая'>></a> ";
        $show .="<a href='?mode=all_orders&page=". $pages . "' title = 'В конец'>>></a> ";
}
    $show .="</div>";
    }