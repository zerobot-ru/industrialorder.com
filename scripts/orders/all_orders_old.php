<?php

if ($_SESSION['currentLng'] == "ru-ru") {
    $pageTitle = 'Поиск заказов' . $label_PTitle;
    $pageDesc = 'В данном разделе публикуются заказы, которые оставили наши посетители. Если вы можете и готовы сделать заказ - связывайтесь с заказчиком напрямую';
} else {
    $pageTitle = 'Search orders' . $label_PTitle;
    $pageDesc = 'This section publishes the orders that our visitors left. If you can and are ready to make an order - contact the customer directly';
}

/**
 * Обработчик вывода всех заказов
 */
//Запрос на выборку контента согласно роли

/* $sql1 = 'SELECT * FROM `' . BEZ_DBPREFIX . 'type` ORDER BY type_id ASC';
$stmt1 = $db->prepare($sql1);
//Выводим контент
if ($stmt1->execute()) {
    $summary = '';
    $rows1 = $stmt1->fetchAll(PDO::FETCH_ASSOC);
    foreach ($rows1 as $val1) {
        $sql22 = 'SELECT * FROM `' . BEZ_DBPREFIX . 'cat` WHERE id_type=' . $val1['type_id'] . ' ORDER BY cat_id ASC';
        $stmt22 = $db->prepare($sql22);
        $summary .= '<optgroup label="' . $val1['type_name'] . '">'; // '<input type="checkbox" value="' . $val1['type_id'] . '">' . $val1['type_name']
        //Выводим контент
        if ($stmt22->execute()) {
            $rows22 = $stmt22->fetchAll(PDO::FETCH_ASSOC);
            foreach ($rows22 as $val22) {
                $summary .= '<option value="' . $val22['cat_id'] . '">' . $val22['cat_name'] . '</option>';
            }
        }
        $summary .= '</optgroup>';
    }
}

$sql1 = 'SELECT * FROM `class_okved` ORDER BY name ASC';
$stmt1 = $db->prepare($sql1);
if ($stmt1->execute()) {
    $summary = '';
    $rows1 = $stmt1->fetchAll(PDO::FETCH_ASSOC);
    foreach ($rows1 as $val1) {
        $summary .= '<option value="' . $val1['code'] . '">' . $val1['name'] . '</option>';
    }
} */

if($_SESSION['currentLng']=="ru-ru"){
    $sql1 = 'SELECT * FROM `type_new` ORDER BY id ASC';
} else {
    $sql1 = 'SELECT * FROM `type_new_eng` ORDER BY id ASC';
}
if(isset($_REQUEST['empty'])){
    $summary1 = "Ничего не найдено";
}
$stmt1 = $db->prepare($sql1);
if ($stmt1->execute()) {
    $summary = '';
    $rows1 = $stmt1->fetchAll(PDO::FETCH_ASSOC);
    foreach ($rows1 as $val1) {
        /* $summary .= '<option value="' . $val1['code'] . '">' . $val1['name'] . '</option>'; */
        if($_SESSION['currentLng']=="ru-ru"){
            $sql22 = 'SELECT * FROM `class_new` WHERE parent=' . $val1['id'];
        } else {
            $sql22 = 'SELECT * FROM `class_new_eng` WHERE parent=' . $val1['id'];
        }

         // . ' ORDER BY cat_id ASC';
        $stmt22 = $db->prepare($sql22);
        $summary .= '<optgroup label="' . $val1['name'] . '">'; // '<input type="checkbox" value="' . $val1['type_id'] . '">' . $val1['type_name']
        //Выводим контент
        if ($stmt22->execute()) {
            $rows22 = $stmt22->fetchAll(PDO::FETCH_ASSOC);
            foreach ($rows22 as $val22) {
                $summary .= '<option value="' . $val22['id'] . '">' . $val22['name'] . '</option>';
            }
        }
        $summary .= '</optgroup>';
    }
}

$sql = 'SELECT * FROM `' . BEZ_DBPREFIX . 'orders` WHERE DATE(end_date)>=CURDATE() ORDER BY id_order DESC LIMIT 10';
$stmt = $db->prepare($sql);

//Выводим контент
if ($stmt->execute()) {
    $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

    foreach ($rows as $val) {

        $s_select = explode(", ", $val['o_section']);
        /* foreach ($s_select as $val1) {
            $sql1 = 'SELECT * FROM `' . BEZ_DBPREFIX . 'cat` WHERE cat_id=' . $val1; // . ' ORDER BY cat_id ASC'
            $stmt1 = $db->prepare($sql1);
            //Выводим контент
            if ($stmt1->execute()) {
                $rows1 = $stmt1->fetchAll(PDO::FETCH_ASSOC);
                foreach ($rows1 as $val11) {
                    $section1 .= '<span class="label label-default">' . $val11['cat_name'] . '</span> ';
                }
            }
        } */

        foreach ($s_select as $val1){
            $sql1='SELECT * FROM `class_new` WHERE code=' . $val1;
            $stmt1=$db->prepare($sql1);
            if($stmt1->execute()){
                $rows1=$stmt1->fetchAll(PDO::FETCH_ASSOC);
                foreach ($rows1 as $val11){
                    $section1.='<strong>' . $val11['name'] . '</strong>';
                }
            }
        }

        $sql2 = 'SELECT order_num FROM `' . BEZ_DBPREFIX . 'files` WHERE `order_num` = :order_num';
        $stmt = $db->prepare($sql2);
        $stmt->bindValue(':order_num', $val['id_order'], PDO::PARAM_INT);
        $stmt->execute();
        $rows2 = $stmt->fetchAll(PDO::FETCH_ASSOC);

        $result = count($rows2);

        $sql3 = 'SELECT c_name, country, city FROM `' . BEZ_DBPREFIX . 'companys` WHERE `id_company` = :id_company';
        $stmt = $db->prepare($sql3);
        $stmt->bindValue(':id_company', $val['o_company'], PDO::PARAM_INT);
        $stmt->execute();
        $rows3 = $stmt->fetchAll(PDO::FETCH_ASSOC);

        $cName = $rows3[0]['c_name'];
        //$subContent = substr(***, 0, 20) . "...";

        $table .= "<div class='panel panel-default'>";
        $table .= "<div class='panel-heading'>";
        $table .= "<span class='pull-right'>" . $rows3[0]['country'] . ", " . $rows3[0]['city'] . "</span><h3 class='panel-title'><a href='" . BEZ_HOST . "?mode=order_view&vid=" . $val['id_order'] . "'>Заказ №" . $val['id_order'] . " от " . date('d.m.Y', strtotime($val['add_date'])) . "</a></h3>";
        $table .= "</div>";
        $table .= "<div class='panel-body'>" . $val['content'] . "</div>";
        $table .= "<div class='panel-footer'><span class='pull-right'>Файлов: " . $result . "</span>" . $section1 . "</div>";
        $table .= "</div>";
        $section1='';
    }
    //$table .= "</article><!-- /Article --></div></div><!-- /container -->";
    //echo $table;
}
?>