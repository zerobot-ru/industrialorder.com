<?php

if ($_SESSION['currentLng'] == "ru-ru") {
    $pageTitle = 'Информация о заказе #' . $_GET['vid'] . $label_PTitle;
    $pageDesc = 'Информация о заказе #' . $_GET['vid'];
} else {
    $pageTitle = 'Information about order #' . $_GET['vid'] . $label_PTitle;
    $pageDesc = 'Information about order #' . $_GET['vid'];
}

//Проверяем зашел ли пользователь
if ($user === false) {
$pass = htmlspecialchars(stripslashes($_REQUEST['pass']));
$email = htmlspecialchars(stripslashes($_REQUEST['email']));
$vid = htmlspecialchars(stripslashes($_REQUEST['vid']));
//автоматическая авторизация при переходе по ссылке

        /* Создаем запрос на выборку из базы
          данных для проверки подлинности пользователя */
        $sql_check = 'SELECT * FROM `' . BEZ_DBPREFIX . 'reg` WHERE `login` = :email';
        // Подготавливаем PDO выражение для SQL запроса
        $stmt_check = $db->prepare($sql_check);
        $stmt_check->bindValue(':email', $email, PDO::PARAM_STR);
        $stmt_check->execute();

        // Получаем данные SQL запроса
        $rows_check = $stmt_check->fetchAll(PDO::FETCH_ASSOC);

        // Если логин совпадает, проверяем пароль
        if (count($rows_check) > 0) {

            // Получаем данные из таблицы
            if ($pass == $rows_check[0]['pass']) { // изменение
                $_SESSION['user'] = true;
                $_SESSION['allready'] = $rows_check[0]['allready'];
                $_SESSION['login'] = $rows_check[0]['login']; /* $_POST['email'] */
                $_SESSION['r_company'] = $rows_check[0]['company_id'];
                $_SESSION['id_reg'] = $rows_check[0]['id_reg'];
                $_SESSION['user_name'] = $rows_check[0]['name'];
                if ($rows_check[0]['avatar'] == NULL) {
                    $_SESSION['reg_avatar'] = BEZ_HOST . 'uploads/avatars/user.jpg';
                } else {
                    $_SESSION['reg_avatar'] = BEZ_HOST . 'uploads/avatars/' . htmlspecialchars($rows_check[0]['avatar']);
                }
                if ($rows_check[0]['defaultLng'] == NULL) {
                    $_SESSION['currentLng'] = chkLng();
                    if (chkLng() == "ru-ru") {
                        $_SESSION['rusLngSel'] = "selected";
                    } else {
                        $_SESSION['engLngSel'] = "selected";
                    }
                } else {
                    if ($rows_check[0]['defaultLng'] == "ru-ru") {
                        $_SESSION['currentLng'] = "ru-ru";
                        $_SESSION['rusLngSel'] = "selected";
                    } else {
                        $_SESSION['currentLng'] = "en-en";
                        $_SESSION['engLngSel'] = "selected";
                    }
                }

                $sql3_check = 'UPDATE `bez_reg` SET `ssid` = :ssid, `last_login` = :last_login WHERE `id_reg` = :id_reg'; // :r_comapny,
                $stmt_check = $db->prepare($sql3_check);
                $stmt_check->bindValue(':ssid', session_id(), PDO::PARAM_STR);
                $stmt_check->bindValue(':id_reg', $rows[0]['id_reg'], PDO::PARAM_INT);
                $stmt_check->bindValue(':last_login', date("Y-m-d H:i:s"), PDO::PARAM_INT);
                $stmt_check->execute();

                $sql4_check = 'UPDATE `bez_reg` SET `ip` = :ip WHERE `id_reg` = :id_reg'; // :r_comapny,
                $stmt_check = $db->prepare($sql4);
                $stmt_check->bindValue(':ip', $_SERVER['REMOTE_ADDR'], PDO::PARAM_STR);
                $stmt_check->bindValue(':id_reg', $rows[0]['id_reg'], PDO::PARAM_INT);
                $stmt_check->execute();
                header('Location: '.$_SERVER['SCRIPT_NAME'] . '?mode=order_view&vid=' . $vid);
            }
               
            }
            
            
    //Запрос на выборку контента согласно роли
    $sql = 'SELECT * FROM `' . BEZ_DBPREFIX . 'orders` WHERE `id_order`=:id_order';
    $stmt = $db->prepare($sql);
    $stmt->bindValue(':id_order', $_GET['vid'], PDO::PARAM_STR);
    $stmt->execute();

    $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

    $company_id = $rows[0]['o_company'];

    $viewCount = $rows[0]['view_count'];
    $viewCount = $viewCount + 1;
    $sql10 = 'UPDATE `' . BEZ_DBPREFIX . 'orders` SET view_count=:view_count WHERE id_order=:id_order';
    $stmt = $db->prepare($sql10);
    $stmt->bindValue(':id_order', $rows[0]['id_order'], PDO::PARAM_STR);
    $stmt->bindValue(':view_count', $viewCount, PDO::PARAM_STR);
    $stmt->execute();

    foreach ($rows as $val) {
        $sql2 = 'SELECT * FROM `' . BEZ_DBPREFIX . 'companys` WHERE `id_company`=:id_company';
        $stmt = $db->prepare($sql2);
        $stmt->bindValue(':id_company', $val['o_company'], PDO::PARAM_INT);
        $stmt->execute();
        $rows3 = $stmt->fetchAll(PDO::FETCH_ASSOC);

            if ($_SESSION['currentLng'] == "ru-ru") {
                $sql1 = 'SELECT * FROM `class_new` WHERE id=' . $rows[0]['o_section'];
            } else {
                $sql1 = 'SELECT * FROM `class_new_eng` WHERE id=' . $rows[0]['o_section'];
            }
            //$sql1='SELECT * FROM `class_okved` WHERE id=' . $val;
            $stmt1 = $db->prepare($sql1);
            if ($stmt1->execute()) {
                $rows1 = $stmt1->fetchAll(PDO::FETCH_ASSOC);
                foreach ($rows1 as $val1) {
                    $section .= '<strong>' . $val1['name'] . '</strong>';
                }
            }

        //$c_name = htmlspecialchars($rows3[0]['c_name']);
        $id_order = htmlspecialchars($rows[0]['id_order']);
        $content = nl2br($rows[0]['content']);
        $o_type = htmlspecialchars($rows[0]['o_type']);
        $o_section = $section;
        $add_date = htmlspecialchars(date('d.m.Y', strtotime($rows[0]['add_date'])));
        $end_date = htmlspecialchars(date('d.m.Y', strtotime($rows[0]['end_date'])));
        $edit_date = htmlspecialchars(date('d.m.Y', strtotime($rows[0]['edit_date'])));
        $country = htmlspecialchars($rows3[0]['country']);
        $city = htmlspecialchars($rows3[0]['city']);
        //$address = htmlspecialchars($rows[0]['address']);
    }
}
if ($user === true && isset($_REQUEST['pass'])) {
    $vid = htmlspecialchars(stripslashes($_REQUEST['vid']));
    header('Location: '.$_SERVER['SCRIPT_NAME'] . '?mode=order_view&vid=' . $vid);
}
if ($user === true) {
    //Запрос на выборку контента согласно роли
    $sql = 'SELECT * FROM `' . BEZ_DBPREFIX . 'orders` WHERE `id_order`=:id_order';
    $stmt = $db->prepare($sql);
    $stmt->bindValue(':id_order', $_GET['vid'], PDO::PARAM_STR);
    $stmt->execute();

    $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
    $company_id = $rows[0]['o_company'];

    $viewCount = $rows[0]['view_count'];
    $viewCount = $viewCount + 1;
    $sql10 = 'UPDATE `' . BEZ_DBPREFIX . 'orders` SET view_count=:view_count WHERE id_order=:id_order';
    $stmt = $db->prepare($sql10);
    $stmt->bindValue(':id_order', $rows[0]['id_order'], PDO::PARAM_STR);
    $stmt->bindValue(':view_count', $viewCount, PDO::PARAM_STR);
    $stmt->execute();
   
    foreach ($rows as $val) {
        $sql3 = 'SELECT * FROM `' . BEZ_DBPREFIX . 'files` WHERE `order_num`=:order_num';
        $stmt = $db->prepare($sql3);
        $stmt->bindValue(':order_num', $val['id_order'], PDO::PARAM_INT);
        $stmt->execute();
        $rows2 = $stmt->fetchAll(PDO::FETCH_ASSOC);

        $jpg = "";
        $jpeg = "";
        $gif = "";
        $png = "";
        $pdf = "";
        $tif = "";

        foreach ($rows2 as $val2) {
            $folder = 'uploads/orders/';
            if (strrchr($val2['file'], '.') == ".jpg") {
                $jpg .= '<a href ="' . $folder . $val2['file'] . '"class="fancybox" rel="gallery">' . '<img class="o-img img-rounded" src="' . $folder . $val2['file'] . '">' . '</a>';
            } elseif (strrchr($val2['file'], '.') == ".jpeg") {
                $jpeg .= '<a href ="' . $folder . $val2['file'] . '"class="fancybox" rel="gallery">' . '<img class="o-img img-rounded" src="' . $folder . $val2['file'] . '">' . '</a>';
            } elseif (strrchr($val2['file'], '.') == ".gif") {
                $gif .= '<a href ="' . $folder . $val2['file'] . '"class="fancybox" rel="gallery">' . '<img class="o-img img-rounded" src="' . $folder . $val2['file'] . '">' . '</a>';
            } elseif (strrchr($val2['file'], '.') == ".png") {
                $png .= '<a href ="' . $folder . $val2['file'] . '"class="fancybox" rel="gallery">' . '<img class="o-img img-rounded" src="' . $folder . $val2['file'] . '">' . '</a>';
            } elseif (strrchr($val2['file'], '.') == ".pdf") {
                $pdf .= '<a href ="' . $folder . $val2['file'] . '" target="_blank">' . $val2['file'] . '</a><br>';
            } elseif (strrchr($val2['file'], '.') == ".tif") {
                $tif .= '<a href ="' . $folder . $val2['file'] . '"class="fancybox" rel="gallery">' . '<img class="o-img img-rounded" src="' . $folder . $val2['file'] . '">' . '</a>';
            }
        }

        $sql2 = 'SELECT * FROM `' . BEZ_DBPREFIX . 'companys` WHERE `id_company`=:id_company';
        $stmt = $db->prepare($sql2);
        $stmt->bindValue(':id_company', $val['o_company'], PDO::PARAM_INT);
        $stmt->execute();
        $rows3 = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $c_name = $rows3[0]['c_name'];
        $s_select = explode(", ", $rows[0]['o_section']);
        /* foreach ($s_select as $val) {
            $sql1 = 'SELECT * FROM `' . BEZ_DBPREFIX . 'cat` WHERE cat_id=' . $val; // . ' ORDER BY cat_id ASC'
            $stmt1 = $db->prepare($sql1);
            //Выводим контент
            if ($stmt1->execute()) {
                //$summary = '';
                $rows1 = $stmt1->fetchAll(PDO::FETCH_ASSOC);
                foreach ($rows1 as $val1) {
                    $section .= '<span class="label label-default">' . $val1['cat_name'] . '</span> ';
                }
            }
        } */

        foreach ($s_select as $val) {
            if ($_SESSION['currentLng'] == "ru-ru") {
                $sql1 = 'SELECT * FROM `class_new` WHERE id=' . $val;
            } else {
                $sql1 = 'SELECT * FROM `class_new_eng` WHERE id=' . $val;
            }
            //$sql1='SELECT * FROM `class_okved` WHERE id=' . $val;
            $stmt1 = $db->prepare($sql1);
            if ($stmt1->execute()) {
                $rows1 = $stmt1->fetchAll(PDO::FETCH_ASSOC);
                
                foreach ($rows1 as $val1) {
                    $section .= '<strong>' . $val1['name'] . '</strong>';
                }
            }
        }

        $id_order = htmlspecialchars($rows[0]['id_order']);
        $c_name = htmlspecialchars($rows3[0]['c_name']);
        $o_title = htmlspecialchars($rows[0]['o_title']);
        $content = nl2br($rows[0]['content']);
        //$o_type = htmlspecialchars($rows[0]['o_type']);
        $o_section = $section;
        if(count($rows1) == 0) {
                    $o_section = "Не указана";
                }
        $comp = "<a href = '?mode=company_view&vid=" . $rows3[0]['id_company'] . "'>" . $rows3[0]['c_name'] . "</a>";
        $add_date = htmlspecialchars(date('d.m.Y', strtotime($rows[0]['add_date'])));
        $end_date = htmlspecialchars(date('d.m.Y', strtotime($rows[0]['end_date'])));
        $email = htmlspecialchars($rows3[0]['email']);
        $site = htmlspecialchars($rows3[0]['site']);
        $tel = htmlspecialchars($rows3[0]['c_tel']);
        $edit_date = htmlspecialchars(date('d.m.Y', strtotime($rows[0]['edit_date'])));
        $country = htmlspecialchars($rows3[0]['country']);
        $city = htmlspecialchars($rows3[0]['city']);
        $address = htmlspecialchars($rows3[0]['address']);
        $o_user = $rows[0]['o_user'];
    }

    // TODO: Добавить id пользователя

    if (isset($_POST['o_answer'])) {
        $pm_id = uniqid($_SESSION['id_reg'] . '_' . $_SERVER["REQUEST_TIME_FLOAT"] . '_', true);
        
$sql_check = 'SELECT COUNT(*) FROM `' . BEZ_DBPREFIX . 'pm` WHERE `pm_title`=:pm_title GROUP BY pm_title';
$stmt_check = $db->prepare($sql_check);
$stmt_check->bindValue(':pm_title', $_POST['pm_title'], PDO::PARAM_STR);
$stmt_check->execute();
$rows_check = $stmt_check->fetch(PDO::FETCH_ASSOC);
$pm_status1 = 1;
 if($rows_check['COUNT(*)'] > 0){
    $pm_status1 = 0; 
 }       
        
        $sql_a = 'SELECT c_name, admin FROM `' . BEZ_DBPREFIX . 'companys` WHERE `id_company`=:id_company';
        $stmt = $db->prepare($sql_a);
        $stmt->bindValue(':id_company', $company_id, PDO::PARAM_STR);
        $stmt->execute();
        $rows_a = $stmt->fetchAll(PDO::FETCH_ASSOC);

        $sql_l = 'SELECT login FROM `' . BEZ_DBPREFIX . 'reg` WHERE `id_reg`=:id_reg';
        $stmt = $db->prepare($sql_l);
        $stmt->bindValue(':id_reg', $o_user, PDO::PARAM_STR);
        $stmt->execute();
        $rows_l = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $emailTo = $rows_l[0]['login'];

        $sql_w = 'INSERT INTO `bez_pm` SET u_from = :u_from, u_to = :u_to, pm_title = :pm_title, pm_message = :pm_message, pm_flag = :pm_flag, pm_status = :pm_status, pm_hash = :pm_hash';
        $stmt_w = $db->prepare($sql_w);
        $stmt_w->bindValue(':u_from', $_SESSION['id_reg']);
        $stmt_w->bindValue(':u_to', $o_user); //$rows_a[0]['admin']
        $stmt_w->bindValue(':pm_title', $_POST['pm_title']);
        $stmt_w->bindValue(':pm_message', $_POST['o_quest']); //, PDO::PARAM_STR
        $stmt_w->bindValue(':pm_flag', '0');
        $stmt_w->bindValue(':pm_status', $pm_status1);
        $stmt_w->bindValue(':pm_hash', $id_order);
        $stmt_w->execute();

        $getID = $db->lastInsertId();
//\r\n
        // $_SESSION['r_company']
        // Отправляем письмо
        $title = $_POST['pm_title'] . " на сайте industrialorder.com";
        $message = "От кого: " . $_SESSION['user_name'] . " | " . $rows_a[0]['c_name'] . "\r\n<br><br>";
        $message .= "Дата: " . date("d.m.Y H:i") . PHP_EOL . "<br><br>";
        $message .= nl2br($_POST['o_quest']) . PHP_EOL . "<br>";
        $message .= "Ссылка на заказ: " . BEZ_HOST . "?mode=order_view&vid=" . $id_order;
        $message .= PHP_EOL . PHP_EOL . "<br><br>С уважением,". PHP_EOL . "<br>портал industrialorder.com". PHP_EOL . PHP_EOL  . "<br><br>" . BEZ_HOST;
        sendMessageMail($emailTo, BEZ_MAIL_AUTOR, $title, $message);
        
        


        // Начало обработчика загрузки файлов

        $folder = ROOT_DIR . "/uploads/answers/"; //BEZ_HOST . 'uploads/avatars/'
        $ext = array(".gif", ".jpg", ".jpeg", ".png", ".tif", ".pdf", ".xls", ".xlsx", ".doc", ".docx", "");

        // Получаем расширение файла
        $file_extends = strtolower(strrchr($_FILES['userfile']['name'], '.'));
        // Генерируем случайное число
        $file_name = uniqid(rand(10000, 99999));
        // Формируем путь на сервере
        $uploadedFile = $folder . $file_name . $file_extends;
        $fileName = $file_name . $file_extends;

        if (!in_array($file_extends, $ext)) {
            echo "<div class=\"alert alert-danger\" role=\"alert\">Такое нельзя загружать.</div>";
        } else {
           
            if (is_uploaded_file($_FILES['userfile']['tmp_name'])) {

                if (move_uploaded_file($_FILES['userfile']['tmp_name'], $uploadedFile)) {
                    $sql3 = 'INSERT `bez_answer` SET file =:file, order_num = :order_num, from_id = :from_id, pmid = :pmid';
                    $stm = $db->prepare($sql3);
                    $stm->bindValue(':file', $fileName, PDO::PARAM_STR);
                    $stm->bindValue(':order_num', $id_order, PDO::PARAM_STR);
                    $stm->bindValue(':from_id', $_SESSION['id_reg'], PDO::PARAM_STR);
                    $stm->bindValue(':pmid', $getID, PDO::PARAM_STR);
                    $stm->execute();
                    echo "<div class=\"alert alert-success\" role=\"alert\">Файл загружен.</div>";
                } else {
                    echo "<div class=\"alert alert-danger\" role=\"alert\">Во время загрузки файла произошла ошибка.</div>";
                    echo($_FILES["userfile"]["error"]);
                }
            } else {
                echo "<div class=\"alert alert-danger\" role=\"alert\">Файл не загружен.</div>";
            }
        }
        
    }
}


if(isset($_REQUEST['submit_reg'])){
     if (empty($_POST['email']))
        $err[] = '<div class="alert alert-danger" role="alert">Поле Email не может быть пустым!</div>';
    else {
        if (emailValid($_POST['email']) === false)
            $err[] = '<div class="alert alert-danger" role="alert"><span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span><span class="sr-only">Error:</span>Не правильно введен E-mail</div>' . "\n";
    }

    /* if (empty($_POST['pass']))
        $err[] = '<div class="alert alert-danger" role="alert">Поле Пароль не может быть пустым</div>';

    if (empty($_POST['pass2']))
        $err[] = '<div class="alert alert-danger" role="alert">Поле Подтверждения пароля не может быть пустым</div>'; */

    // Проверяем наличие ошибок и выводим пользователю
    if (count($err) > 0)
        echo showErrorMessage($err);
    else {
        /* Продолжаем проверять введенные данные
          Проверяем на совпадение пароли */
        /* if ($_POST['pass'] != $_POST['pass2'])
            $err[] = '<div class="alert alert-danger" role="alert">Пароли не совпадают</div>'; */

        // Проверяем наличие ошибок и выводим пользователю
        if (count($err) > 0)
            echo showErrorMessage($err);
        else {
            /* Проверяем существует ли у нас
              такой пользователь в БД */

            $sql = 'SELECT `login`
					FROM `' . BEZ_DBPREFIX . 'reg`
					WHERE `login` = :login';
            // Подготавливаем PDO выражение для SQL запроса
            $stmt = $db->prepare($sql);
            $stmt->bindValue(':login', $_POST['email'], PDO::PARAM_STR);

            $stmt->execute();

            $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

            if (count($rows) > 0)
                $err[] = '<div class="alert alert-danger" role="alert">К сожалению Логин: <b>' . $_POST['email'] . '</b> занят!</div>';

            // Проверяем наличие ошибок и выводим пользователю
            if (count($err) > 0)
                echo showErrorMessage($err);
            else {

                // Получаем ХЕШ соли
                //$salt = salt();

                // Солим пароль
                $pass1 = generate_password(6);
                $pass2 = md5($pass1);

                if(isset($_SESSION['currentLng'])){
                    $lang=$_SESSION['currentLng'];
                }else{
                    $lang=chkLng();
                }

                /* Если все хорошо, пишем данные в базу */
                $sql = 'INSERT INTO `bez_reg` SET `login` = :email,`pass` = :pass,`status` = 1,`defaultLng` = :defaultLng, ref = :ref'; // внесены изменения
                // Подготавливаем PDO выражение для SQL запроса
                $stmt = $db->prepare($sql);
                $stmt->bindValue(':email', $_POST['email'], PDO::PARAM_STR);
                $stmt->bindValue(':pass', $pass2, PDO::PARAM_STR);
                $stmt->bindValue(':ref', 1, PDO::PARAM_STR);
                $stmt->bindValue(':defaultLng', $lang, PDO::PARAM_STR);

                $stmt->execute();

                $getID = $db->lastInsertId();

                // Отправляем письмо для активации
                $url = BEZ_HOST . '?mode=reg&key=' . $_POST['email'];
                $title = 'Регистрация на https://industrialorder.com/';
                
                $message .= 'Спасибо за регистрацию.<br>' . PHP_EOL; // Для активации Вашего акаунта пройдите по ссылке <a href="' . $url . '">' . $url . '</a>
                $message .= 'Ваш логин: ' . $_POST['email'] . PHP_EOL . "<br>";
                $message .= 'Ваш пароль: ' . $pass1 .  PHP_EOL . "<br><br>";
                
                $message .= 'Thank you for registration.<br>' . PHP_EOL; // Для активации Вашего акаунта пройдите по ссылке <a href="' . $url . '">' . $url . '</a>
                $message .= 'Your login: ' . $_POST['email'] . PHP_EOL . "<br>";
                $message .= 'Your password: ' . $pass1 .  PHP_EOL . "<br>";




             

                sendMessageMail($_POST['email'], BEZ_MAIL_AUTOR, $title, $message);

                $_SESSION['user'] = true;
                $_SESSION['allready'] = NULL;
                $_SESSION['login'] = $_POST['email']; /* $_POST['email'] */
                $_SESSION['id_reg'] = $getID;

                $sql3 = 'UPDATE `bez_reg` SET `ssid` = :ssid WHERE `id_reg` = :id_reg'; // :r_comapny,
                $stmt = $db->prepare($sql3);
                $stmt->bindValue(':ssid', session_id(), PDO::PARAM_STR);
                $stmt->bindValue(':id_reg', $getID, PDO::PARAM_INT);
                $stmt->execute();

                $sql4 = 'UPDATE `bez_reg` SET `ip` = :ip WHERE `id_reg` = :id_reg'; // :r_comapny,
                $stmt = $db->prepare($sql4);
                $stmt->bindValue(':ip', $_SERVER['REMOTE_ADDR'], PDO::PARAM_STR);
                $stmt->bindValue(':id_reg', $getID, PDO::PARAM_INT);
                $stmt->execute();

                // Сбрасываем параметры
                header('Location:' . $_SERVER["SCRIPT_NAME"] . "?" . $_SERVER["QUERY_STRING"]);
                exit;
            }
        }
    }
}

?>