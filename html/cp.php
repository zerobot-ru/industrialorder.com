<?php

//Ключ защиты
if (!defined('BEZ_KEY')) {
    header("HTTP/1.1 404 Not Found");
    exit(file_get_contents('../404.html'));
}

echo '<ol class="list-unstyled">';
echo '<li class="smenu"><span class="glyphicon gmenu glyphicon-user"></span> /li>';
echo '<li class="smenu"><span class="glyphicon gmenu glyphicon-envelope"></span> </li>';

echo '<li class="smenu"><span class="glyphicon gmenu glyphicon-plus"></span> </li>';
echo '<li class="smenu"><span class="glyphicon gmenu glyphicon-list-alt"></span> </li>';

echo '<li class="smenu"><span class="glyphicon gmenu glyphicon-off"></span> </li>';
echo '</ol>';
echo "<hr>";
?>