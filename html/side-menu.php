<?php
/**
 * Created by PhpStorm.
 * User: Алексей
 * Date: 21.02.2017
 * Time: 11:19
 */


if ($user === FALSE) {
    echo '<!-- Pushy Menu -->
    <nav class="pushy pushy-left">
        <div class="pushy-content">
        <img src="../images/logo.png" alt="industrialorder.com" class="pushy-logo">
        <ul><li class="pushy-link">Вы не авторизованы.</li><li class="pushy-link"><a href="' . BEZ_HOST . '?mode=auth">Войдите</a> или <a href="' . BEZ_HOST . '?mode=reg">зарегистрируйтесь</a></li></ul>
        </div>
    </nav>
    <!-- Site Overlay -->
    <div class="site-overlay"></div>

    <!-- Your Content -->
    <div id="container">
        <!-- Menu Button -->
        <button class="menu-btn">&#9776;<br>М<br>Е<br>Н<br>Ю</button>
    </div>';
} else {
    ?>

    <!-- Pushy Menu -->
    <nav class="pushy pushy-left">
        <div class="pushy-content">
            <img src="<?php echo $_SESSION['reg_avatar']; ?>" class="img-rounded p-logo">
            <ul>
                <!-- li class="pushy-link"><a href="<?php BEZ_HOST; ?>?mode="><i class="fa fa-bar-chart" aria-hidden="true"></i> <s>Рейтинг</s></a>
            </li>
            <li class="pushy-link"><a href="<?php BEZ_HOST; ?>?mode="><i class="fa fa-eye" aria-hidden="true"></i> <s>Отзывы</s></a></li -->
                <li class="pushy-link"><a href="<?php BEZ_HOST; ?>?mode=profile"><i class="fa fa-user"
                                                                                    aria-hidden="true"></i> Мой профиль</a> <a href="<?php BEZ_HOST; ?>?mode=profile_edit">(ред.)</a>
                </li>
                <li class="pushy-link"><a href="<?php BEZ_HOST; ?>?mode=my_pm"><i class="fa fa-envelope-o"
                                                                                  aria-hidden="true"></i> Мои сообщения</a>
                    <span class="badge"><?php echo checkPM($_SESSION['id_reg']) ?></span></li>
                <!-- li class="pushy-link"><a href="<?php BEZ_HOST; ?>?mode="><i class="fa fa-newspaper-o" aria-hidden="true"></i>
                    <s>Новости</s></a></li -->
                <li class="pushy-link"><a href="<?php BEZ_HOST; ?>?mode=orders"><i class="fa fa-money" aria-hidden="true"></i>
                        Заказы</a></li>
                <ul>
                    <li class="pushy-link"><a href="<?php BEZ_HOST; ?>?mode=order_add">Добавить заказ</a></li>
                    <li class="pushy-link"><a href="<?php BEZ_HOST; ?>?mode=orders">Мои заказы</a></li>
                </ul>
                <!-- li class="pushy-link"><a href="<?php BEZ_HOST; ?>?mode="><i class="fa fa-users" aria-hidden="true"></i> <s>Партнеры</s></a>
            </li>
            <li class="pushy-link"><a href="<?php BEZ_HOST; ?>?mode="><i class="fa fa-user-plus" aria-hidden="true"></i> <s>CRM база</s></a>
            </li>
            <li class="pushy-link"><a href="<?php BEZ_HOST; ?>?mode="><i class="fa fa-sticky-note-o" aria-hidden="true"></i> <s>Заметки</s></a>
            </li>
            <li class="pushy-link"><a href="<?php BEZ_HOST; ?>?mode="><i class="fa fa-book" aria-hidden="true"></i> <s>Техническая
                        библиотека</s></a></li>
                <li class="pushy-link"><a href="<?php BEZ_HOST; ?>?mode=add_question"><i class="fa fa-question"
                                                                                         aria-hidden="true"></i>
                        Форум</a></li -->
                <li class="pushy-link"><a href="<?php BEZ_HOST; ?>?mode=company_add"><i class="fa fa-plus-square-o"
                                                                                        aria-hidden="true"></i>
                        Добавить компанию</a></li>
                <li class="pushy-link"><a href="<?php BEZ_HOST; ?>?mode=auth&exit=true"><i class="fa fa-sign-out"
                                                                                           aria-hidden="true"></i> Выход</a>
                </li>
            </ul>
        </div>
    </nav>

    <!-- Site Overlay -->
    <div class="site-overlay"></div>

    <!-- Your Content -->
    <div id="container">
        <!-- Menu Button -->
        <button class="menu-btn">&#9776;<br>М<br>Е<br>Н<br>Ю</button>
    </div>
    <?php
}
?>