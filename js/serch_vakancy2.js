/**
 * Created by Aleksej Kazin on 21.03.2017.
 */

$(function () {

    //Живой поиск
    $('#search2').bind("change keyup input click", function two() {
        var regexp = /^ /;
        if (this.value.length >= 2 && this.value.search(regexp) === -1) {
            $.ajax({
                type: 'post',
                url: "../scripts/orders/all_vakancy.php", //Путь к обработчику

                data: {'search': this.value},
                response: 'text',
                success: function (data) {
                    $("#search_result2").html(data).fadeIn(); //Выводим полученые данные в списке
                }
            })
        }
    })

    $("#search_result2").hover(function () {
        $("#search2").blur(); //Убираем фокус с input
    })
/*
    //При выборе результата поиска, прячем список и заносим выбранный результат в input
    $("#search_result2").on("click", "li", function () {
        s_user = $(this).text();
        $("#search2").val(s_user).attr('disabled', 'disabled'); //деактивируем input, если нужно
        $("#search_result2").fadeOut();

    })
*/
})


