<?php
/**
 * Created by PhpStorm.
 * User: Aleksej Kazin
 * Date: 10.12.2016
 * Time: 12:22
 */

function startSession()
{
    if (session_id()) return true;
    else return session_start();
}

function destroySession()
{
    if (session_id()) {
        setcookie(session_name(), session_id(), time() - 60 * 60 * 24);
        session_unset();
        session_destroy();
    }
}