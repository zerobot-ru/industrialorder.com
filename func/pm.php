<?php
/**
 * Created by PhpStorm.
 * User: Алексей
 * Date: 31.10.2016
 * Time: 14:14
 */

function checkPM($user_id)
{

    include 'bd/bd.php';

    //Запрос на выборку контента согласно id
    $sql = 'SELECT * FROM `' . BEZ_DBPREFIX . 'pm` WHERE `u_to` = :u_to AND `pm_flag` = :pm_flag AND `pm_status` = :pm_status';
    $stmt = $db->prepare($sql);
    $stmt->bindValue(':u_to', $_SESSION['id_reg'], PDO::PARAM_STR);
    $stmt->bindValue(':pm_flag', '0', PDO::PARAM_STR);
    $stmt->bindValue(':pm_status', '0', PDO::PARAM_STR);
    $stmt->execute();

    $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

    $pm_count = count($rows);

    //Отправляем данные
    if ($pm_count == 0)
        return '0';
    else
        return $pm_count;
}