<?php
/**
 * Created by PhpStorm.
 * User: Алексей
 * Date: 16.12.2016
 * Time: 16:09
 */

if ($_SESSION['currentLng'] == 'ru-ru') {
    /* Меню */
    $menu_Index = "Главная";
    $menu_Auth = "Войти";
    $menu_Reg = "Регистрация";
    $menu_Orders = "Заказы";
    $menu_Companys = "Компании";
    $menu_Contacts = "Контакты";
    $menu_Profile = "Профиль";
    $menu_Messages = "Сообщения";
    $menu_Exit = "Выход";
    $menu_OOKVED = "Поиск заказа по виду деятельности";
    $menu_OTitle = "Поиск заказа по названию";
    $menu_AddOrder = "Добавить новый заказ";
    $menu_MyOrders = "Мои заказы";
    $menu_COKVED = "Поиск компании по виду деятельности";
    $menu_CTitle = "Поиск компании по названию";
    $menu_Forum = "Форум";
    $menu_Help = "Помощь";
    $menu_Find_emploier = "Найти нового сотрудника";
    
    /* Кнопки */
    $btn_SignIn = "Войти";
    $btn_SignUp = "Зарегистрироваться";
    $btn_SendPassword = "Восстановить пароль";
    $btn_Save = "Сохранить";
    $btn_View = "Просмотреть";
    $btn_Edit = "Редактировать";
    $btn_Delete = "Удалить";
    $btn_CQuest = "Связаться с представителем компании";
    $btn_OQuest = "Откликнуться на заказ / Отправить файл";
    $btn_Close = "Закрыть";
    $btn_Send = "Отправить";
    $btn_Search = "Искать";
    $btn_Friend = "Добавить в друзья";
    $btn_notes = "Добавить заметку";
    $btn_Add = "Добавить";
    

    /* Метки */
    $label_Name = "Имя";
    $label_Department = "Отдел";
    $label_Position = "Должность";
    $label_Tel = "Телефон";
    $label_Index = "Главная";
    $label_UAccount = "Личный кабинет пользователя";
    $label_PEdit = "Редактировать профиль";
    $label_EditCompany = "Редактировать компанию";
    $label_YourCompany = "Ваша компания";
    $label_AddCompany = "Добавить компанию";
    $label_AllreadyCompany = "Вы уже добавили компанию";
    $label_CManage = "Управление компанией";
    $label_SDescription = "Короткое описание";
    $label_FDescription = "Полное описание";
    $label_Activity = "Вид деятельности";
    $label_Title = "Название";
    $label_Services = "Услуги";
    $label_INN = "ИНН";
    $label_CReg = "Регистрация компании";
    $label_CEdit = "Редактирование компании";
    $label_BInfo = "Основная информация";
    $label_CDetails = "Контактные данные";
    $label_Country = "Страна";
    $label_City = "Город";
    $label_Address = "Адрес";
    $label_WebSite = "Web-сайт";
    $label_CPerson = "Контактное лицо";
    $label_Payment = "Платежные реквизиты";
    $label_LName = "Юридическое название";
    $label_KPP = "КПП";
    $label_FIOGD = "ФИО руководителя";
    $label_Dolg = "Должность руководителя";
    $label_Bank = "Банк";
    $label_BIK = "БИК";
    $label_RS = "Расчетный счет";
    $label_KS = "Корреспондентский счет";
    $label_Other = "Прочее";
    $label_CLogo = "Логотип компании";
    $label_Alert = "Поля со значком <font color='red'>*</font> обязательны для заполнения!";
    $label_EProfile = "Редактирование анкеты";
    $label_Photo = "Фотография";
    $label_AboutUs = "О нас";
    $label_COrders = "Заказы компании";
    $label_CAbout = "О компании";
    $label_Emp = "Сотрудники предприятия";
    $label_Reviews = "Отзывы";
    $label_Order = "Заказ";
    $label_EndDate = "Дата окончания";
    $label_CProfile = "Карточка компании";
    $label_Company = "Компания";
    $label_OView = "Просмотр заказа";
    $label_Category = "Категория";
    $label_Description = "Описание";
    $label_NeedAuth = "Контакты доступны после регистрации!";
    $label_Attention = "Внимание! Для регистрации Вам нужно указать действующий e-mail, т.к. на него будет выслано письмо с паролем!";
    $label_Contacts = "Обратная связь";
    $label_ContactsH1 = "Связь с администрацией сайта";
    $label_ContactUs = "Свяжитесь с нами!";
    $label_Message = "Сообщение";
    $label_CentralOffice = "Центральный офис находится по адресу: Россия, Санкт-Петербург";
    $label_World = "Мы работаем по всему миру.";
    $label_Skype = "";
    $label_AboutUs1 = "Социальная сеть для производственных предприятий.";
    $label_AboutUs2 = "Воспользуйтесь нашим интернет-порталом партнерства поставщиков и заказчиков.";
    $label_AboutUs3 = "Этот сайт ориентирован на установление прочных деловых связей.";
    $label_Social = "Мы в социальных сетях";
    $label_MainAddress = "Россия, Санкт-Петербург";
    $label_Footer = 'ООО "Производственный заказ"';
    $label_PTitle = " | Социальная сеть для производственных предприятий";
    $label_ASignIn = "Войдите в свой аккаунт";
    $label_AQuest = "Нет аккаунта на нашем сайте?";
    $label_Password = "Пароль";
    $label_Reg = "Регистрация на industrialorder.com";
    $label_Guest = "Доступ закрыт, зарегистрируйтесь или авторизуйтесь!";
    $label_OSearch = "Поиск заказов";
    $label_OSearchT = "Поиск заказов по названию";
    $label_OSearchI = "Введите название заказа";
    $label_CSearch = "Поиск компаний по виду деятельности";
    $label_CSearchT = "Поиск компаний по названию";
    $label_CSearchI = "Введите название компании";
    $label_Section = "Выберите раздел";
    $label_OQuest = "Вопрос по заказу";
    $label_CQuest = "Вопрос по компании";
    $label_YourQuest = "Ваш вопрос";
    $label_FExt = "Допустимые расширения файлов";
    $label_File = "Файл";
    $label_Files = "Файлы";
    $label_FLimit = "Не более 20 файлов.";
    $label_AllOrders = "Все заказы";
    $label_AllCompanys = "Все компании";
    $label_OAdd = "Добавить заказ";
    $label_EndDateDesc = "Дата до которой заказ актуален. Затем заказ удаляется из базы.";
    $label_AddOrder = "Название заказа";
    $label_AddOrderDesc = "Введите название заказа, используется при поиске.";
    $label_OrderDesc = "Описание заказа";
    $label_OrderDescHelp = "Введите подробное описание заказа.";
    $label_Help = "Помощь";
    /* 21.04.17 */
    $label_AddFiles = "Добавить фотографии";
    $label_AddFilesAlert = "Сначала надо добавить компанию!";
    $label_CompanyFiles = "Дополнительная информация о компании";
    $label_CVPhoto = "Фотографии компании";
    $label_AddLibFile = "Добавить в библиотеку";
    $label_filename = "Имя файла";
    $label_techlib_view = "Просмотр библиотеки";
    $label_News = "Новости";
    $label_Valuation = "Оценка";
    $label_Note = "Добавить заметку";
    $label_Delete = "Удалить";
    $label_ChangeLogo = "Заменить логотип";
    $label_SendPassword = "Восстановить пароль на industrialorder.com";
    $label_ForgetPassword = "Забыли пароль";
    $label_ChangeEmail = "Изменить E-mail"; 
    $label_Helper = "Добро пожаловать!<br>Оставьте контактные данные и мы зарегистрируем Вашу компанию самостоятельно";
    $label_Website = "Адрес вашего сайта";
    $label_Reg_email = "Введите E-mail, указанный при регистрации и мы вышлем на него пароль";
    $label_Change_password = "Изменить пароль";
    $label_New_password = "Новый пароль";
    $label_New_password_two = "Новый пароль еще раз";
    $label_Back = "Назад";
    $label_Action1 = "Начните вводить вид деятельности или выберите его из списка";
    $label_Company_name = "Название компании";
    $label_Load = "Загрузить";
    $label_Friends = "Друзья";
    $label_Notes = "Заметки";
    $label_AdminPanel = "Панель администратора компании";
    $label_RegPeople = "Регистрация сотрудника";
    $label_FullName = "ФИО";
    $label_Employers = "Список сотрудников";
    $label_CompanyAdmin = "Администрирование компании";
    $label_NewEmail = "Введите новый E-mail адрес";
    $label_Theme = "Тема";
    $label_Date = "Дата";
    $label_Action = "Действие";
    $label_Read = "Прочитать";
    $label_Answers = "Ответы компаний";
    $label_MessageRead = "Чтение сообщения";
    $label_From = "от";
    $label_Answer = "Ответить";
    $label_Private = "Личные сообщения";
    $label_DateAdding = "Дата добавления";
    $label_WriteYouAnswer = "Введите ваш ответ";
    $label_DateChange = "Дата последнего изменения";
    $label_DateEnd = "Дата окончания заказа";
    $label_FromCompany = "От компании";
    $label_OrderNumber = "Номер заказа";
    $label_ViewOrders = "Просмотр заказов";
    $label_CurrentOrder = "Текущие заказы";
    $label_ArchiveOrders = "Архив заказов";
    $label_WriteReview = "Напишите отзыв о компании";
    $label_Valuation = "Рейтинг";
    $label_AskQuestion = "Задать вопрос";
    $label_AnswersForum = "Ответы";
    $label_OrderNumber = "Заказ №";
    $label_FilesSearch = "Файлов";
    $label_SearchResult = "Результат поиска";
    $label_Inquiry = "Запрос";
    $label_job = "Полезные ссылки";
    
} else {
    /* Меню */
    $menu_Index = "Main";
    $menu_Auth = "Sign in";
    $menu_Reg = "Sign up";
    $menu_Orders = "Orders";
    $menu_Companys = "Companys";
    $menu_Contacts = "Contacts";
    $menu_Profile = "My profile";
    $menu_Messages = "Messages";
    $menu_Exit = "Sign out";
    $menu_OOKVED = "Search by type of activity";
    $menu_OTitle = "Search by order title";
    $menu_AddOrder = "Add new order";
    $menu_MyOrders = "My orders";
    $menu_COKVED = "Search by type of activity";
    $menu_CTitle = "Search by company name";
    $menu_Forum = "Forum";
    $menu_Help = "Help";
    $menu_Find_emploier = 'Find a new employee';

    /* Кнопки */
    $btn_SignIn = "Sign in";
    $btn_SignUp = "Sign up";
    $btn_SendPassword = "reestablish password";
    $btn_Save = "Save";
    $btn_View = "View";
    $btn_Edit = "Edit";
    $btn_Delete = "Delete";
    $btn_CQuest = "Contact the company";
    $btn_OQuest = "Reply to an order / Send a file";
    $btn_Close = "Close";
    $btn_Send = "Send";
    $btn_Search = "Search";
    $btn_Friend = "add friends";
    $btn_notes = "add notes";
    $btn_Add = "Add";
    
    /* Метки */
    $label_Name = "Name";
    $label_Department = "Department";
    $label_Position = "Position";
    $label_Tel = "Phone";
    $label_Index = "Main";
    $label_UAccount = "User account";
    $label_PEdit = "Edit profile";
    $label_EditCompany = "Edit company";
    $label_YourCompany = "Your company";
    $label_AddCompany = "Add company";
    $label_AllreadyCompany = "You already added a company";
    $label_CManage = "Company management";
    $label_SDescription = "Short description";
    $label_FDescription = "Full description";
    $label_Activity = "Type of activity";
    $label_Title = "Title";
    $label_Services = "Services";
    $label_INN = "Taxpayer identification number";
    $label_CReg = "Company registration";
    $label_CEdit = "Company edit";
    $label_BInfo = "Basic information";
    $label_CDetails = "Contact details";
    $label_Country = "Country";
    $label_City = "City";
    $label_Address = "Address";
    $label_WebSite = "WWW";
    $label_CPerson = "The contact person";
    $label_Payment = "Payment requisites";
    $label_LName = "Legal name";
    $label_KPP = "Code of reason";
    $label_FIOGD = "Full name of the General Director";
    $label_Dolg = "Position of manager";
    $label_Bank = "Bank";
    $label_BIK = "IBAN, SWIFT or CAB";
    $label_RS = "Checking account";
    $label_KS = "Correspondent account";
    $label_Other = "Other";
    $label_CLogo = "Company`s logo";
    $label_Alert = "Fields marked with an asterisk <font color='red'>*</font> are required!";
    $label_EProfile = "Editing a profile";
    $label_Photo = "Photo";
    $label_AboutUs = "About us";
    $label_COrders = "Company orders";
    $label_CAbout = "About Company";
    $label_Emp = "Employees of the company";
    $label_Reviews = "Reviews";
    $label_Order = "Order";
    $label_EndDate = "Expiration date";
    $label_CProfile = "Company profile";
    $label_Company = "Company";
    $label_OView = "Order view";
    $label_Category = "Category";
    $label_Description = "Description";
    $label_NeedAuth = "Contacts are available after registration!";
    $label_Attention = "Attention! For registration, you need to specify a valid e-mail, because A letter with a password will be sent to him!";
    $label_Contacts = "Contacts";
    $label_ContactsH1 = "Contact with the site administration";
    $label_ContactUs = "Contact us!";
    $label_Message = "Your message";
    $label_CentralOffice = "The central office is located at: Russia, St. Petersburg";
    $label_World = "We work all over the world.";
    $label_Skype = "";
    $label_AboutUs1 = "Social network for manufacturing enterprises.";
    $label_AboutUs2 = "Take advantage of our online portal for suppliers and customers.";
    $label_AboutUs3 = "This site is focused on establishing strong business ties.";
    $label_Social = "Social links";
    $label_MainAddress = "Russia, Saint-Petersburg";
    $label_Footer = 'Industrial order LLC';
    $label_PTitle = " | Social network for manufacturing enterprises";
    $label_ASignIn = "Sign in to your account";
    $label_AQuest = "You do not have an account on our site?";
    $label_Password = "Password";
    $label_Reg = "Registration on industrialorder.com";
    $label_SendPassword = "reestablish password on industrialorder.com";
    $label_Guest = "Access is denied, sign up or sign in now!";
    $label_OSearch = "Search orders";
    $label_OSearchT = "Search orders by title";
    $label_OSearchI = "Enter order title";
    $label_CSearch = "Search companys by type of activity";
    $label_CSearchT = "Search companys by title";
    $label_CSearchI = "Enter company title";
    $label_Section = "Choose a section";
    $label_OQuest = "Question about the order";
    $label_CQuest = "Question about your company";
    $label_YourQuest = "Your question";
    $label_FExt = "Valid file extensions";
    $label_File = "File";
    $label_Files = "Files";
    $label_FLimit = "No more than 20 files.";
    $label_AllOrders = "All orders";
    $label_AllCompanys = "All companys";
    $label_OAdd = "Add an order";
    $label_EndDateDesc = "Date to which the order is relevant. Then the order is removed from the database.";
    $label_AddOrder = "Order title";
    $label_AddOrderDesc = "Enter the title of the order, used when searching.";
    $label_OrderDesc = "Order description";
    $label_OrderDescHelp = "Enter the detailed order description.";
    /* 21.04.17 */
    $label_AddFiles = "Add photos";
    $label_AddFilesAlert = "First you need to add a company!";
    $label_CompanyFiles = "Additional information about the company";
    $label_CVPhoto = "Company photos";
    $label_AddLibFile = "ADD to library";
    $label_filename = "file name";
    $label_techlib_view = "show library";
    $label_News = "news";
    $label_Valuation = "valuation";
    $label_Note = "add node";
    $label_Delete = "Delete";
    $label_ChangeLogo = "Change logo";
    $label_ForgetPassword = "Forget password";
    $label_Help = "Help";
    $label_ChangeEmail = "Change E-mail"; 
    $label_Helper = "Welcome! <br> Leave the contact details and we will register your company yourself";
    $label_Website = "Address of your site";
    $label_Reg_email = "Enter the e-mail specified during registration and we will send a password to it";
    $label_Change_password = "Change password"; 
    $label_New_password = "New password";
    $label_New_password_two = "New password again";
    $label_Back = "Back";
    $label_Action1 = "Start typing an activity or select it from the list";
    $label_Company_name = "Company name";
    $label_Load = "Load";
    $label_Friends = "Friends";
    $label_Notes = "Notes";
    $label_AdminPanel = "Company admin panel";
    $label_RegPeople = "Employee registration";
    $label_FullName = "Full name";
    $label_Employers = "A list of employees";
    $label_CompanyAdmin = "Administration of the company";
    $label_NewEmail = "Enter new E-mail address";
    $label_Theme = "Subject";
    $label_Date = "Date";
    $label_Action = "Action";
    $label_Read = "Read the";
    $label_Answers = "Company responses";
    $label_MessageRead = "Reading the message";
    $label_From = "from";
    $label_Answer = "reply";
    $label_Private = "Private messages";
    $label_DateAdding = "Upload date";
    $label_WriteYouAnswer = "Enter your answer";
    $label_DateChange = "Date of last change";
    $label_DateEnd = "End date of the order";
    $label_FromCompany = "From company";
    $label_OrderNumber = "Order number";
    $label_ViewOrders = "View orders";
    $label_CurrentOrder = "Current orders";
    $label_ArchiveOrders = "Archive of orders";
    $label_WriteReview = "Write a review on the company";
    $label_Valuation = "rating";
    $label_AskQuestion = "Ask a Question";
    $label_AnswersForum = "Answers";
    $label_OrderNumber = "Order №";
    $label_FilesSearch = "Files";
    $label_SearchResult = "search results";
    $label_Inquiry = "Inquiry";
    $label_job = "useful links";
    
}
