<?php
/**
 * Copyright (c) 2017.
 */

/**
 * Файл с пользовательскими функциями
 */

function chkLng()
{

    $mystring = $_SERVER['HTTP_USER_AGENT'];
    $findme = 'yandex.com';
    $pos = strpos($mystring, $findme);
    $findme2 = 'google';
    $pos2 = strpos($mystring, $findme2);
    if ($pos === FALSE && $pos2 === FALSE) {
        preg_match_all('/([a-z]{1,8}(?:-[a-z]{1,8})?)(?:;q=([0-9.]+))?/', strtolower($_SERVER["HTTP_ACCEPT_LANGUAGE"]), $matches); // Получаем массив $matches с соответствиями
        $langs = array_combine($matches[1], $matches[2]); // Создаём массив с ключами $matches[1] и значениями $matches[2]
        foreach ($langs as $n => $v)
            $langs[$n] = $v ? $v : 1; // Если нет q, то ставим значение 1
        arsort($langs); // Сортируем по убыванию q
        $default_lang = key($langs); // Берём 1-й ключ первого (текущего) элемента (он же максимальный по q)
        return $default_lang; // Выводим язык по умолчанию
    } else {
        $default_lang = "ru-ru";
        $_SESSION['currentLng'] = "ru-ru";
        return $default_lang;
    }
}

function generate_password($number)
{
    $arr = array('a', 'b', 'c', 'd', 'e', 'f',
        'g', 'h', 'i', 'j', 'k', 'l',
        'm', 'n', 'p', 'r', 's',
        't', 'u', 'v', 'x', 'y', 'z',
        'A', 'B', 'C', 'D', 'E', 'F',
        'G', 'H', 'I', 'J', 'K', 'L',
        'M', 'N', 'P', 'R', 'S',
        'T', 'U', 'V', 'X', 'Y', 'Z',
        '1', '2', '3', '4', '5', '6',
        '7', '8', '9');
    // Генерируем пароль
    $pass_gen = "";
    for ($i = 0; $i < $number; $i++) {
        // Вычисляем случайный индекс массива
        $index = rand(0, count($arr) - 1);
        $pass_gen .= $arr[$index];
    }
    return $pass_gen;
}

/** Отпровляем сообщение на почту
 * @param string $to
 * @param string $from
 * @param string $title
 * @param string $message
 * @return bool
 */
function sendMessageMail($to, $from, $title, $message)
{
    //Формируем заголовок письма
    $subject = $title;
    $subject = '=?utf-8?b?' . base64_encode($subject) . '?=';

    //Формируем заголовки для почтового сервера
    $headers = "Content-type: text/html; charset=\"utf-8\"". PHP_EOL;
    $headers .= "From: robot@industrialorder.com" . PHP_EOL;
    $headers .= "MIME-Version: 1.0" . PHP_EOL;
    $headers .= "Date: " . date('D, d M Y h:i:s O') . PHP_EOL;
    $headers .= "X-Mailer: PHP/" . phpversion();

    //Отправляем данные на ящик админа сайта
    if (!mail($to, $subject, $message, $headers))
        return 'Ошибка отправки письма!';
    else
        return true;
}

/** функция вывода ошибок
 * @param array $data
 * @return string $err
 */
function showErrorMessage($data)
{
    $err = '';
    if (is_array($data)) {
        foreach ($data as $val)
            $err .= '<div class="alert alert-danger" role="alert">' . $val . '</div>' . "\n";
    } else
        $err .= '<div class="alert alert-danger" role="alert">' . $data . '</div>' . "\n";
    return $err;
}

/** Простой генератор соли
 * @param string $sql
 * @return string $salt
 */
function salt()
{
    $salt = substr(md5(uniqid()), -8);
    return $salt;
}

/** Проверка валидации email
 * @param string $email
 * @return bool
 */
function emailValid($email)
{
    if (function_exists('filter_var')) {
        if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
            return true;
        } else {
            return false;
        }
    } else {
        if (!preg_match("/^[a-z0-9_.-]+@([a-z0-9]+\.)+[a-z]{2,6}$/i", $email)) {
            return false;
        } else {
            return true;
        }
    }
}

/** Запись телеметрии во внешнюю базу
 * @param string $remote_addr
 * @param string $http_current
 * @param string $http_full
 * @param double $gen_time
 * @param double $memory_avg
 * @param double $memory_peak
 * @return void
 */
function teleLog($remote_addr, $http_current, $http_full, $gen_time, $memory_avg, $memory_peak)
{
    try {
        $dbl = new PDO('mysql:host=91.240.86.235;dbname=' . BEZ_DATABASE_L, BEZ_DBUSER_L, BEZ_DBPASSWORD_L, array(
            PDO::ATTR_PERSISTENT => true
        ));
    } catch (PDOException $e) {
        print "Ошибка соединения!: " . $e->getMessage() . "<br/>";
        die();
    }
    $dbl->exec("set names utf8");
    $dbl->exec("SET CHARACTER SET 'utf8'");
    $dbl->exec("SET SESSION collation_connection = 'utf8_general_ci'");

    if ($dbl) {
        $sql = 'INSERT INTO `stable` SET remote_addr=:remote_addr,http_current=:http_current,http_full=:http_full,gen_time=:gen_time,memory_avg=:memory_avg,memory_peak=:memory_peak';
        $stmt = $dbl->prepare($sql);
        $stmt->bindValue(':remote_addr', $remote_addr);
        $stmt->bindValue(':http_current', $http_current);
        $stmt->bindValue(':http_full', $http_full);
        $stmt->bindValue(':gen_time', $gen_time);
        $stmt->bindValue(':memory_avg', $memory_avg);
        $stmt->bindValue(':memory_peak', $memory_peak);
        $stmt->execute();
    } else {
        echo "<div class='alert alert-danger' role='alert'>База телеметрии не работает!</div>";
    }

}

/** Вывод лога телеметрии
 * @return array
 */
function showLog()
{
    try {
        $dbs = new PDO('mysql:host=91.240.86.235;dbname=' . BEZ_DATABASE_L, BEZ_DBUSER_L, BEZ_DBPASSWORD_L, array(pdo::ATTR_PERSISTENT => true));
    } catch (PDOException $e) {
        print "Ошибка соединения!: " . $e->getMessage() . "<br/>";
        die();
    }
    $dbs->exec("set names utf8");
    $dbs->exec("SET CHARACTER SET 'utf8'");
    $dbs->exec("SET SESSION collation_connection = 'utf8_general_ci'");

    if ($dbs) {
        $sql = "SELECT * FROM `stable` ORDER BY id DESC LIMIT 50";
        $stmt = $dbs->prepare($sql);
        $stmt->execute();

        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

        $table = "<div class='table-responsive'><table class='table table-bordered table-hover'>";
        $table .= "<thead> <tr> <th>#</th> <th>Время</th> <th>IP</th> <th>Страница</th> <th>Полный путь</th> <th>Генерация</th> <th>Среднее</th> <th>Максимум</th> </tr> </thead>";
        $table .= "<tbody>";

        foreach ($rows as $val) {
            $id = $val['id'];
            $visit_date = $val['visit_date'];
            $remote_addr = $val['remote_addr'];
            $http_current = $val['http_current'];
            $http_full = $val['http_full'];
            $gen_time = $val['gen_time'];
            $memory_avg = $val['memory_avg'];
            $memory_peak = $val['memory_peak'];

            $table .= '<tr> <th scope="row">' . $id . '</th> <td>' . date('d-m-Y', strtotime($visit_date)) . '</td> <td>' . $remote_addr . '</td> <td>' . $http_current . '</td> <td>' . $http_full . '</td> <td>' . sprintf("%01.3f", $gen_time) . ' сек.</td> <td>' . sprintf("%01.3f", $memory_avg) . ' Мб.</td>  <td>' . sprintf("%01.3f", $memory_peak) . ' Мб.</td> </tr>';
        }
        $table .= "</tbody>";
        $table .= "</table></div>";

        $sqlt = "SELECT gen_time, memory_avg, memory_peak FROM `stable`";
        $stmt = $dbs->prepare($sqlt);
        $stmt->execute();
        $rowst = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $kolvo = count($rowst);
        $clock = $avg = $peak = 0;
        foreach ($rowst as $val) {
            $clock = $clock + $val['gen_time'];
            $avg = $avg + $val['memory_avg'];
            $peak = $peak + $val['memory_peak'];
        }
        $telemetria = "Всего записей: <span class='label label-default'>" . $kolvo . "</span>, среднее время генерации страницы: <span class='label label-default'>" . sprintf("%01.4f", ($clock / $kolvo)) . "</span>, средняя загрузка ram: <span class='label label-default'>" . sprintf("%01.4f", ($avg / $kolvo)) . "</span>, средняя из max загрузка ram: <span class='label label-default'>" . sprintf("%01.4f", ($peak / $kolvo)) . "</span>.";

        return array($table, $telemetria);
    } else {
        echo "<div class='alert alert-danger' role='alert'>База телеметрии не работает!</div>";
    }
}

/* Разбить на микрсервисы */

/** Создание вопроса (аналог форума)
 * @param array $array
 * @return string $answer
 */
function createQuestion($array)
{
    //foreach ($array as $value){
    $login = $array[0];
    $subject = $array[1];
    $question = $array[2];
    $section = $array[3];
    //$time
    //}
    //$answer='Дата создания: '. $time.'<br>Логин пользователя: '.$login.'<br>Тема вопроса: '.$subject.'<br>Текст вопроса: '.nl2br($question);
    //return $answer;
    //Подключение к базе данных mySQL с помощью PDO
    try {
        $db = new PDO('mysql:host=localhost;dbname=' . BEZ_DATABASE, BEZ_DBUSER, BEZ_DBPASSWORD, array( //krosis.mysql //localhost
            PDO::ATTR_PERSISTENT => true
        ));
    } catch (PDOException $e) {
        print "Ошибка соединения!: " . $e->getMessage() . "<br/>";
        die();
    }

   
    $db->exec("set names utf8");
    $db->exec("SET CHARACTER SET 'utf8'");
    $db->exec("SET SESSION collation_connection = 'utf8_general_ci'");

    if ($db) {
        $sql = 'INSERT INTO `bez_groups` SET g_login=:g_login,g_subject=:g_subject,g_question=:g_question, section=:section';
        $stmt = $db->prepare($sql);
        $stmt->bindValue(':g_login', $login);
        $stmt->bindValue(':g_subject', $subject);
        $stmt->bindValue(':g_question', $question);
        $stmt->bindValue(':section', $section);
        $stmt->execute();
    } else {
        echo "<div class='alert alert-danger' role='alert'>Тред не создан!</div>";
    }
}

/** Вывод заданных вопросов
 * @return string $show
 */
function showQuestion()
{

    //Подключение к базе данных mySQL с помощью PDO
    try {
        $db = new PDO('mysql:host=localhost;dbname=' . BEZ_DATABASE, BEZ_DBUSER, BEZ_DBPASSWORD, array(
            PDO::ATTR_PERSISTENT => true
        ));
    } catch (PDOException $e) {
        print "Ошибка соединения!: " . $e->getMessage() . "<br/>";
        die();
    }

    $db->exec("set names utf8");
    $db->exec("SET CHARACTER SET 'utf8'");
    $db->exec("SET SESSION collation_connection = 'utf8_general_ci'");
    
    if(isset($_REQUEST[page])){
    $page = $_REQUEST[page];
    }
    else {
        $page = 1;
    }
     $offset = ($page - 1)*10;
    if(isset($_REQUEST['sort'])) {
        $sort = htmlspecialchars(stripslashes($_REQUEST['sort']));
        $desc = htmlspecialchars(stripslashes($_REQUEST['desc']));
    }
    else { 
        $sort = "id";
        $desc = "desc";
    }
    $sql = "SELECT * FROM `bez_groups` ORDER BY $sort $desc LIMIT 10 OFFSET $offset";
    $stmt = $db->prepare($sql);
    $stmt->execute();

    $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
    $url = $_SERVER['QUERY_STRING'];
    $table = "<div class='table-responsive'><table class='table table-bordered table-hover'>";
    $table .= '<thead> <tr> <th> # <a href = ' . $_SERVER['SCRIPT_NAME'] . '?mode=add_question&page=' . $page . '&sort=id&desc=asc><i class="fa fa-caret-down" aria-hidden="true"></i> </a><a href = ' . $_SERVER['SCRIPT_NAME'] . '?mode=add_question&page=' . $page . '&sort=id&desc=desc><i class="fa fa-caret-up" aria-hidden="true"></i></a></th> '
            . '<th>Время <a href = ' . $_SERVER['SCRIPT_NAME'] . '?mode=add_question&page=' . $page . '&sort=g_time&desc=asc><i class="fa fa-caret-down" aria-hidden="true"></i> </a><a href = ' . $_SERVER['SCRIPT_NAME'] . '?mode=add_question&page=' . $page . '&sort=g_time&desc=desc><i class="fa fa-caret-up" aria-hidden="true"></i></a></th> '
            . '<th>Логин <a href = ' . $_SERVER['SCRIPT_NAME'] . '?mode=add_question&page=' . $page . '&sort=g_login&desc=asc><i class="fa fa-caret-down" aria-hidden="true"></i> </a><a href = ' . $_SERVER['SCRIPT_NAME'] . '?mode=add_question&page=' . $page . '&sort=g_login&desc=desc><i class="fa fa-caret-up" aria-hidden="true"></i></a></th> '
            . '<th>Раздел <a href = ' . $_SERVER['SCRIPT_NAME'] . '?mode=add_question&page=' . $page . '&sort=section&desc=asc><i class="fa fa-caret-down" aria-hidden="true"></i> </a><a href = ' . $_SERVER['SCRIPT_NAME'] . '?mode=add_question&page=' . $page . '&sort=section&desc=desc><i class="fa fa-caret-up" aria-hidden="true"></i></a></th> '
            . '<th>Тема <a href = ' . $_SERVER['SCRIPT_NAME'] . '?mode=add_question&page=' . $page . '&sort=g_subject&desc=asc><i class="fa fa-caret-down" aria-hidden="true"></i> </a><a href = ' . $_SERVER['SCRIPT_NAME'] . '?mode=add_question&page=' . $page . '&sort=g_subject&desc=desc><i class="fa fa-caret-up" aria-hidden="true"></i></a></th> '
            . '<th>Сообщение <a href = ' . $_SERVER['SCRIPT_NAME'] . '?mode=add_question&page=' . $page . '&sort=g_question&desc=asc><i class="fa fa-caret-down" aria-hidden="true"></i> </a><a href = ' . $_SERVER['SCRIPT_NAME'] . '?mode=add_question&page=' . $page . '&sort=g_question&desc=desc><i class="fa fa-caret-up" aria-hidden="true"></i></a></th> </tr> </thead>';
    $table .= "<tbody>";

    foreach ($rows as $val) {
        $id = $val['id'];
        $datatime = strtotime($val['g_time']);
        $time = date("d-m-Y", $datatime);
        $login = $val['g_login'];
        $section = $val['section'];
        $subject = $val['g_subject'];
        $question = $val['g_question'];
        $link = "?mode=view_question&g_subject=".$id;
        $table .= '<tr> <th scope="row">' . $id . '</th> <td>' . $time . '</td> <td>' . $login . '</td>  <td>' . $section . '</td> <td><a href = ' . $link . '>' . $subject . '</a></td> <td>' . $question . '</td> </tr>';
    }
    $table .= "</tbody>";
    $table .= "</table></div>";
    
   
    
    $sql1 = "SELECT COUNT(*) FROM `bez_groups` GROUP BY id";
    $stmt1 = $db->prepare($sql1);
    $stmt1->execute();
    $rows1 = $stmt1->fetchAll(PDO::FETCH_ASSOC);
    
    $length = count($rows1); //Количество тем в форуме
    $pages = ceil(($length + 1) / 10); //Количество страниц
  
    
    if($length > 9) {
        $table .= "<div class = 'col-md-offset-6'>";
        if($page != 1) {
          $table .="<a href='?mode=add_question&page=1' title = 'В начало'><<</a> ";
          $table .="<a href='?mode=add_question&page=" . ($page - 1) . "' title = 'Предыдущая'><</a> ";
        }
        
        if(($page - 4) > 0) {
    $table .= " ... ";
    }
    if(($page - 3) > 0) {
         $table .="<a href='?mode=add_question&page=" . ($page - 3) . "'>" . ($page - 3) . " </a>";
    }
    if(($page - 2) > 0) {
         $table .="<a href='?mode=add_question&page=" . ($page - 2) . "'>" . ($page - 2) . " </a>";
    }
    if(($page - 1) > 0) {
         $table .="<a href='?mode=add_question&page=" . ($page - 1) . "'>" . ($page - 1) . " </a>";
    }
    if(($page) > 0) {
         $table .="<a href='?mode=add_question&page=" . $page . "'>" . $page . " </a>";
    }
    if(($page + 1) < $pages+1) {
         $table .="<a href='?mode=add_question&page=" . ($page + 1) . "'>" . ($page + 1) . " </a>";
    }
    if(($page + 2) < $pages+1) {
         $table .="<a href='?mode=add_question&page=" . ($page + 2) . "'>" . ($page + 2) . " </a>";
    }
    if(($page + 3) < $pages+1) {
         $table .="<a href='?mode=add_question&page=" . ($page + 3) . "'>" . ($page + 3) . " </a>";
    }
    if(($page + 3) < $pages) {
        $table .= " ... ";
    }
    
        }
        if($page != $pages) {
          $table .="<a href='?mode=add_question&page=" . ($page + 1) . "' title = 'Следующая'>></a> ";
          $table .="<a href='?mode=add_question&page=". $pages . "' title = 'В конец'>>></a> ";
        }
        $table .="</div>";
    
    //$table .= "<div class = 'col-md-offset-6'><a href = ''>1</a></div>";
    return $table;
}

function getUsersOnline()
{
    $count = 0;
    $handle = opendir(session_save_path());
    if ($handle == false) return -1;
    while (($file = readdir($handle)) != false) {
        if (preg_match("/^sess/", $file)) $count++;
    }
    closedir($handle);
    return $count;
}

/** Вывод заказа
 *
 */
function showOrder()
{

}

/** Добавление заказа
 *
 */
function addOrder()
{

}

/** Вывод карточки компании
 *
 */
function showCompany()
{

}

/** Добавление карточки компании
 *
 */
function addCompany()
{

}

/** Отправка личного сообщения
 *
 */
function sendPM()
{

}

/** Чтение личного сообщения
 *
 */
function readPM()
{

}

/** Ответ на личное сообщение
 *
 */
function answerPM()
{

}

/** Отправка запроса
 *
 */
function sendQuestion()
{

}

/** Отображение профиля
 *
 */
function showProfile()
{

}


?>