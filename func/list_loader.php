<?php
/**
 * Created by PhpStorm.
 * User: Aleksej Kazin
 * Date: 08.03.2017
 * Time: 11:35
 */

$id_type = intval($_REQUEST['type_id']);

//echo '<option>' . $type_id . '</option>';
$sql = 'SELECT * FROM `bez_cat` WHERE id_type=' . $id_type . ' ORDER BY cat_id ASC';
$stmt = $db->prepare($sql);

//Выводим контент
$stmt->execute();

$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

$result_cat = '';

foreach ($rows as $val) {
    $id = $val['cat_id'];
    $name = $val['cat_name'];

    $result_cat .= '<option value="' . $id . '">' . $name . '</option>';
}
echo $result_cat;