<?php

$start_time = microtime();
$start_array = explode(" ", $start_time);
$start_time = $start_array[1] + $start_array[0];

session_start();
//Устанавливаем кодировку и вывод всех ошибок
header('Content-Type: text/html; charset=UTF8');
//error_reporting(E_ALL);
//Включаем буферизацию содержимого
ob_start();

//Определяем переменную для переключателя
$mode = isset($_GET['mode']) ? $_GET['mode'] : false;
$user = isset($_SESSION['user']) ? $_SESSION['user'] : false;
$userID = isset($_SESSION['userlogin']) ? $_SESSION['userlogin'] : false;
$id_reg = isset($_SESSION['id_reg']) ? $_SESSION['id_reg'] : false;
$err = array();

setcookie('id_reg', $_SESSION['id_reg'], time() + 3600, '/', 'industrialorder.com');
setcookie('user', $_SESSION['user'], time() + 3600, '/', 'industrialorder.com');
setcookie('userlogin', $_SESSION['userlogin'], time() + 3600, '/', 'industrialorder.com');


define('ROOT_DIR', dirname(__FILE__));
//Устанавливаем ключ защиты
define('BEZ_KEY', true);

//Подключаем конфигурационный файл
include 'config.php';
setcookie('BEZ_HOST_IO', BEZ_HOST, time() + 3600, '/', 'industrialorder.com');

//подключаем MySQL
include 'bd/bd.php';

//Подключаем скрипт с функциями
include 'func/funct.php';

if (isset($_SESSION['currentLng'])) {
    if ($_SESSION['currentLng'] == "ru-ru") {
        //$_SESSION['currentLng'] = "ru-ru";
        $_SESSION['rusLngSel'] = "selected";
        $_SESSION['engLngSel'] = "";
    } else {
        //$_SESSION['currentLng'] = "en-en";
        $_SESSION['engLngSel'] = "selected";
        $_SESSION['rusLngSel'] = "";
    }
} else {
    if (chkLng() == "ru-ru") {
        $_SESSION['currentLng'] = "ru-ru";
        $_SESSION['rusLngSel'] = "selected";
        $_SESSION['engLngSel'] = "";
    } else {
        $_SESSION['currentLng'] = "en-en";
        $_SESSION['engLngSel'] = "selected";
        $_SESSION['rusLngSel'] = "";
    }
}

//Подключаем селектор языков
include 'func/langSelector.php';

//Проверка на наличие PM
include 'func/pm.php';

$filename = 'changelog.log';
$filedata = date("d.m.Y H:i", filectime($filename));

switch ($mode) {
    // Подключаем обработчик с формой регистрации
    case 'reg':
        include 'scripts/reg/reg.php';
        //include 'scripts/reg/reg_form.html';
        break;

    // Подключаем обработчик с формой авторизации
    case 'auth':
        include 'scripts/auth/auth.php';
        //include 'scripts/auth/auth_form.html';
        break;

    // Подключаем обработчик главной страницы
    case 'index':
    case '':
        include 'scripts/index/index.php';
        include 'scripts/index/index.html';
        break;

    // Подключаем обработчик редактора профиля пользователя
    case 'profile_edit':
        include 'scripts/profile/profile_edit.php';
        include 'scripts/profile/profile_edit.html';
        break;

    // Подключаем обработчик  профиля пользователя
    case 'profile':
        include 'scripts/profile/profile.php';
        include 'scripts/profile/profile.html';
        break;

    // Подключаем обработчик вывода заказов пользователя
    case 'orders':
        include 'scripts/order/orders.php';
        include 'scripts/order/orders.html';
        break;

    // Подключаем обработчик редактора заказов пользователя
    case 'order_edit':
        include 'scripts/order/order_edit.php';
        include 'scripts/order/order_edit.html';
        break;

    // Подключаем обработчик добавления заказа
    case 'order_add':
        include 'scripts/order/order_add.php';
        include 'scripts/order/order_add.html';
        break;

    // Подключаем обработчик вывода всех заказов
    /*case 'all_orders':
        include 'scripts/orders/all_orders.php';
        include 'scripts/orders/all_orders.html';
        break;*/
    case 'all_orders':
        include 'scripts/orders/all_orders.php';
        include 'scripts/orders/all_orders.html';
        break;
    case 'title_search':
        include 'scripts/orders/all_vakancy.php';
        include 'scripts/orders/all_vakancy.html';
        break;

    // Подключаем обработчик вывода компании
    case 'company':
        include 'scripts/company/company.php';
        include 'scripts/company/company.html';
        break;

    // Подключаем обработчик добавления компании
    case 'company_add':
        include 'scripts/company/company_add.php';
        include 'scripts/company/company_add.html';
        break;

    // Подключаем обработчик вывода всех компаний
    case 'all_companys':
        include 'scripts/companys/all_companys.php';
        include 'scripts/companys/all_companys.html';
        break;

    case 'name_search':
        include 'scripts/companys/all_vakancy.html';
        //include 'scripts/companys/all_vakancy.php';
        break;

    //
    case 'company_view':
        include 'scripts/companys/company_view.php';
        include 'scripts/companys/company_view.html';
        break;

    case 'company_admin':
        include 'scripts/company/company_admin.php';
        include 'scripts/company/company_admin.html';
        break;

    case 'company_edit':
        include 'scripts/company/company_edit.php';
        include 'scripts/company/company_edit.html';
        break;

    //
    case 'order_view':
        include 'scripts/orders/order_view.php';
        include 'scripts/orders/order_view.html';
        break;

    //
    case 'search':
        include 'scripts/search/search.php';
        //include 'scripts/search/search.html';
        break;

    //
    case 'search_company':
        include 'scripts/search/search_company.php';
        include 'scripts/search/search_company.html';
        break;
    case 'send_password':
        include 'scripts/send_password/send_password.php';
        include 'scripts/send_password/send_password.html';
        break;
    //
    case 'error':
        include 'scripts/error/error.php';
        break;

    //
    case 'my_pm':
        include 'scripts/pm/my_pm.php';
        include 'scripts/pm/my_pm.html';
        break;

    //
    case 'pm_view':
        include 'scripts/pm_view/pm_view.php';
        include 'scripts/pm_view/pm_view.html';
        break;
    case 'pm_privat':
        include 'scripts/pm_privat/pm_privat.php';
        include 'scripts/pm_privat/pm_privat.html';
        break;
    //
    case 'about':
        include 'scripts/about/about.php';
        include 'scripts/about/about.html';
        break;

    //
    case 'format':
        include 'scripts/format/format.php';
        include 'scripts/format/format.html';
        break;

    //
    case 'contacts':
        include 'scripts/contacts/contacts.php';
        //include 'scripts/contacts/contacts.html';
        break;

    //
    case 'sitemap':
        include 'scripts/sitemap/sitemap.html';
        //include 'scripts/contacts/contacts.html';
        break;

    case 'add_question':
        include 'scripts/add_question/add_question.php';
        include 'scripts/add_question/add_question.html';
        break;

    case 'test':
        include 'scripts/test/test.php';
        include 'scripts/test/test.html';
        break;

    case 'blog_post':
        include 'scripts/blog_post/post.php';
        include 'scripts/blog_post/post.html';
        break;

    case 'list_orders':
        include 'scripts/list/list_orders.php';
        include 'scripts/list/list_orders.html';
        break;
    case 'list_companys':
        include 'scripts/list/list_companys.php';
        include 'scripts/list/list_companys.html';
        break;

    case 'company_files':
        include 'scripts/company_files/company_files.php';
        include 'scripts/company_files/company_files.html';
        break;
    case 'techlib':
        include 'scripts/techlib/techlib.php';
        include 'scripts/techlib/techlib.html';
        break;
    case 'techlib_view';
        include 'scripts/techlib/techlib_view.php';
        include 'scripts/techlib/techlib_view.html';
        break;
    case 'news';
        include 'scripts/news/news.php';
        include 'scripts/news/news.html';
        break;
    case 'view_question';
        include 'scripts/view_question/view_question.php';
        include 'scripts/view_question/view_question.html';
        break;
    case 'help';
        include 'scripts/help/help.php';
        include 'scripts/help/help.html';
        break;
    case 'reg_and_order_add';
        include 'scripts/reg/reg_and_order_add.php';
        include 'scripts/reg/reg_and_order_add.html';
        break;

    // Переадресация на 404 при отсутствии mode
    default:
        header('Location: ' . BEZ_HOST . '404.html');
        break;
}

//Получаем данные с буфера
$content = ob_get_contents();
ob_end_clean();

//кнопка левого меню + вывод логотипа
if ($user === true) {

    $sql = "SELECT avatar FROM `" . BEZ_DBPREFIX . "reg` WHERE id_reg=:id_reg";
    $stmt = $db->prepare($sql);
    $stmt->bindValue(':id_reg', $_SESSION['id_reg'], PDO::PARAM_STR);
    $stmt->execute();
    $row = $stmt->fetchAll(PDO::FETCH_ASSOC);

    $img = "../images/" . $row[0]['avatar'];
//$img = 'uploads/avatars/user.jpg';
    if ($img == "../images/") {
        $img = BEZ_HOST . 'uploads/avatars/user.png';
    } else {
        $img = BEZ_HOST . 'uploads/avatars/' . $row[0]['avatar'];
    }
}
if (isset($_POST['chngLngRUS'])) {
    /* if ($_REQUEST['selLng'] == 'ru-ru') { */
    $_SESSION['currentLng'] = "ru-ru";
    $_SESSION['rusLngSel'] = "selected";
    $_SESSION['engLngSel'] = "";
    if ($user == true) {
        $sql3 = 'UPDATE `bez_reg` SET `defaultLng` = :defaultLng WHERE `id_reg` = :id_reg'; // :r_comapny,
        $stmt = $db->prepare($sql3);
        $stmt->bindValue(':defaultLng', $_SESSION['currentLng'], PDO::PARAM_STR);
        $stmt->bindValue(':id_reg', $_SESSION['id_reg'], PDO::PARAM_INT);
        $stmt->execute();
    }
    header('Location:https://industrialorder.com' . $_SERVER['REQUEST_URI']);
} /* else { */
if (isset($_POST['chngLngENG'])) {
    $_SESSION['currentLng'] = "en-en";
    $_SESSION['engLngSel'] = "selected";
    $_SESSION['rusLngSel'] = "";
    if ($user == true) {
        $sql3 = 'UPDATE `bez_reg` SET `defaultLng` = :defaultLng WHERE `id_reg` = :id_reg'; // :r_comapny,
        $stmt = $db->prepare($sql3);
        $stmt->bindValue(':defaultLng', $_SESSION['currentLng'], PDO::PARAM_STR);
        $stmt->bindValue(':id_reg', $_SESSION['id_reg'], PDO::PARAM_INT);
        $stmt->execute();
        /* } */
    }
    header('Location:https://industrialorder.com' . $_SERVER['REQUEST_URI']);
}
$sql_count = 'SELECT * FROM `' . BEZ_DBPREFIX . 'pm` WHERE u_to =:u_to AND pm_flag = :pm_flag';
$pdo0 = $db->prepare($sql_count);
$pdo0->bindValue(':u_to', $_SESSION['id_reg'], PDO::PARAM_STR);
$pdo0->bindValue(':pm_flag', '0', PDO::PARAM_STR);
$pdo0->execute();
$rows_count = $pdo0->fetchAll(PDO::FETCH_ASSOC);
$sum = count($rows_count);

$count1 = " (" . $sum . ")";

//Подключаем наш шаблон
include 'html/index.html';

$end_time = microtime();
$end_array = explode(" ", $end_time);
$end_time = $end_array[1] + $end_array[0];
$time = $end_time - $start_time;

//$mem_avg = round(memory_get_usage() / 1024 / 1024, 2);
//$mem_max = round(memory_get_peak_usage() / 1024 / 1024, 2);

//$req_uri = explode('&', $_SERVER['REQUEST_URI']);

//teleLog($_SERVER['REMOTE_ADDR'], $req_uri[0], $_SERVER['REQUEST_URI'], round($time, 4), round(memory_get_usage() / 1024 / 1024, 4), round(memory_get_peak_usage() / 1024 / 1024, 4));


if ($user === true) { ?>
    <script>
        document.getElementById('left_menu').style.display = "block";
    </script>
    <?php
} else { ?>
    <script>
        document.getElementById('left_menu').style.display = "none";
    </script>
    <?php
}